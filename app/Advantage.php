<?php

namespace App;

use App\Traits\HasDefaultScopes;
use Illuminate\Database\Eloquent\Model;


class Advantage extends Model
{
    use HasDefaultScopes;
}
