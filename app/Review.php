<?php

namespace App;

use App\Traits\HasDefaultScopes;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    use HasDefaultScopes;
    protected $guarded = [];
}
