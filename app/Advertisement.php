<?php

namespace App;

use App\Traits\HasDefaultScopes;
use Illuminate\Database\Eloquent\Model;

class Advertisement extends Model
{
    use HasDefaultScopes;
}
