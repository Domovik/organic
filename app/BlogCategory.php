<?php

namespace App;

use App\Traits\HasDefaultScopes;
use Illuminate\Database\Eloquent\Model;

class BlogCategory extends Model
{
    use HasDefaultScopes;
}
