<?php

namespace App;

use App\Traits\HasDefaultScopes;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasDefaultScopes;

    public function category(){
        return $this->belongsTo(ProductCategory::class, 'product_category_id');
    }

    public function reviews(){
        return $this->hasMany(Review::class, 'product_id')->where('status', '1');
    }
}
