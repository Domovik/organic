<?php

namespace App;

use App\Traits\HasDefaultScopes;
use Illuminate\Database\Eloquent\Model;

class Worker extends Model
{
    use HasDefaultScopes;
}
