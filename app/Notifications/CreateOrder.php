<?php

namespace App\Notifications;

use App\Product;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class CreateOrder extends Notification
{
    use Queueable;
    public $products_title;
    public $total_price;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($order)
    {
        $products_title_array = [];
        $order->orderRelationship;

        foreach ($order->orderRelationship as $order_products_id){
            $products_title_array[] = Product::where('id', "$order_products_id->product_id")->select('title')->get();
        }

        $string = implode(',', $products_title_array);
        $array = array('[{"title":"', '"}]');
        $this->products_title = str_replace($array, '', $string);
        $this->total_price = $order->total_price;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line( "Вы заказали $this->products_title")
                    ->action('Notification Action', url('/'))
                    ->line("Общая сумма заказа $this->total_price");
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
