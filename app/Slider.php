<?php

namespace App;

use App\Traits\HasDefaultScopes;
use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    use HasDefaultScopes;
}
