<?php

namespace App;

use App\Traits\HasDefaultScopes;
use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    use HasDefaultScopes;
}
