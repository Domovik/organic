<?php

namespace App;

use App\Traits\HasDefaultScopes;
use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    use HasDefaultScopes;
}
