<?php

namespace App;

use App\Traits\HasDefaultScopes;
use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
    use HasDefaultScopes;
}
