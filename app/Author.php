<?php

namespace App;

use App\Traits\HasDefaultScopes;
use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    use HasDefaultScopes;
}
