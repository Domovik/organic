<?php

namespace App;

use App\Traits\HasDefaultScopes;
use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    use HasDefaultScopes;

    public function products(){
        return $this->hasMany(Product::class, 'product_category_id');
    }
}
