<?php

namespace App;

use App\Traits\HasDefaultScopes;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use HasDefaultScopes;
}
