<?php

namespace App;

use App\Traits\HasDefaultScopes;
use Illuminate\Database\Eloquent\Model;

class Recent extends Model
{
    use HasDefaultScopes;
}
