<?php

namespace App;

use App\Traits\HasDefaultScopes;
use Illuminate\Database\Eloquent\Model;

class BlogReview extends Model
{
    protected $guarded = [];
    use HasDefaultScopes;
}
