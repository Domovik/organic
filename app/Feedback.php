<?php

namespace App;

use App\Traits\HasDefaultScopes;
use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    use HasDefaultScopes;
    protected $guarded = [];
}
