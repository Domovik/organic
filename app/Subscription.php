<?php

namespace App;

use App\Traits\HasDefaultScopes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Subscription extends Model
{
    use Notifiable;
    use HasDefaultScopes;
    protected $guarded = [];

}
