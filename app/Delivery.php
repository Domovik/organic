<?php

namespace App;

use App\Traits\HasDefaultScopes;
use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
    use HasDefaultScopes;
}
