<?php
namespace App\Http\Controllers;

use App\Advertisement;
use App\BlogCategory;
use App\BlogReview;
use App\Delivery;
use App\Faq;
use App\Feedback;
use App\Filters\BlogsFilter;
use App\Http\Requests\CreateFeedbackRequest;
use App\Http\Requests\CreateProductReviewsRequest;
use App\Http\Requests\CreateSubcribeRequest;
use App\Http\Requests\CreateBlogReviewsRequest;
use App\Page;
use App\Partner;
use App\Product;
use App\Blog;
use App\Filters\ProductsFilter;
use App\Review;
use App\Sale;
use App\Testimonial;
use App\Traits\OG;
use App\Worker;
use App\Advantage;
use ChrisKonnertz\OpenGraph\OpenGraph;
use Illuminate\Http\Request;
use App\Subscription;
use App\Slider;
use App\ProductCategory;
use Spatie\Sitemap\Sitemap;
use JsonLd\Context;


class FrontendController extends Controller{
    use OG;

    public function index(Request $request)
    {
        $pages = Page::where('name_page', 'home')->firstOrFail();
        $sales = Sale::active()->byOrder()->Limit(2)->get();
        $products_feature = Product::active()->byOrder()->where('feature', '1')->Limit(12)->get();
        $products_top = Product::active()->byOrder()->Limit(9)->get();
        $advantages = Advantage::active()->byOrder()->Limit(4)->get();
        $blogs = Blog::active()->byOrder()->Limit(6)->get();
        $workers = Worker::active()->byOrder()->Limit(9)->get();
        $testimonials = Testimonial::active()->orderBy('created_at', 'DESC')->Limit(6)->get();
        $partners = Partner::active()->byOrder()->Limit(9)->get();
        $sliders = Slider::active()->byOrder()->Limit(3)->get();
        $product_categories = ProductCategory::active()->byOrder()->Limit(4)->get();
        $og = $this->ogMarkUp($pages, $request);
        $context = Context::create('local_business', [
            'name' => 'Organic Product',
            'description' => 'U can buy all organic product on our site',
            'telephone' => '8-800-555-35-35',
            'openingHours' => 'mon-fri 8:00-19:00',
            'address' => [
                'streetAddress' => '112 Apple St.',
                'addressLocality' => 'Hamden',
                'addressRegion' => 'CT',
                'postalCode' => '06514',
            ],
            'geo' => [
                'latitude' => '41.3958333',
                'longitude' => '-72.8972222',
            ],
        ]);

        return view('index', compact('sales','products_feature', 'advantages', 'blogs', 'workers', 'testimonials', 'partners', 'sliders', 'product_categories', 'products_top', 'pages', 'og', 'context'));
    }
    public function subscribe(CreateSubcribeRequest $request)
    {
        Subscription::create($request->all());

        return response()->json('success');
    }

    public function feedback(CreateFeedbackRequest $request)
    {
        Feedback::create($request->all());

        return response()->json('success');
    }

    public function aboutUs(Request $request)
    {
        $pages = Page::where('name_page', 'about-as')->firstOrFail();
        $product = Product::active()->byOrder()->limit(5)->get();
        $advertisements = Advertisement::active()->byOrder()->limit(3)->get();
        $deliveries = Delivery::active()->byOrder()->limit(4)->get();
        $product_categories = ProductCategory::active()->byOrder()->limit(4)->get();
        $og = $this->ogMarkUp($pages, $request);
        $context = Context::create('local_business', [
            'name' => 'Organic Product',
            'description' => 'U can buy all organic product on our site',
            'telephone' => '8-800-555-35-35',
            'openingHours' => 'mon-fri 8:00-19:00',
            'address' => [
                'streetAddress' => '112 Apple St.',
                'addressLocality' => 'Hamden',
                'addressRegion' => 'CT',
                'postalCode' => '06514',
            ],
            'geo' => [
                'latitude' => '41.3958333',
                'longitude' => '-72.8972222',
            ],
        ]);


        return view('about-us', compact('pages','advertisements', 'deliveries', 'product_categories', 'product', 'og', 'context'));
    }



//    вытаскиваем по iD
    public function  blog_detail($slug, Request $request)
    {
        $categories = BlogCategory::active()->get();
        $blog_detail = Blog::where('slug', $slug)->firstOrFail();
        $pages = $blog_detail;
        $og = $this->ogMarkUp($pages, $request);

        return view('blog-details', compact('blog_detail', 'categories', 'pages', 'og'));
    }

    public function comment_blog(CreateBlogReviewsRequest $request)
    {
        BlogReview::create($request->all());

        return response()->json('success');
    }

    public function contact(Request $request)
    {
        $pages = Page::where('name_page', 'contacts')->firstOrFail();
        $og = $this->ogMarkUp($pages, $request);
        $context = Context::create('local_business', [
            'name' => 'Organic Product',
            'description' => 'U can buy all organic product on our site',
            'telephone' => '8-800-555-35-35',
            'openingHours' => 'mon-fri 8:00-19:00',
            'address' => [
                'streetAddress' => '112 Apple St.',
                'addressLocality' => 'Hamden',
                'addressRegion' => 'CT',
                'postalCode' => '06514',
            ],
            'geo' => [
                'latitude' => '41.3958333',
                'longitude' => '-72.8972222',
            ],
        ]);

        return view('contact', compact('pages', 'og', 'context'));
    }

    public function faq(Request $request)
    {
        $pages = Page::where('name_page', 'faq')->firstOrFail();
        $faqs = Faq::active()->get();

        $og = $this->ogMarkUp($pages, $request);
        $context = Context::create('local_business', [
            'name' => 'Organic Product',
            'description' => 'U can buy all organic product on our site',
            'telephone' => '8-800-555-35-35',
            'openingHours' => 'mon-fri 8:00-19:00',
            'address' => [
                'streetAddress' => '112 Apple St.',
                'addressLocality' => 'Hamden',
                'addressRegion' => 'CT',
                'postalCode' => '06514',
            ],
            'geo' => [
                'latitude' => '41.3958333',
                'longitude' => '-72.8972222',
            ],
        ]);

        return view('faq', compact('faqs', 'pages', 'og', 'context'));
    }

    public  function testimonial(Request $request)
    {
        $pages = Page::where('name_page', 'testimonial')->firstOrFail();
        $testimonials = Testimonial::active()->get();
        $og = $this->ogMarkUp($pages, $request);
        $context = Context::create('local_business', [
            'name' => 'Organic Product',
            'description' => 'U can buy all organic product on our site',
            'telephone' => '8-800-555-35-35',
            'openingHours' => 'mon-fri 8:00-19:00',
            'address' => [
                'streetAddress' => '112 Apple St.',
                'addressLocality' => 'Hamden',
                'addressRegion' => 'CT',
                'postalCode' => '06514',
            ],
            'geo' => [
                'latitude' => '41.3958333',
                'longitude' => '-72.8972222',
            ],
        ]);

        return view('testimonials', compact('testimonials','pages', 'og', 'context'));
    }

    public function blog(Request $request)
    {
        $pages = Page::where('name_page', 'blog')->firstOrFail();
        $blog_categories = BlogCategory::active()->get();
        $builder = Blog::active();
        $paginator = Blog::paginate(6);
        $og = $this->ogMarkUp($pages, $request);

        if ($request->has('slug'))
        {
            $category = BlogCategory::where('slug', $request->get('slug'))->first();
            $builder = $builder->where('blog_category_id', $category->id);
        }

        $context = Context::create('local_business', [
            'name' => 'Organic Product',
            'description' => 'U can buy all organic product on our site',
            'telephone' => '8-800-555-35-35',
            'openingHours' => 'mon-fri 8:00-19:00',
            'address' => [
                'streetAddress' => '112 Apple St.',
                'addressLocality' => 'Hamden',
                'addressRegion' => 'CT',
                'postalCode' => '06514',
            ],
            'geo' => [
                'latitude' => '41.3958333',
                'longitude' => '-72.8972222',
            ],
        ]);
        $blogs = (new BlogsFilter(new Blog, $request))->apply()->get();

        return view('blog', compact('blogs', 'blog_categories', 'builder', 'paginator', 'pages', 'og', 'context'));
    }

    public function shop(Request $request)
    {
        $pages = Page::where('name_page', 'shop')->firstOrFail();
        $categories = ProductCategory::active()->get();
        $popular_products = Product::active()->byOrder()->limit(3)->get();
        $builder = Product::active();
        $og = $this->ogMarkUp($pages, $request);

        if ($request->has('slug')) {
            $category = ProductCategory::where('slug', $request->get('slug'))->first();
            $builder = $builder->where('product_category_id', $category->id);
        }

        if ($request->has('min', 'max')){
            $builder = $builder->whereBetween('price', [$request->get('min'), $request->get('max')]);
        }

        $products = $builder->get();
        $context = Context::create('local_business', [
            'name' => 'Organic Product',
            'description' => 'U can buy all organic product on our site',
            'telephone' => '8-800-555-35-35',
            'openingHours' => 'mon-fri 8:00-19:00',
            'address' => [
                'streetAddress' => '112 Apple St.',
                'addressLocality' => 'Hamden',
                'addressRegion' => 'CT',
                'postalCode' => '06514',
            ],
            'geo' => [
                'latitude' => '41.3958333',
                'longitude' => '-72.8972222',
            ],
        ]);

        $products_filter = Product::query();
        $products_filter = (new ProductsFilter($products_filter, $request))->apply()->get();
        if($products_filter->isNotEmpty())
        {
            $products = $products_filter;
        };

        return view('shop', compact('builder', 'popular_products', 'categories', 'products', 'pages', 'og', 'context', 'products_filter'));
    }

    public function product_detail($slug, Request $request)
    {
        $popular_products = Product::where('top', '1')->orderBy('created_at', 'DESC')->limit(3)->get();
        $categories = ProductCategory::active()->get();
        $product_detail = Product::where('slug', $slug)->first();
        $related_products = Product::where('feature', '1')->orderBy('created_at', 'DESC')->limit(3)->get();
        $pages = $product_detail;
        $og = $this->ogMarkUp($pages, $request);

        return view('shop-single', compact('product_detail', 'categories', 'og', 'popular_products', 'related_products', 'pages'));
    }

    public function review_product(CreateProductReviewsRequest $request)
    {
        Review::create($request->all());

        return response()->json('success');
    }

    public function search(Request $request){

        $search_products = Product::where('title', 'LIKE', '%'.$request->get('search').'%' )->orWhere('description', 'LIKE', '%'.$request->get('search').'%' )->take(3)->get();
        $search_blogs = Blog::where('title', 'LIKE', '%'.$request->get('search').'%' )->orWhere('description', 'LIKE', '%'.$request->get('search').'%' )->take(3)->get();
        return view('layouts.partials.autocomplete', compact('search_blogs', 'search_products'));
    }
}
