<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Notifications\CreateOrder;
use App\Order;
use App\OrderRelationship;
use App\Page;
use App\Product;
use App\Traits\OG;
use Cartalyst\Stripe\Exception\CardErrorException;
use Illuminate\Http\Request;
use App\Http\Requests\CheckoutRequest;
use Cartalyst\Stripe\Laravel\Facades\Stripe;

class CartController extends Controller
{
    use OG;
    public function cart_sent($product_id){
        $cart = Cart::where('product_id', $product_id)->where('status', 1)->where('token', session()->getId())->first();

        if($cart){
            $cart->quantity ++;
            $cart->save();
        }else{
            Cart::create([
                'product_id' => $product_id,
                'token' => session()->getId(),
            ]);
        }

        return response()->view('layouts.partials.cart');
    }

    public function cart(Request $request){
        $carts = Cart::where('token', session()->getId())->where('status', '1')->select('product_id', 'quantity', 'id')->get();
        $pages = Page::where('name_page', 'shop-cart')->firstOrFail();
        $og = $this->ogMarkUp($pages, $request);

        return view('shop-cart', compact('og', 'carts'));
    }

    public function cart_delete($id){
        Cart::where('id', $id)->delete();

        return redirect('shop-cart');
    }

    public $total_price;

    public function order_store (Request $request){
        try {
            $this->order_add($request);
            $charge = Stripe::charges()->create([
                'amount' => $this->total_price,
                'currency' => 'CAD',
                'source' => $request->stripeToken,
                'description' => 'Order',
                'receipt_email' => $request->email,
            ]);

        return $this->order_add($request);

        } catch (CardErrorException $error) {
            echo $error->getMessage();
        }
    }

    public function order_add(Request $request){
        $cart_products = Cart::where('token', session()->getId())->where('status', '1')->select('id', 'quantity', 'product_id', 'status')->get();
        $check = session()->get('email');
        $this->total_price = 0;

        if($cart_products->isNotEmpty() && $check !== null) {
            $order = Order::create(['email' => $check,
                'total_price' => $this->total_price]);

            foreach ($cart_products as $cart_product) {

                $this->total_price += $cart_product->product->price * $cart_product->quantity;
                OrderRelationship::create([
                    'order_id' => $order->id,
                    'product_id' => $cart_product->product_id,
                    'quantity' => $cart_product->quantity,
                ]);
                $cart_product->status = 0;
                $cart_product->save();
            }

            $order->total_price = $this->total_price;
            $order->save();
            $order->notify(new CreateOrder($order));

            return redirect(route('shop-cart'));
        }
    }
}
