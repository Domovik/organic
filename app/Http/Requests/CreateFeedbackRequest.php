<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateFeedbackRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'min:2', 'max:40'],
            'email' => ['required', 'email'],
            'phone' => ['required', 'integer'],
            'subject' => ['required', 'string', 'max:100'],
            'comment' => ['required', 'string', 'max:3000'],
        ];
    }
}
