<?php

namespace App\Providers;

use App\Blog;
use App\Cart;
use App\Observers\BlogObserver;
use App\Observers\OrderObserver;
use App\Order;
use App\Product;
use App\ProductCategory;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\ServiceProvider;
use Illuminate\View\View;
use Spatie\SchemaOrg\Car;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
//        Blog::observe(BlogObserver::class);
//        $cart_products = Cart::where('token', session()->getId())->select('product_id', 'quantity')->get();
//        dump(session()->getId());
//        $product= Product::where('status', '1')->get();
//        $footer_blogs = Blog::where('status', '1')->orderBy('created_at', 'DESC')->Limit(3)->get();
//        $footer_categories = ProductCategory::where('status', '1')->orderBy('created_at', 'DESC')->Limit(3)->get();
//
//        View::share([
//            'cart_products' => $cart_products,
//            'footer_blogs' => $footer_blogs,
//            'footer_categories' => $footer_categories,
//            'product' => $product,
//        ]);
        Blog::observe(BlogObserver::class);
        Order::observe(OrderObserver::class);
        view()->composer('*', function (View $view)
    {
        $product= Product::where('status', '1')->get();
        $cart_products = Cart::where('token', session()->getId())->select('product_id', 'quantity')->get();
        $total_cart_price = 0;

        foreach ($cart_products as $cart_product){
            $total_cart_price += $cart_product->product->price * $cart_product->quantity;
        }
        $footer_blogs = Blog::where('status', '1')->orderBy('created_at', 'DESC')->Limit(3)->get();
        $footer_categories = ProductCategory::where('status', '1')->orderBy('created_at', 'DESC')->Limit(3)->get();
        //...with this variable
        $view->with('cart_products', $cart_products );
        $view->with('footer_blogs', $footer_blogs );
        $view->with('footer_categories', $footer_categories );
        $view->with('product', $product );
        $view->with('total_cart_price', $total_cart_price);
    });
    }
}
