<?php


namespace App\Traits;

use ChrisKonnertz\OpenGraph\OpenGraph;
use http\Env\Request;

trait OG
{
    public function ogMarkUp($pages, $request){
        return (new OpenGraph)
            ->title($pages->meta_title)
            ->description($pages->meta_description)
//            ->image($image)
//            ->type($type)
            ->url($request->url());
    }
}
