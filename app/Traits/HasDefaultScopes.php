<?php


namespace App\Traits;



use Illuminate\Database\Eloquent\Builder;

trait HasDefaultScopes
{
    public function scopeActive(Builder $query){
        return $query->where('status', 1);
    }

    public function scopeByOrder(Builder $query){
        return $query->orderBy('order', 'DESC');
    }
}
