<?php

namespace App\Console\Commands;

use App\Blog;
use App\Page;
use App\Product;
use Illuminate\Console\Command;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\SitemapGenerator;

class GenerateSitemap extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'sitemap:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate the sitemap.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sitemap = Sitemap::create();

        Page::select('name_page')->get()->each(function($item) use ($sitemap){
            $sitemap->add('http://organic.org/'.$item->name_page);
        });

        Product::select('slug')->get()->each(function ($item) use ($sitemap){
            $sitemap->add('http://organic.org/'.$item->name_page);
        });

        Blog::select('slug')->get()->each(function ($item) use ($sitemap){
            $sitemap->add(route('blog_detail', $item->slug));
        });

        $sitemap->writeToFile(public_path('sitemap.xml'));
    }
}
