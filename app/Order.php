<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Order extends Model
{
    use Notifiable;
    protected $fillable = ['email'];

    public function orderRelationship(){
        return $this->hasMany(OrderRelationship::class, 'order_id');
    }

    public function totalPrice(){
        $cart_products = Cart::where('token', session()->getId())->where('status', '1')->select('id', 'quantity', 'product_id', 'status')->get();
        $total_price = 0;
        foreach ($cart_products as $cart_product) {
            $total_price += $cart_product->product->price * $cart_product->quantity;
        }
        return $total_price;

    }
}
