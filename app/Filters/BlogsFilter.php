<?php

namespace App\Filters;

use App\Filters\Filter;

class BlogsFilter extends Filter
{
    public function filter(string $value)
    {
        $this->builder->search($value);
    }
}
