<?php

namespace App\Filters;

use App\Filters\Filter;

class ProductsFilter extends Filter
{
    public function filter($value)
    {
        $this->builder->where('title', 'like', "%$value%");

    }

}
