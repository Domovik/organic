<?php

namespace App;

use App\Traits\HasDefaultScopes;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasDefaultScopes;
    protected $guarded = [];
    public function product(){
        return $this->belongsTo(Product::class, 'product_id');
    }
}
