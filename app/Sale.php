<?php

namespace App;

use App\Traits\HasDefaultScopes;
use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    use HasDefaultScopes;
}
