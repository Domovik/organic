<?php

namespace App;

use App\Traits\HasDefaultScopes;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Blog extends Model
{
    use HasDefaultScopes;
    use Searchable;

    public function author(){
        return $this->belongsTo(Author::class, 'author_id');
    }
    public function category(){
        return $this->belongsTo(BlogCategory::class, 'blog_category_id');
    }
    public function review(){
        return $this->hasMany(BlogReview::class, 'blog_id')->where('status', '1');
    }
    public $asYouType = true;

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $array = $this->toArray();

        // Customize array...

        return $array;
    }
}
