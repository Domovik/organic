<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('title',75);
            $table->string('image',100);
            $table->decimal('price');
            $table->decimal('old_price');
            $table->string('contains',100);
            $table->tinyInteger('is_hot')->default(0)->nullable;
            $table->tinyInteger('is_new')->default(0)->nullable;
            $table->tinyInteger('feature')->default(0)->nullable;
            $table->tinyInteger('top')->default(0)->nullable;
            $table->text('description');
            $table->string('short_description',500);
            $table->tinyInteger('order')->default(0)->nullable;
            $table->tinyInteger('status')->default(0)->nullable;
            $table->foreignId('product_category_id')->constrained();
            $table->string('meta_description');
            $table->string('meta_title');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
