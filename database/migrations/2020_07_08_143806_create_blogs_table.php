<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('image_short',225);
            $table->string('image_full',225);
            $table->string('title',100);
            $table->string('short_description',800);
            $table->string('pag_description',1500);
            $table->text('description');
            $table->tinyInteger('order')->default(0)->nullable;
            $table->tinyInteger('status')->default(0)->nullable;
            $table->foreignId('blog_category_id')->constrained();
            $table->foreignId('author_id')->constrained();
            $table->string('meta_description');
            $table->string('meta_title');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogs');
    }
}
