-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               10.3.22-MariaDB - mariadb.org binary distribution
-- Операционная система:         Win64
-- HeidiSQL Версия:              11.0.0.5958
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Дамп структуры базы данных organic
CREATE DATABASE IF NOT EXISTS `organic` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;
USE `organic`;

-- Дамп структуры для таблица organic.advantages
CREATE TABLE IF NOT EXISTS `advantages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` tinyint(4) NOT NULL DEFAULT 0,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `icon` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы organic.advantages: ~2 rows (приблизительно)
DELETE FROM `advantages`;
/*!40000 ALTER TABLE `advantages` DISABLE KEYS */;
INSERT INTO `advantages` (`id`, `created_at`, `updated_at`, `title`, `description`, `order`, `status`, `icon`) VALUES
	(1, '2020-07-14 12:19:57', '2020-07-14 12:42:25', '100% Organic Products', '<p>Duis aute irure dolor in reprehenderit voluptate velit esse seds cillum dolore eu fugiat nulla pariatur excepteur sint occaecat.</p>', 1, 1, 'ficon icon-fruit-1'),
	(2, '2020-07-14 12:19:57', '2020-07-14 12:42:25', '100% Organic Products', '<p>Duis aute irure dolor in reprehenderit voluptate velit esse seds cillum dolore eu fugiat nulla pariatur excepteur sint occaecat.</p>', 1, 1, 'ficon icon-wheat');
/*!40000 ALTER TABLE `advantages` ENABLE KEYS */;

-- Дамп структуры для таблица organic.advertisements
CREATE TABLE IF NOT EXISTS `advertisements` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `icon` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(5000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` tinyint(4) NOT NULL DEFAULT 0,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы organic.advertisements: ~3 rows (приблизительно)
DELETE FROM `advertisements`;
/*!40000 ALTER TABLE `advertisements` DISABLE KEYS */;
INSERT INTO `advertisements` (`id`, `created_at`, `updated_at`, `icon`, `title`, `description`, `order`, `status`) VALUES
	(1, '2020-07-15 20:49:00', '2020-07-16 09:18:27', 'icon-chicken', '', '<p>We will give you a complete account of the</p>\r\n<p>system, and expound the actual teachings</p>\r\n<p>of the great explorer of the truth, the</p>\r\n<p>master-builder of human complete happiness.</p>', 1, 1),
	(2, '2020-07-15 20:49:00', '2020-07-16 09:18:27', 'icon-chicken', '', '<p>We will give you a complete account of the</p>\r\n<p>system, and expound the actual teachings</p>\r\n<p>of the great explorer of the truth, the</p>\r\n<p>master-builder of human complete happiness.</p>', 1, 1),
	(3, '2020-07-15 20:49:00', '2020-07-16 09:18:27', 'icon-chicken', '', '<p>We will give you a complete account of the</p>\r\n<p>system, and expound the actual teachings</p>\r\n<p>of the great explorer of the truth, the</p>\r\n<p>master-builder of human complete happiness.</p>', 1, 1);
/*!40000 ALTER TABLE `advertisements` ENABLE KEYS */;

-- Дамп структуры для таблица organic.authors
CREATE TABLE IF NOT EXISTS `authors` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` tinyint(4) NOT NULL DEFAULT 0,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы organic.authors: ~1 rows (приблизительно)
DELETE FROM `authors`;
/*!40000 ALTER TABLE `authors` DISABLE KEYS */;
INSERT INTO `authors` (`id`, `created_at`, `updated_at`, `name`, `image`, `description`, `order`, `status`) VALUES
	(1, '2020-07-14 13:32:00', '2020-07-19 10:42:07', 'Vlad Vladik', 'authors\\July2020\\8uqKhQiWOw06dU92gYqN.jpg', '<p>How all this mistaken idea of denouncing pleasures and praising ours pains was born and I will give you sed</p>\r\n<p>a works complete account of the system, and expound the actual teachings off the greatest seds explorer</p>\r\n<p>of thats truth, that master-builders of human happiness pleasure and praising will give you a complete.</p>', 1, 1);
/*!40000 ALTER TABLE `authors` ENABLE KEYS */;

-- Дамп структуры для таблица organic.blogs
CREATE TABLE IF NOT EXISTS `blogs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image_short` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_description` varchar(800) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` tinyint(4) NOT NULL DEFAULT 0,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `meta_title` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blog_category_id` bigint(20) unsigned NOT NULL,
  `author_id` bigint(20) unsigned NOT NULL,
  `image_full` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pag_discription` varchar(1500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `blogs_blog_category_id_foreign` (`blog_category_id`),
  KEY `blogs_author_id_foreign` (`author_id`),
  CONSTRAINT `blogs_author_id_foreign` FOREIGN KEY (`author_id`) REFERENCES `authors` (`id`),
  CONSTRAINT `blogs_blog_category_id_foreign` FOREIGN KEY (`blog_category_id`) REFERENCES `blog_categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы organic.blogs: ~5 rows (приблизительно)
DELETE FROM `blogs`;
/*!40000 ALTER TABLE `blogs` DISABLE KEYS */;
INSERT INTO `blogs` (`id`, `created_at`, `updated_at`, `image_short`, `title`, `short_description`, `description`, `order`, `status`, `meta_title`, `meta_description`, `blog_category_id`, `author_id`, `image_full`, `pag_discription`, `slug`) VALUES
	(8, '2020-07-17 07:41:00', '2020-07-19 10:59:15', 'blogs\\July2020\\SUV0rwtZVG6ayvnWZQIR.jpg', '5', '<p>Здесь находится короткое описание</p>', '<p>How all this mistaken idea denouncing pleasure and praising will give you a completed take a trivial sed example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with man sed who chooses to enjoyonsequences which of us every physical exercise.</p>\r\n<p>Praising will give you a completed take a trivial sed example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with man sed who chooses to enjoy.</p>\r\n<div class="qoute">\r\n<h3>Men who are so beguiled and demoralized by the charms of pleasure of <br />the moment, so blinded by desire, they cannot foresee.</h3>\r\n<span class="author color1">- Bianca Jones - </span></div>\r\n<div class="text">\r\n<p>All this mistaken idea of denouncing pleasure and praising pain was born and I will give you seds our complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful.</p>\r\n</div>', 2, 1, 'Проверка', 'Проверка', 1, 1, 'blogs\\July2020\\DpJudS7tS85pbNMd4pEE.jpg', '<p>Здесь находится описание для Страницы с пагинацией</p>', 'novost-dnya_5\r\n'),
	(9, '2020-07-17 07:41:00', '2020-07-28 09:18:09', 'blogs\\July2020\\SUV0rwtZVG6ayvnWZQIR.jpg', '4', '<p>Здесь находится короткое описание</p>', '<p>Здесь полное описание, для детальной страницы</p>', 2, 1, NULL, NULL, 2, 1, 'blogs\\July2020\\DpJudS7tS85pbNMd4pEE.jpg', '<p>Здесь находится описание для Страницы с пагинацией</p>', 'novost-dnya_4'),
	(11, '2020-07-17 07:41:00', '2020-07-17 07:42:16', 'blogs\\July2020\\SUV0rwtZVG6ayvnWZQIR.jpg', '3', '<p>Здесь находится короткое описание</p>', '<p>Здесь полное описание, для детальной страницы</p>', 2, 1, NULL, NULL, 2, 1, 'blogs\\July2020\\DpJudS7tS85pbNMd4pEE.jpg', '<p>Здесь находится описание для Страницы с пагинацией</p>', 'novost-dnya-3'),
	(12, '2020-07-17 07:41:00', '2020-07-17 07:42:16', 'blogs\\July2020\\SUV0rwtZVG6ayvnWZQIR.jpg', '2', '<p>Здесь находится короткое описание</p>', '<p>Здесь полное описание, для детальной страницы</p>', 2, 1, NULL, NULL, 2, 1, 'blogs\\July2020\\DpJudS7tS85pbNMd4pEE.jpg', '<p>Здесь находится описание для Страницы с пагинацией</p>', 'novost-dnya_2'),
	(13, '2020-07-17 07:41:00', '2020-07-17 07:42:16', 'blogs\\July2020\\SUV0rwtZVG6ayvnWZQIR.jpg', '1', '<p>Здесь находится короткое описание</p>', '<p>Здесь полное описание, для детальной страницы</p>', 2, 1, '1', '2', 2, 1, 'blogs\\July2020\\DpJudS7tS85pbNMd4pEE.jpg', '<p>Здесь находится описание для Страницы с пагинацией</p>', 'novost-dnya_1');
/*!40000 ALTER TABLE `blogs` ENABLE KEYS */;

-- Дамп структуры для таблица organic.blog_categories
CREATE TABLE IF NOT EXISTS `blog_categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` tinyint(4) NOT NULL DEFAULT 0,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы organic.blog_categories: ~3 rows (приблизительно)
DELETE FROM `blog_categories`;
/*!40000 ALTER TABLE `blog_categories` DISABLE KEYS */;
INSERT INTO `blog_categories` (`id`, `created_at`, `updated_at`, `title`, `order`, `status`, `slug`) VALUES
	(1, '2020-07-14 14:10:28', '2020-07-14 14:10:28', 'Eat', 1, 1, 'eat'),
	(2, '2020-07-14 14:10:28', '2020-07-14 14:10:28', 'Gun', 1, 1, 'gun'),
	(3, '2020-07-14 14:10:28', '2020-07-14 14:10:28', 'Ship', 1, 1, 'ship');
/*!40000 ALTER TABLE `blog_categories` ENABLE KEYS */;

-- Дамп структуры для таблица organic.blog_reviews
CREATE TABLE IF NOT EXISTS `blog_reviews` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `comment` varchar(1500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` tinyint(4) NOT NULL DEFAULT 0,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `blog_id` bigint(20) unsigned NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `site` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `blog_reviews_blog_id_foreign` (`blog_id`),
  CONSTRAINT `blog_reviews_blog_id_foreign` FOREIGN KEY (`blog_id`) REFERENCES `blogs` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы organic.blog_reviews: ~7 rows (приблизительно)
DELETE FROM `blog_reviews`;
/*!40000 ALTER TABLE `blog_reviews` DISABLE KEYS */;
INSERT INTO `blog_reviews` (`id`, `created_at`, `updated_at`, `comment`, `order`, `status`, `blog_id`, `name`, `site`, `email`) VALUES
	(1, '2020-07-14 14:13:00', '2020-07-16 21:03:49', 'Очень интересная новость', 1, 1, 8, 'Vlad', '', '0'),
	(2, '2020-07-19 12:35:32', '2020-07-19 12:35:32', 'dasdads', 0, 1, 8, 'asdasda', 'sadasdad', 'dad@mail.ru'),
	(4, '2020-07-19 13:11:18', '2020-07-19 13:11:18', 'asdasd', 0, 1, 8, 'adasdasd', 'asdasdasd', 'kahovskaia@gmail.com'),
	(5, '2020-07-19 13:12:34', '2020-07-19 13:12:34', 'asdads', 0, 1, 8, 'asdasd', 'sdad', 'kahovskaia@gmail.com'),
	(6, '2020-07-19 13:13:11', '2020-07-19 13:13:11', 'asdasda', 0, 1, 8, 'sdasd', 'sadasd', 'admin@admin.com'),
	(14, '2020-07-19 13:23:43', '2020-07-19 13:23:43', 'sdadas', 0, 1, 9, 'asdasdasd', 'asdasd', 'asdasd@mail.ru'),
	(15, '2020-07-24 10:28:43', '2020-07-24 10:28:43', '\'or 1 = 1 </div>', 0, 0, 9, 'Влад', 'asdasdasd', '93-3caenko@mail.ru');
/*!40000 ALTER TABLE `blog_reviews` ENABLE KEYS */;

-- Дамп структуры для таблица organic.data_rows
CREATE TABLE IF NOT EXISTS `data_rows` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data_type_id` int(10) unsigned NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `browse` tinyint(1) NOT NULL DEFAULT 1,
  `read` tinyint(1) NOT NULL DEFAULT 1,
  `edit` tinyint(1) NOT NULL DEFAULT 1,
  `add` tinyint(1) NOT NULL DEFAULT 1,
  `delete` tinyint(1) NOT NULL DEFAULT 1,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  KEY `data_rows_data_type_id_foreign` (`data_type_id`),
  CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=219 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы organic.data_rows: ~206 rows (приблизительно)
DELETE FROM `data_rows`;
/*!40000 ALTER TABLE `data_rows` DISABLE KEYS */;
INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
	(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
	(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
	(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
	(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
	(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
	(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
	(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
	(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
	(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{"model":"TCG\\\\Voyager\\\\Models\\\\Role","table":"roles","type":"belongsTo","column":"role_id","key":"id","label":"display_name","pivot_table":"roles","pivot":0}', 10),
	(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{"model":"TCG\\\\Voyager\\\\Models\\\\Role","table":"roles","type":"belongsToMany","column":"id","key":"id","label":"display_name","pivot_table":"user_roles","pivot":"1","taggable":"0"}', 11),
	(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
	(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
	(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
	(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
	(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
	(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
	(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
	(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
	(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
	(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
	(21, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
	(22, 4, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(23, 4, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '{}', 7),
	(24, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 2),
	(25, 4, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 3),
	(26, 4, 'description', 'rich_text_box', 'Description', 1, 0, 1, 1, 1, 1, '{}', 4),
	(27, 4, 'order', 'number', 'Order', 1, 0, 1, 1, 1, 1, '{}', 5),
	(28, 4, 'status', 'checkbox', 'Status', 1, 1, 1, 1, 1, 1, '{}', 6),
	(29, 5, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(30, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 8),
	(31, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 2),
	(32, 5, 'icon', 'text', 'Icon', 1, 1, 1, 1, 1, 1, '{}', 3),
	(33, 5, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 4),
	(34, 5, 'description', 'rich_text_box', 'Description', 1, 0, 1, 1, 1, 1, '{}', 5),
	(35, 5, 'order', 'number', 'Order', 1, 0, 1, 1, 1, 1, '{}', 6),
	(36, 5, 'status', 'checkbox', 'Status', 1, 1, 1, 1, 1, 1, '{}', 7),
	(37, 6, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(38, 6, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 8),
	(39, 6, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 2),
	(40, 6, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 3),
	(41, 6, 'image', 'image', 'Image', 1, 1, 1, 1, 1, 1, '{}', 4),
	(42, 6, 'description', 'rich_text_box', 'Description', 1, 0, 1, 1, 1, 1, '{}', 5),
	(43, 6, 'order', 'number', 'Order', 1, 0, 1, 1, 1, 1, '{}', 6),
	(44, 6, 'status', 'checkbox', 'Status', 1, 1, 1, 1, 1, 1, '{}', 7),
	(45, 7, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(46, 7, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
	(47, 7, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 2),
	(48, 7, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 3),
	(49, 7, 'order', 'number', 'Order', 1, 0, 1, 1, 1, 1, '{}', 4),
	(50, 7, 'status', 'checkbox', 'Status', 1, 1, 1, 1, 1, 1, '{}', 5),
	(51, 8, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(52, 8, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 7),
	(53, 8, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 3),
	(54, 8, 'comment', 'text_area', 'Comment', 1, 1, 1, 1, 1, 1, '{}', 4),
	(55, 8, 'order', 'number', 'Order', 1, 0, 1, 1, 1, 1, '{}', 5),
	(56, 8, 'status', 'checkbox', 'Status', 1, 1, 1, 1, 1, 1, '{}', 6),
	(57, 8, 'blog_id', 'number', 'Blog Id', 1, 1, 1, 1, 1, 1, '{}', 2),
	(58, 9, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(59, 9, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 13),
	(60, 9, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
	(62, 9, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 5),
	(63, 9, 'short_description', 'rich_text_box', 'Short Description', 1, 0, 1, 1, 1, 1, '{}', 8),
	(64, 9, 'description', 'rich_text_box', 'Description', 1, 0, 1, 1, 1, 1, '{}', 10),
	(65, 9, 'order', 'number', 'Order', 1, 0, 1, 1, 1, 1, '{}', 11),
	(66, 9, 'status', 'checkbox', 'Status', 1, 1, 1, 1, 1, 1, '{}', 12),
	(67, 9, 'blog_category_id', 'hidden', 'Blog Category Id', 1, 0, 1, 1, 1, 1, '{}', 2),
	(68, 9, 'author_id', 'hidden', 'Author Id', 1, 0, 1, 1, 1, 1, '{}', 3),
	(69, 10, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(70, 10, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 8),
	(71, 10, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 2),
	(72, 10, 'icon', 'text', 'Icon', 1, 0, 1, 1, 1, 1, '{}', 3),
	(73, 10, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 4),
	(74, 10, 'description', 'rich_text_box', 'Description', 1, 0, 1, 1, 1, 1, '{}', 5),
	(75, 10, 'order', 'number', 'Order', 1, 0, 1, 1, 1, 1, '{}', 6),
	(76, 10, 'status', 'checkbox', 'Status', 1, 1, 1, 1, 1, 1, '{}', 7),
	(77, 11, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(78, 11, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 7),
	(79, 11, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 2),
	(80, 11, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 3),
	(81, 11, 'description', 'rich_text_box', 'Description', 1, 0, 1, 1, 1, 1, '{}', 4),
	(82, 11, 'order', 'number', 'Order', 1, 0, 1, 1, 1, 1, '{}', 5),
	(83, 11, 'status', 'checkbox', 'Status', 1, 1, 1, 1, 1, 1, '{}', 6),
	(84, 12, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(85, 12, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 9),
	(86, 12, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 2),
	(87, 12, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 3),
	(88, 12, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, '{}', 4),
	(89, 12, 'phone', 'text', 'Phone', 1, 0, 1, 1, 1, 1, '{}', 5),
	(90, 12, 'subject', 'text', 'Subject', 1, 0, 1, 1, 1, 1, '{}', 6),
	(91, 12, 'comment', 'rich_text_box', 'Comment', 1, 0, 1, 1, 1, 1, '{}', 7),
	(92, 12, 'status', 'checkbox', 'Status', 1, 1, 1, 1, 1, 1, '{}', 8),
	(93, 13, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(94, 13, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 1, 0, 1, '{}', 7),
	(95, 13, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 2),
	(96, 13, 'image', 'image', 'Image', 1, 1, 1, 1, 1, 1, '{}', 3),
	(97, 13, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 4),
	(98, 13, 'order', 'number', 'Order', 1, 0, 1, 1, 1, 1, '{}', 5),
	(99, 13, 'status', 'checkbox', 'Status', 1, 1, 1, 1, 1, 1, '{}', 6),
	(100, 14, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(101, 14, 'created_at', 'timestamp', 'Created At', 1, 1, 1, 1, 0, 1, '{}', 6),
	(102, 14, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 2),
	(103, 14, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 3),
	(104, 14, 'order', 'number', 'Order', 1, 0, 1, 1, 1, 1, '{}', 4),
	(105, 14, 'status', 'checkbox', 'Status', 1, 1, 1, 1, 1, 1, '{}', 5),
	(106, 16, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(107, 16, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 17),
	(108, 16, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 3),
	(109, 16, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 4),
	(110, 16, 'image', 'image', 'Image', 1, 1, 1, 1, 1, 1, '{}', 5),
	(111, 16, 'price', 'number', 'Price', 1, 0, 1, 1, 1, 1, '{}', 6),
	(112, 16, 'old_price', 'number', 'Old Price', 1, 0, 1, 1, 1, 1, '{}', 7),
	(113, 16, 'contains', 'text', 'Contains', 1, 0, 1, 1, 1, 1, '{}', 8),
	(114, 16, 'is_hot', 'checkbox', 'Is Hot', 1, 0, 1, 1, 1, 1, '{}', 9),
	(115, 16, 'is_new', 'checkbox', 'Is New', 1, 0, 1, 1, 1, 1, '{}', 10),
	(116, 16, 'feature', 'checkbox', 'Feature', 1, 0, 1, 1, 1, 1, '{}', 12),
	(117, 16, 'description', 'rich_text_box', 'Description', 1, 0, 1, 1, 1, 1, '{}', 13),
	(118, 16, 'short_description', 'rich_text_box', 'Short Description', 1, 0, 1, 1, 1, 1, '{}', 14),
	(119, 16, 'order', 'number', 'Order', 1, 0, 1, 1, 1, 1, '{}', 15),
	(120, 16, 'status', 'checkbox', 'Status', 1, 1, 1, 1, 1, 1, '{}', 16),
	(121, 16, 'product_category_id', 'text', 'Product Category Id', 1, 0, 1, 1, 1, 1, '{}', 2),
	(122, 17, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(123, 17, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 11),
	(124, 17, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 2),
	(125, 17, 'label', 'text', 'Label', 1, 0, 1, 1, 1, 1, '{}', 3),
	(126, 17, 'offer', 'text', 'Offer', 1, 0, 1, 1, 1, 1, '{}', 4),
	(127, 17, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 5),
	(128, 17, 'description', 'rich_text_box', 'Description', 1, 0, 1, 1, 1, 1, '{}', 6),
	(129, 17, 'link', 'text', 'Link', 1, 0, 1, 1, 1, 1, '{}', 7),
	(130, 17, 'image', 'image', 'Image', 1, 1, 1, 1, 1, 1, '{}', 8),
	(131, 17, 'order', 'number', 'Order', 1, 0, 1, 1, 1, 1, '{}', 9),
	(132, 17, 'status', 'checkbox', 'Status', 1, 1, 1, 1, 1, 1, '{}', 10),
	(133, 18, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(134, 18, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 7),
	(135, 18, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 2),
	(136, 18, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 3),
	(137, 18, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, '{}', 4),
	(138, 18, 'order', 'number', 'Order', 1, 1, 1, 1, 1, 1, '{}', 5),
	(139, 18, 'status', 'checkbox', 'Status', 1, 1, 1, 1, 1, 1, '{}', 6),
	(140, 19, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(141, 19, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
	(142, 19, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 2),
	(143, 19, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 3),
	(144, 19, 'order', 'number', 'Order', 1, 0, 1, 1, 1, 1, '{}', 4),
	(145, 19, 'status', 'checkbox', 'Status', 1, 1, 1, 1, 1, 1, '{}', 5),
	(146, 20, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(147, 20, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 8),
	(148, 20, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 2),
	(149, 20, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 3),
	(150, 20, 'image', 'image', 'Image', 1, 1, 1, 1, 1, 1, '{}', 4),
	(151, 20, 'description', 'rich_text_box', 'Description', 1, 0, 1, 1, 1, 1, '{}', 5),
	(152, 20, 'profession', 'text', 'Profession', 1, 0, 1, 1, 1, 1, '{}', 6),
	(153, 20, 'status', 'checkbox', 'Status', 1, 1, 1, 1, 1, 1, '{}', 7),
	(154, 21, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(155, 21, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 12),
	(156, 21, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 2),
	(157, 21, 'image', 'image', 'Image', 1, 1, 1, 1, 1, 1, '{}', 4),
	(158, 21, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 3),
	(159, 21, 'profession', 'text', 'Profession', 1, 0, 1, 1, 1, 1, '{}', 5),
	(160, 21, 'description', 'rich_text_box', 'Description', 1, 0, 1, 1, 1, 1, '{}', 6),
	(161, 21, 'facebook', 'text', 'Facebook', 1, 0, 1, 1, 1, 1, '{}', 7),
	(162, 21, 'instagram', 'text', 'Instagram', 1, 0, 1, 1, 1, 1, '{}', 8),
	(163, 21, 'twitter', 'text', 'Twitter', 1, 0, 1, 1, 1, 1, '{}', 9),
	(164, 21, 'order', 'number', 'Order', 1, 0, 1, 1, 1, 1, '{}', 10),
	(165, 21, 'status', 'checkbox', 'Status', 1, 1, 1, 1, 1, 1, '{}', 11),
	(166, 22, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(167, 22, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
	(168, 22, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 3),
	(169, 22, 'comment', 'rich_text_box', 'Comment', 1, 0, 1, 1, 1, 1, '{}', 4),
	(170, 22, 'rate', 'select_dropdown', 'Rate', 1, 0, 1, 1, 1, 1, '{}', 5),
	(171, 22, 'product_id', 'hidden', 'Product Id', 1, 1, 1, 1, 1, 1, '{}', 2),
	(173, 16, 'product_belongsto_product_category_relationship', 'relationship', 'product_categories', 0, 1, 1, 1, 1, 1, '{"model":"App\\\\ProductCategory","table":"product_categories","type":"belongsTo","column":"product_category_id","key":"id","label":"title","pivot_table":"advantages","pivot":"0","taggable":"0"}', 18),
	(174, 9, 'blog_belongsto_blog_category_relationship', 'relationship', 'Сategories', 0, 0, 1, 1, 1, 1, '{"model":"App\\\\BlogCategory","table":"blog_categories","type":"belongsTo","column":"blog_category_id","key":"id","label":"title","pivot_table":"advantages","pivot":"0","taggable":"0"}', 14),
	(175, 9, 'blog_belongsto_author_relationship', 'relationship', 'Authors', 0, 0, 1, 1, 1, 1, '{"model":"App\\\\Author","table":"authors","type":"belongsTo","column":"author_id","key":"id","label":"name","pivot_table":"advantages","pivot":"0","taggable":"0"}', 15),
	(177, 14, 'product_id', 'hidden', 'Product Id', 1, 0, 1, 1, 1, 1, '{}', 7),
	(178, 8, 'blog_review_belongsto_blog_relationship', 'relationship', 'blogs', 0, 1, 1, 1, 1, 1, '{"model":"App\\\\Blog","table":"blogs","type":"belongsTo","column":"blog_id","key":"id","label":"title","pivot_table":"advantages","pivot":"0","taggable":null}', 8),
	(179, 9, 'image_short', 'image', 'Image Short', 1, 1, 1, 1, 1, 1, '{}', 7),
	(180, 9, 'image_full', 'image', 'Image Full', 1, 0, 1, 1, 1, 1, '{}', 6),
	(181, 23, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(183, 23, 'image', 'image', 'Image', 1, 0, 1, 1, 1, 1, '{}', 3),
	(184, 23, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 4),
	(185, 23, 'sup_title', 'text_area', 'Sup Title', 1, 0, 1, 1, 1, 1, '{}', 5),
	(186, 23, 'description', 'text_area', 'Description', 1, 0, 1, 1, 1, 1, '{}', 6),
	(187, 23, 'order', 'number', 'Order', 1, 0, 1, 1, 1, 1, '{}', 7),
	(188, 23, 'status', 'checkbox', 'Status', 1, 1, 1, 1, 1, 1, '{}', 8),
	(189, 23, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 9),
	(190, 23, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 10),
	(191, 23, 'image_background', 'image', 'Image Background', 1, 1, 1, 1, 1, 1, '{}', 2),
	(192, 16, 'top', 'checkbox', 'Top', 1, 0, 1, 1, 1, 1, '{}', 11),
	(197, 10, 'number', 'text', 'Number', 1, 0, 1, 1, 1, 1, '{}', 5),
	(199, 9, 'pag_discription', 'rich_text_box', 'Pag Discription', 1, 0, 1, 1, 1, 1, '{}', 9),
	(200, 22, 'name', 'text', 'Name', 0, 0, 1, 1, 1, 1, '{}', 7),
	(201, 22, 'email', 'text', 'Email', 0, 1, 1, 1, 1, 1, '{}', 8),
	(202, 22, 'site', 'text', 'Site', 0, 0, 1, 1, 1, 1, '{}', 9),
	(206, 16, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{"slugify":{"origin":"title","forceUpdate":true}}', 18),
	(207, 7, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{"slugify":{"origin":"title","forceUpdate":true}}', 7),
	(208, 14, 'slug', 'text', 'Slug', 0, 1, 1, 1, 1, 1, '{"slugify":{"origin":"title","forceUpdate":true}}', 8),
	(209, 9, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{"slugify":{"origin":"title","forceUpdate":true}}', 14),
	(210, 24, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(211, 24, 'name_page', 'text', 'Name Page', 0, 1, 0, 0, 0, 0, '{}', 2),
	(212, 24, 'title', 'text', 'Title', 1, 0, 0, 0, 0, 0, '{}', 3),
	(213, 24, 'slug', 'text', 'Slug', 0, 0, 0, 0, 0, 0, '{}', 4),
	(214, 24, 'meta_title', 'text', 'Meta Title', 1, 1, 1, 1, 1, 1, '{}', 5),
	(215, 24, 'meta_description', 'text', 'Meta Description', 1, 1, 1, 1, 1, 1, '{}', 6),
	(216, 24, 'status', 'text', 'Status', 1, 1, 1, 1, 1, 1, '{}', 7),
	(217, 24, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 1, 0, 1, '{}', 8),
	(218, 24, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9);
/*!40000 ALTER TABLE `data_rows` ENABLE KEYS */;

-- Дамп структуры для таблица organic.data_types
CREATE TABLE IF NOT EXISTS `data_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT 0,
  `server_side` tinyint(4) NOT NULL DEFAULT 0,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_types_name_unique` (`name`),
  UNIQUE KEY `data_types_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы organic.data_types: ~23 rows (приблизительно)
DELETE FROM `data_types`;
/*!40000 ALTER TABLE `data_types` DISABLE KEYS */;
INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
	(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', '', 1, 0, NULL, '2020-07-14 08:54:13', '2020-07-14 08:54:13'),
	(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2020-07-14 08:54:13', '2020-07-14 08:54:13'),
	(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, 'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController', '', 1, 0, NULL, '2020-07-14 08:54:13', '2020-07-14 08:54:13'),
	(4, 'advantages', 'advantages', 'Advantage', 'Advantages', NULL, 'App\\Advantage', NULL, NULL, NULL, 1, 0, '{"order_column":"order","order_display_column":"title","order_direction":"asc","default_search_key":null,"scope":null}', '2020-07-14 09:04:07', '2020-07-14 12:19:29'),
	(5, 'advertisements', 'advertisements', 'Advertisement', 'Advertisements', NULL, 'App\\Advertisement', NULL, NULL, NULL, 1, 0, '{"order_column":"order","order_display_column":"title","order_direction":"asc","default_search_key":null,"scope":null}', '2020-07-14 09:08:19', '2020-07-16 09:15:55'),
	(6, 'authors', 'authors', 'Author', 'Authors', NULL, 'App\\Author', NULL, NULL, NULL, 1, 0, '{"order_column":"order","order_display_column":"name","order_direction":"asc","default_search_key":null,"scope":null}', '2020-07-14 09:11:06', '2020-07-19 10:41:36'),
	(7, 'blog_categories', 'blog-categories', 'Blog Category', 'Blog Categories', NULL, 'App\\BlogCategory', NULL, NULL, NULL, 1, 0, '{"order_column":"order","order_display_column":"title","order_direction":"asc","default_search_key":null,"scope":null}', '2020-07-14 09:11:58', '2020-07-28 09:17:00'),
	(8, 'blog_reviews', 'blog-reviews', 'Blog Review', 'Blog Reviews', NULL, 'App\\BlogReview', NULL, NULL, NULL, 1, 0, '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null}', '2020-07-14 09:21:25', '2020-07-14 09:21:25'),
	(9, 'blogs', 'blogs', 'Blog', 'Blogs', NULL, 'App\\Blog', NULL, NULL, NULL, 1, 0, '{"order_column":"order","order_display_column":"title","order_direction":"asc","default_search_key":null,"scope":null}', '2020-07-14 09:23:51', '2020-07-28 09:17:42'),
	(10, 'deliveries', 'deliveries', 'Delivery', 'Deliveries', NULL, 'App\\Delivery', NULL, NULL, NULL, 1, 0, '{"order_column":"order","order_display_column":"title","order_direction":"asc","default_search_key":null,"scope":null}', '2020-07-14 09:26:24', '2020-07-16 09:48:21'),
	(11, 'faqs', 'faqs', 'Faq', 'Faqs', NULL, 'App\\Faq', NULL, NULL, NULL, 1, 0, '{"order_column":"order","order_display_column":"title","order_direction":"asc","default_search_key":null}', '2020-07-14 09:32:11', '2020-07-14 09:32:11'),
	(12, 'feedback', 'feedback', 'Feedback', 'Feedback', NULL, 'App\\Feedback', NULL, NULL, NULL, 1, 0, '{"order_column":"status","order_display_column":"email","order_direction":"asc","default_search_key":null}', '2020-07-14 09:33:43', '2020-07-14 09:33:43'),
	(13, 'partners', 'partners', 'Partner', 'Partners', NULL, 'App\\Partner', NULL, NULL, NULL, 1, 0, '{"order_column":"order","order_display_column":"name","order_direction":"asc","default_search_key":null}', '2020-07-14 09:36:57', '2020-07-14 09:36:57'),
	(14, 'product_categories', 'product-categories', 'Product Category', 'Product Categories', NULL, 'App\\ProductCategory', NULL, NULL, NULL, 1, 0, '{"order_column":"order","order_display_column":"title","order_direction":"asc","default_search_key":null,"scope":null}', '2020-07-14 09:39:12', '2020-07-28 09:17:04'),
	(16, 'products', 'products', 'Product', 'Products', NULL, 'App\\Product', NULL, NULL, NULL, 1, 0, '{"order_column":"order","order_display_column":"title","order_direction":"asc","default_search_key":null,"scope":null}', '2020-07-14 09:42:17', '2020-07-28 09:16:36'),
	(17, 'sales', 'sales', 'Sale', 'Sales', NULL, 'App\\Sale', NULL, NULL, NULL, 1, 0, '{"order_column":"order","order_display_column":"title","order_direction":"asc","default_search_key":null}', '2020-07-14 09:55:46', '2020-07-14 09:55:46'),
	(18, 'subscriptions', 'subscriptions', 'Subscription', 'Subscriptions', NULL, 'App\\Subscription', NULL, NULL, NULL, 1, 0, '{"order_column":"order","order_display_column":"email","order_direction":"asc","default_search_key":null}', '2020-07-14 10:00:10', '2020-07-14 10:00:10'),
	(19, 'tags', 'tags', 'Tag', 'Tags', NULL, 'App\\Tag', NULL, NULL, NULL, 1, 0, '{"order_column":"order","order_display_column":"title","order_direction":"asc","default_search_key":null}', '2020-07-14 10:01:23', '2020-07-14 10:01:23'),
	(20, 'testimonials', 'testimonials', 'Testimonial', 'Testimonials', NULL, 'App\\Testimonial', NULL, NULL, NULL, 1, 0, '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null}', '2020-07-14 10:03:04', '2020-07-14 10:03:04'),
	(21, 'workers', 'workers', 'Worker', 'Workers', NULL, 'App\\Worker', NULL, NULL, NULL, 1, 0, '{"order_column":"order","order_display_column":"name","order_direction":"asc","default_search_key":null}', '2020-07-14 10:05:36', '2020-07-14 10:05:36'),
	(22, 'reviews', 'reviews', 'Review', 'Reviews', NULL, 'App\\Review', NULL, NULL, NULL, 1, 0, '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":null}', '2020-07-14 10:08:17', '2020-07-19 14:15:42'),
	(23, 'sliders', 'sliders', 'Slider', 'Sliders', NULL, 'App\\Slider', NULL, NULL, NULL, 1, 0, '{"order_column":"order","order_display_column":"title","order_direction":"asc","default_search_key":null,"scope":null}', '2020-07-15 12:33:13', '2020-07-15 13:19:02'),
	(24, 'pages', 'pages', 'Page', 'Pages', NULL, 'App\\Page', NULL, NULL, NULL, 1, 0, '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null}', '2020-08-06 13:01:42', '2020-08-06 13:01:42');
/*!40000 ALTER TABLE `data_types` ENABLE KEYS */;

-- Дамп структуры для таблица organic.deliveries
CREATE TABLE IF NOT EXISTS `deliveries` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `icon` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number` tinyint(4) NOT NULL DEFAULT 0,
  `title` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` tinyint(4) NOT NULL DEFAULT 0,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы organic.deliveries: ~4 rows (приблизительно)
DELETE FROM `deliveries`;
/*!40000 ALTER TABLE `deliveries` DISABLE KEYS */;
INSERT INTO `deliveries` (`id`, `created_at`, `updated_at`, `icon`, `number`, `title`, `description`, `order`, `status`) VALUES
	(1, '2020-07-16 09:42:00', '2020-07-16 09:47:31', 'icon-business', 4, 'CUSTOMER ORDER', '<p>Organicstore.com&rsquo;s customers order is captured by warehouse API..</p>', 1, 1),
	(2, '2020-07-16 09:42:35', '2020-07-16 09:42:35', 'rot icon-check', 0, 'REPORTING', '<p>Organicstore.com&rsquo;s customers order is captured by warehouse API..</p>', 1, 1),
	(3, '2020-07-16 09:42:00', '2020-07-16 09:47:39', 'rot icon-transport', 2, 'DELIVERY', '<p>Organicstore.com&rsquo;s customers order is captured by warehouse API..</p>', 1, 1),
	(4, '2020-07-16 09:42:35', '2020-07-16 09:42:35', 'rot icon-nature-1', 0, 'PACKING', '<p>Organicstore.com&rsquo;s customers order is captured by warehouse API..</p>', 1, 1);
/*!40000 ALTER TABLE `deliveries` ENABLE KEYS */;

-- Дамп структуры для таблица organic.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы organic.failed_jobs: ~0 rows (приблизительно)
DELETE FROM `failed_jobs`;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Дамп структуры для таблица organic.faqs
CREATE TABLE IF NOT EXISTS `faqs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` tinyint(4) NOT NULL DEFAULT 0,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы organic.faqs: ~4 rows (приблизительно)
DELETE FROM `faqs`;
/*!40000 ALTER TABLE `faqs` DISABLE KEYS */;
INSERT INTO `faqs` (`id`, `created_at`, `updated_at`, `title`, `description`, `order`, `status`) VALUES
	(1, '2020-07-17 16:36:27', '2020-07-17 16:36:28', 'What are the benefits of organic food?', '<p>Again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise.</p>', 1, 1),
	(2, '2020-07-17 16:36:27', '2020-07-17 16:36:28', 'What are the benefits of organic food?', '<p>Again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise.</p>', 1, 1),
	(3, '2020-07-17 16:36:27', '2020-07-17 16:36:28', 'What are the benefits of organic food?', '<p>Again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise.</p>', 1, 1),
	(4, '2020-07-17 16:36:27', '2020-07-17 16:36:28', 'What are the benefits of organic food?', '<p>Again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise.</p>', 1, 1);
/*!40000 ALTER TABLE `faqs` ENABLE KEYS */;

-- Дамп структуры для таблица organic.feedback
CREATE TABLE IF NOT EXISTS `feedback` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` varchar(1200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы organic.feedback: ~96 rows (приблизительно)
DELETE FROM `feedback`;
/*!40000 ALTER TABLE `feedback` DISABLE KEYS */;
INSERT INTO `feedback` (`id`, `created_at`, `updated_at`, `name`, `email`, `phone`, `subject`, `comment`, `status`) VALUES
	(1, '2020-07-16 14:48:53', '2020-07-16 14:48:53', 'qeqwe', 'kahovskaia@gmail.com', '23141', '13123', 'd123d13d1', 0),
	(2, '2020-07-16 14:51:47', '2020-07-16 14:51:47', 'qeqwe', 'kahovskaia@gmail.com', '23141', '13123', 'd123d13d1', 0),
	(3, '2020-07-16 14:51:58', '2020-07-16 14:51:58', 'sweet', 'admin@gmail.com', '123412315123', 'wfsdfsdf', 'dfsdfsf', 0),
	(4, '2020-07-16 14:53:20', '2020-07-16 14:53:20', 'sweet', 'admin@gmail.com', '123412315123', 'wfsdfsdf', 'dfsdfsf', 0),
	(5, '2020-07-16 14:53:46', '2020-07-16 14:53:46', '232', 'kahovskaia@gmail.com', '123412315123', 'wfsdfsdf', 'dasdasdasd', 0),
	(6, '2020-07-16 14:55:16', '2020-07-16 14:55:16', 'Vlad Vladik', 'admin@gmail.com', '+37377892055', 'wfsdfsdf', 'dasdagfaf', 0),
	(7, '2020-07-16 17:16:28', '2020-07-16 17:16:28', 'Vlad Vladik', 'kahovskaia@gmail.com', '23123', 'sdfsdfsdf', 'dsfsdfgsdf', 0),
	(8, '2020-07-16 17:16:34', '2020-07-16 17:16:34', 'Vlad Vladik', 'kahovskaia@gmail.com', '23123', 'sdfsdfsdf', 'dsfsdfgsdf', 0),
	(9, '2020-07-16 17:16:38', '2020-07-16 17:16:38', 'Vlad Vladik', 'kahovskaia@gmail.com', '23123', 'sdfsdfsdf', 'dsfsdfgsdf', 0),
	(10, '2020-07-16 17:16:38', '2020-07-16 17:16:38', 'Vlad Vladik', 'kahovskaia@gmail.com', '23123', 'sdfsdfsdf', 'dsfsdfgsdf', 0),
	(11, '2020-07-16 17:16:38', '2020-07-16 17:16:38', 'Vlad Vladik', 'kahovskaia@gmail.com', '23123', 'sdfsdfsdf', 'dsfsdfgsdf', 0),
	(12, '2020-07-16 17:16:39', '2020-07-16 17:16:39', 'Vlad Vladik', 'kahovskaia@gmail.com', '23123', 'sdfsdfsdf', 'dsfsdfgsdf', 0),
	(13, '2020-07-16 17:16:39', '2020-07-16 17:16:39', 'Vlad Vladik', 'kahovskaia@gmail.com', '23123', 'sdfsdfsdf', 'dsfsdfgsdf', 0),
	(14, '2020-07-16 17:16:39', '2020-07-16 17:16:39', 'Vlad Vladik', 'kahovskaia@gmail.com', '23123', 'sdfsdfsdf', 'dsfsdfgsdf', 0),
	(15, '2020-07-16 17:16:39', '2020-07-16 17:16:39', 'Vlad Vladik', 'kahovskaia@gmail.com', '23123', 'sdfsdfsdf', 'dsfsdfgsdf', 0),
	(16, '2020-07-16 17:16:39', '2020-07-16 17:16:39', 'Vlad Vladik', 'kahovskaia@gmail.com', '23123', 'sdfsdfsdf', 'dsfsdfgsdf', 0),
	(17, '2020-07-16 17:16:40', '2020-07-16 17:16:40', 'Vlad Vladik', 'kahovskaia@gmail.com', '23123', 'sdfsdfsdf', 'dsfsdfgsdf', 0),
	(18, '2020-07-16 17:16:40', '2020-07-16 17:16:40', 'Vlad Vladik', 'kahovskaia@gmail.com', '23123', 'sdfsdfsdf', 'dsfsdfgsdf', 0),
	(19, '2020-07-16 17:19:33', '2020-07-16 17:19:33', 'Влад', 'admin@gmail.com', '+37377892055', 'wfsdfsdf', 'sdvf', 0),
	(20, '2020-07-16 17:23:32', '2020-07-16 17:23:32', 'Влад', 'admin@admin.com', '37377892055', 'wfsdfsdf', 'fdsdfsdf', 0),
	(21, '2020-07-16 17:23:33', '2020-07-16 17:23:33', 'Влад', 'admin@admin.com', '37377892055', 'wfsdfsdf', 'fdsdfsdf', 0),
	(22, '2020-07-16 17:23:34', '2020-07-16 17:23:34', 'Влад', 'admin@admin.com', '37377892055', 'wfsdfsdf', 'fdsdfsdf', 0),
	(23, '2020-07-16 17:23:34', '2020-07-16 17:23:34', 'Влад', 'admin@admin.com', '37377892055', 'wfsdfsdf', 'fdsdfsdf', 0),
	(24, '2020-07-16 17:23:34', '2020-07-16 17:23:34', 'Влад', 'admin@admin.com', '37377892055', 'wfsdfsdf', 'fdsdfsdf', 0),
	(25, '2020-07-16 17:23:34', '2020-07-16 17:23:34', 'Влад', 'admin@admin.com', '37377892055', 'wfsdfsdf', 'fdsdfsdf', 0),
	(26, '2020-07-16 17:23:35', '2020-07-16 17:23:35', 'Влад', 'admin@admin.com', '37377892055', 'wfsdfsdf', 'fdsdfsdf', 0),
	(27, '2020-07-16 17:23:35', '2020-07-16 17:23:35', 'Влад', 'admin@admin.com', '37377892055', 'wfsdfsdf', 'fdsdfsdf', 0),
	(28, '2020-07-16 17:23:35', '2020-07-16 17:23:35', 'Влад', 'admin@admin.com', '37377892055', 'wfsdfsdf', 'fdsdfsdf', 0),
	(29, '2020-07-16 17:24:08', '2020-07-16 17:24:08', 'Vlad Vladik', 'admin@admin.com', '123412315123', 'wfsdfsdf', 'asdasd', 0),
	(30, '2020-07-16 17:24:08', '2020-07-16 17:24:08', 'Vlad Vladik', 'admin@admin.com', '123412315123', 'wfsdfsdf', 'asdasd', 0),
	(31, '2020-07-16 17:24:09', '2020-07-16 17:24:09', 'Vlad Vladik', 'admin@admin.com', '123412315123', 'wfsdfsdf', 'asdasd', 0),
	(32, '2020-07-16 17:24:09', '2020-07-16 17:24:09', 'Vlad Vladik', 'admin@admin.com', '123412315123', 'wfsdfsdf', 'asdasd', 0),
	(33, '2020-07-16 17:24:21', '2020-07-16 17:24:21', 'Vlad Vladik', 'admin@admin.com', '123412315123', 'wfsdfsdf', 'asdasd', 0),
	(34, '2020-07-16 17:24:25', '2020-07-16 17:24:25', 'Vlad Vladik', 'admin@admin.com', '123412315123', 'wfsdfsdf', 'asdasd', 0),
	(35, '2020-07-16 17:24:25', '2020-07-16 17:24:25', 'Vlad Vladik', 'admin@admin.com', '123412315123', 'wfsdfsdf', 'asdasd', 0),
	(36, '2020-07-16 17:24:25', '2020-07-16 17:24:25', 'Vlad Vladik', 'admin@admin.com', '123412315123', 'wfsdfsdf', 'asdasd', 0),
	(37, '2020-07-16 17:25:34', '2020-07-16 17:25:34', 'sweet', 'kahovskaia@gmail.com', '123412315123', 'wfsdfsdf', 'asdaf', 0),
	(38, '2020-07-16 17:25:35', '2020-07-16 17:25:35', 'sweet', 'kahovskaia@gmail.com', '123412315123', 'wfsdfsdf', 'asdaf', 0),
	(39, '2020-07-16 17:25:35', '2020-07-16 17:25:35', 'sweet', 'kahovskaia@gmail.com', '123412315123', 'wfsdfsdf', 'asdaf', 0),
	(40, '2020-07-16 17:25:35', '2020-07-16 17:25:35', 'sweet', 'kahovskaia@gmail.com', '123412315123', 'wfsdfsdf', 'asdaf', 0),
	(41, '2020-07-16 17:25:35', '2020-07-16 17:25:35', 'sweet', 'kahovskaia@gmail.com', '123412315123', 'wfsdfsdf', 'asdaf', 0),
	(42, '2020-07-16 17:26:31', '2020-07-16 17:26:31', 'Влад', 'kahovskaia@gmail.com', '123412315123', 'wfsdfsdf', 'sdasads', 0),
	(43, '2020-07-16 17:26:34', '2020-07-16 17:26:34', 'Влад', 'kahovskaia@gmail.com', '123412315123', 'wfsdfsdf', 'sdasads', 0),
	(44, '2020-07-16 17:26:36', '2020-07-16 17:26:36', 'Влад', 'kahovskaia@gmail.com', '123412315123', 'wfsdfsdf', 'sdasads', 0),
	(45, '2020-07-16 17:26:37', '2020-07-16 17:26:37', 'Влад', 'kahovskaia@gmail.com', '123412315123', 'wfsdfsdf', 'sdasads', 0),
	(46, '2020-07-16 17:26:43', '2020-07-16 17:26:43', 'Влад', 'kahovskaia@gmail.com', '123412315123', 'wfsdfsdf', 'sdasads', 0),
	(47, '2020-07-16 17:26:43', '2020-07-16 17:26:43', 'Влад', 'kahovskaia@gmail.com', '123412315123', 'wfsdfsdf', 'sdasads', 0),
	(48, '2020-07-16 17:26:44', '2020-07-16 17:26:44', 'Влад', 'kahovskaia@gmail.com', '123412315123', 'wfsdfsdf', 'sdasads', 0),
	(49, '2020-07-16 17:26:46', '2020-07-16 17:26:46', 'Влад', 'kahovskaia@gmail.com', '123412315123', 'wfsdfsdf', 'sdasads', 0),
	(50, '2020-07-16 17:26:46', '2020-07-16 17:26:46', 'Влад', 'kahovskaia@gmail.com', '123412315123', 'wfsdfsdf', 'sdasads', 0),
	(51, '2020-07-16 17:26:46', '2020-07-16 17:26:46', 'Влад', 'kahovskaia@gmail.com', '123412315123', 'wfsdfsdf', 'sdasads', 0),
	(52, '2020-07-16 17:26:50', '2020-07-16 17:26:50', 'Влад', 'kahovskaia@gmail.com', '123412315123', 'wfsdfsdf', 'sdasads', 0),
	(53, '2020-07-16 17:26:54', '2020-07-16 17:26:54', 'Влад', 'kahovskaia@gmail.com', '123412315123', 'wfsdfsdf', 'sdasads', 0),
	(54, '2020-07-16 17:26:57', '2020-07-16 17:26:57', 'Влад', 'kahovskaia@gmail.com', '123412315123', 'wfsdfsdf', 'sdasads', 0),
	(55, '2020-07-16 17:27:04', '2020-07-16 17:27:04', 'Влад', 'kahovskaia@gmail.com', '123412315123', 'wfsdfsdf', 'sdasads', 0),
	(56, '2020-07-16 17:27:37', '2020-07-16 17:27:37', 'sweet', 'admin@admin.com', '123412315123', 'wfsdfsdf', 'asdad', 0),
	(57, '2020-07-16 17:27:40', '2020-07-16 17:27:40', 'sweet', 'admin@admin.com', '123412315123', 'wfsdfsdf', 'asdad', 0),
	(58, '2020-07-16 17:27:43', '2020-07-16 17:27:43', 'sweet', 'admin@admin.com', '123412315123', 'wfsdfsdf', 'asdad', 0),
	(59, '2020-07-16 17:27:46', '2020-07-16 17:27:46', 'sweet', 'admin@admin.com', '123412315123', 'wfsdfsdf', 'asdad', 0),
	(60, '2020-07-16 17:27:58', '2020-07-16 17:27:58', 'sweet', 'admin@admin.com', '123412315123', 'wfsdfsdf', 'asdad', 0),
	(61, '2020-07-16 17:28:01', '2020-07-16 17:28:01', 'sweet', 'admin@admin.com', '123412315123', 'wfsdfsdf', 'asdad', 0),
	(62, '2020-07-17 11:36:19', '2020-07-17 11:36:19', 'Vlad Vladik', 'kahovskaia@gmail.com', '123412315123', 'wfsdfsdf', 'asdad', 0),
	(63, '2020-07-17 11:39:20', '2020-07-17 11:39:20', 'leaf', 'kahovskaia@gmail.com', '123412315123', 'wfsdfsdf', 'jhgjjhj', 0),
	(64, '2020-07-17 11:40:50', '2020-07-17 11:40:50', 'asdasd', 'kahovskaia@gmail.com', '123412315123', 'adasd', 'asdads', 0),
	(65, '2020-07-17 11:58:08', '2020-07-17 11:58:08', 'Vlad Vladik', 'admin@admin.com', '+37377892055', 'wfsdfsdf', '3213', 0),
	(66, '2020-07-17 11:58:48', '2020-07-17 11:58:48', 'Влад', 'kahovskaia@gmail.com', '3123', 'wfsdfsdf', '123124rr12', 0),
	(67, '2020-07-17 12:01:16', '2020-07-17 12:01:16', 'Влад', 'kahovskaia@gmail.com', '123412315123', 'wfsdfsdf', 'fsdfsdfsdfsdfsdfsfsdfsdfsdfsdfsfsfsfsdfsdfsdfsdfsdfsfsdfsdfsdfsdfsfsfsfsdfsdfsdfsdfsdfsfsdfsdfsdfsdfsfsfsfsdfsdfsdfsdfsdfsfsdfsdfsdfsdfsfsfsfsdfsdfsdfsdfsdfsfsdfsdfsdfsdfsfsfsfsdfsdfsdfsdfsdfsfsdfsdfsdfsdfsfsfsfsdfsdfsdfsdfsdfsfsdfsdfsdfsdfsfsfsfsdfsdfsdfsdfsdfsfsdfsdfsdfsdfsfsfsfsdfsdfsdfsdfsdfsfsdfsdfsdfsdfsfsfs', 0),
	(68, '2020-07-17 12:10:17', '2020-07-17 12:10:17', 'Влад', 'kahovskaia@gmail.com', '123412315123', 'wfsdfsdf', 'fsdfsdfsdfsdfsdfsfsdfsdfsdfsdfsfsfsfsdfsdfsdfsdfsdfsfsdfsdfsdfsdfsfsfsfsdfsdfsdfsdfsdfsfsdfsdfsdfsdfsfsfsfsdfsdfsdfsdfsdfsfsdfsdfsdfsdfsfsfsfsdfsdfsdfsdfsdfsfsdfsdfsdfsdfsfsfsfsdfsdfsdfsdfsdfsfsdfsdfsdfsdfsfsfsfsdfsdfsdfsdfsdfsfsdfsdfsdfsdfsfsfsfsdfsdfsdfsdfsdfsfsdfsdfsdfsdfsfsfsfsdfsdfsdfsdfsdfsfsdfsdfsdfsdfsfsfs', 0),
	(69, '2020-07-17 12:10:20', '2020-07-17 12:10:20', 'Влад', 'kahovskaia@gmail.com', '123412315123', 'wfsdfsdf', 'fsdfsdfsdfsdfsdfsfsdfsdfsdfsdfsfsfsfsdfsdfsdfsdfsdfsfsdfsdfsdfsdfsfsfsfsdfsdfsdfsdfsdfsfsdfsdfsdfsdfsfsfsfsdfsdfsdfsdfsdfsfsdfsdfsdfsdfsfsfsfsdfsdfsdfsdfsdfsfsdfsdfsdfsdfsfsfsfsdfsdfsdfsdfsdfsfsdfsdfsdfsdfsfsfsfsdfsdfsdfsdfsdfsfsdfsdfsdfsdfsfsfsfsdfsdfsdfsdfsdfsfsdfsdfsdfsdfsfsfsfsdfsdfsdfsdfsdfsfsdfsdfsdfsdfsfsfs', 0),
	(70, '2020-07-17 12:11:52', '2020-07-17 12:11:52', 'adsad', 'kahovskaia@gmail.com', '231231', 'asda', 'sdasdd', 0),
	(71, '2020-07-17 12:11:57', '2020-07-17 12:11:57', 'leaf', 'kahovskaia@gmail.com', '+37377892055', 'wfsdfsdf', 'asdadsasdad', 0),
	(72, '2020-07-17 12:12:06', '2020-07-17 12:12:06', 'leaf', 'kahovskaia@gmail.com', '+37377892055', 'wfsdfsdf', 'asdadsasdad', 0),
	(73, '2020-07-17 12:14:17', '2020-07-17 12:14:17', 'Vlad Vladik', 'kahovskaia@gmail.com', '123412315123', 'wfsdfsdf', 'sadads', 0),
	(74, '2020-07-17 12:14:21', '2020-07-17 12:14:21', 'Vlad Vladik', 'kahovskaia@gmail.com', '123412315123', 'wfsdfsdf', 'sadads', 0),
	(75, '2020-07-17 12:14:24', '2020-07-17 12:14:24', 'Vlad Vladik', 'kahovskaia@gmail.com', '123412315123', 'wfsdfsdf', 'sadads', 0),
	(76, '2020-07-17 12:14:38', '2020-07-17 12:14:38', 'Vlad Vladik', 'kahovskaia@gmail.com', '123412315123', 'wfsdfsdf', 'sadads', 0),
	(77, '2020-07-17 12:14:42', '2020-07-17 12:14:42', 'Vlad Vladik', 'kahovskaia@gmail.com', '123412315123', 'wfsdfsdf', 'sadads', 0),
	(78, '2020-07-17 12:14:46', '2020-07-17 12:14:46', 'Vlad Vladik', 'kahovskaia@gmail.com', '123412315123', 'wfsdfsdf', 'sadads', 0),
	(79, '2020-07-17 12:15:01', '2020-07-17 12:15:01', 'Vlad Vladik', 'kahovskaia@gmail.com', '123412315123', 'wfsdfsdf', 'sadads', 0),
	(80, '2020-07-17 12:15:04', '2020-07-17 12:15:04', 'Vlad Vladik', 'kahovskaia@gmail.com', '123412315123', 'wfsdfsdf', 'sadads', 0),
	(81, '2020-07-17 12:16:46', '2020-07-17 12:16:46', 'Влад', 'kahovskaia@gmail.com', '123412315123', 'wfsdfsdf', '213123', 0),
	(82, '2020-07-17 12:16:46', '2020-07-17 12:16:46', 'Влад', 'kahovskaia@gmail.com', '123412315123', 'wfsdfsdf', '213123', 0),
	(83, '2020-07-17 12:16:46', '2020-07-17 12:16:46', 'Влад', 'kahovskaia@gmail.com', '123412315123', 'wfsdfsdf', '213123', 0),
	(84, '2020-07-17 12:16:46', '2020-07-17 12:16:46', 'Влад', 'kahovskaia@gmail.com', '123412315123', 'wfsdfsdf', '213123', 0),
	(85, '2020-07-17 12:16:46', '2020-07-17 12:16:46', 'Влад', 'kahovskaia@gmail.com', '123412315123', 'wfsdfsdf', '213123', 0),
	(86, '2020-07-17 12:16:47', '2020-07-17 12:16:47', 'Влад', 'kahovskaia@gmail.com', '123412315123', 'wfsdfsdf', '213123', 0),
	(87, '2020-07-17 12:16:47', '2020-07-17 12:16:47', 'Влад', 'kahovskaia@gmail.com', '123412315123', 'wfsdfsdf', '213123', 0),
	(88, '2020-07-17 12:16:47', '2020-07-17 12:16:47', 'Влад', 'kahovskaia@gmail.com', '123412315123', 'wfsdfsdf', '213123', 0),
	(89, '2020-07-17 12:17:09', '2020-07-17 12:17:09', 'Vlad Vladik', 'admin@admin.com', '+37377892055', 'wfsdfsdf', 'asdadasd', 0),
	(90, '2020-07-17 12:23:39', '2020-07-17 12:23:39', 'asdasd', 'asdasd@mail.ru', '123412315123', 'wfsdfsdf', 'asdasdad', 0),
	(91, '2020-07-17 12:43:05', '2020-07-17 12:43:05', 'asdasd', 'asdasd@mail.ru', '123412315123', 'wfsdfsdf', 'asdasdad', 0),
	(92, '2020-07-17 12:43:16', '2020-07-17 12:43:16', 'Влад', 'admin@admin.com', '123412315123', 'wfsdfsdf', 'adsad', 0),
	(93, '2020-07-17 12:43:20', '2020-07-17 12:43:20', 'Влад', 'admin@admin.com', '123412315123', 'wfsdfsdf', 'adsad', 0),
	(94, '2020-07-17 12:43:23', '2020-07-17 12:43:23', 'asdasd', 'asdasd@mail.ru', '123412315123', 'wfsdfsdf', 'asdasdad', 0),
	(95, '2020-07-17 13:42:33', '2020-07-17 13:42:33', 'Влад', 'kahovskaia2@gmail.com', '123412315123', 'czxcz', 'asdasd', 0),
	(96, '2020-07-17 13:42:36', '2020-07-17 13:42:36', 'Влад', 'kahovskaia2@gmail.com', '123412315123', 'czxcz', 'asdasd', 0);
/*!40000 ALTER TABLE `feedback` ENABLE KEYS */;

-- Дамп структуры для таблица organic.menus
CREATE TABLE IF NOT EXISTS `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы organic.menus: ~2 rows (приблизительно)
DELETE FROM `menus`;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
	(1, 'admin', '2020-07-14 08:54:15', '2020-07-14 08:54:15'),
	(2, 'Menu', '2020-07-20 13:48:05', '2020-07-20 13:48:05');
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;

-- Дамп структуры для таблица organic.menu_items
CREATE TABLE IF NOT EXISTS `menu_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `menu_items_menu_id_foreign` (`menu_id`),
  CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы organic.menu_items: ~44 rows (приблизительно)
DELETE FROM `menu_items`;
/*!40000 ALTER TABLE `menu_items` DISABLE KEYS */;
INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
	(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, 5, 2, '2020-07-14 08:54:15', '2020-07-22 12:45:45', 'voyager.dashboard', NULL),
	(2, 1, 'Media', '', '_self', 'voyager-images', NULL, 5, 1, '2020-07-14 08:54:15', '2020-07-22 12:45:45', 'voyager.media.index', NULL),
	(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 1, '2020-07-14 08:54:15', '2020-08-06 12:57:50', 'voyager.roles.index', NULL),
	(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 2, '2020-07-14 08:54:15', '2020-08-06 12:57:50', NULL, NULL),
	(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 6, '2020-07-14 08:54:15', '2020-07-22 12:45:45', 'voyager.menus.index', NULL),
	(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 3, '2020-07-14 08:54:15', '2020-07-22 12:45:45', 'voyager.database.index', NULL),
	(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 4, '2020-07-14 08:54:16', '2020-07-22 12:45:45', 'voyager.compass.index', NULL),
	(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 5, '2020-07-14 08:54:16', '2020-07-22 12:45:45', 'voyager.bread.index', NULL),
	(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, 5, 8, '2020-07-14 08:54:16', '2020-08-06 13:03:47', 'voyager.settings.index', NULL),
	(11, 1, 'Advantages', '', '_self', NULL, NULL, 35, 3, '2020-07-14 09:04:08', '2020-07-21 15:11:10', 'voyager.advantages.index', NULL),
	(12, 1, 'Advertisements', '', '_self', NULL, NULL, 36, 1, '2020-07-14 09:08:20', '2020-07-21 15:09:16', 'voyager.advertisements.index', NULL),
	(13, 1, 'Authors', '', '_self', NULL, NULL, 40, 1, '2020-07-14 09:11:06', '2020-07-22 12:39:05', 'voyager.authors.index', NULL),
	(14, 1, 'Blog Categories', '', '_self', NULL, NULL, 38, 1, '2020-07-14 09:11:59', '2020-07-22 12:34:05', 'voyager.blog-categories.index', NULL),
	(15, 1, 'Blog Reviews', '', '_self', NULL, NULL, 39, 1, '2020-07-14 09:21:26', '2020-07-22 12:36:16', 'voyager.blog-reviews.index', NULL),
	(16, 1, 'Blogs', '', '_self', NULL, NULL, 37, 1, '2020-07-14 09:23:51', '2020-07-22 12:31:07', 'voyager.blogs.index', NULL),
	(17, 1, 'Deliveries', '', '_self', NULL, NULL, 36, 2, '2020-07-14 09:26:24', '2020-07-22 12:30:56', 'voyager.deliveries.index', NULL),
	(18, 1, 'Faqs', '', '_self', NULL, NULL, 40, 3, '2020-07-14 09:32:12', '2020-07-22 12:39:14', 'voyager.faqs.index', NULL),
	(19, 1, 'Feedback', '', '_self', NULL, NULL, 41, 2, '2020-07-14 09:33:43', '2020-08-06 12:57:52', 'voyager.feedback.index', NULL),
	(20, 1, 'Partners', '', '_self', NULL, NULL, 35, 5, '2020-07-14 09:36:57', '2020-07-21 15:11:10', 'voyager.partners.index', NULL),
	(21, 1, 'Product Categories', '', '_self', NULL, NULL, 38, 2, '2020-07-14 09:39:13', '2020-07-22 12:34:10', 'voyager.product-categories.index', NULL),
	(22, 1, 'Products', '', '_self', NULL, NULL, 37, 2, '2020-07-14 09:42:18', '2020-07-22 12:31:19', 'voyager.products.index', NULL),
	(23, 1, 'Sales', '', '_self', NULL, NULL, 35, 2, '2020-07-14 09:55:47', '2020-07-21 15:11:10', 'voyager.sales.index', NULL),
	(24, 1, 'Subscriptions', '', '_self', NULL, NULL, 41, 1, '2020-07-14 10:00:10', '2020-07-22 12:40:21', 'voyager.subscriptions.index', NULL),
	(26, 1, 'Testimonials', '', '_self', NULL, NULL, 40, 2, '2020-07-14 10:03:05', '2020-07-22 12:39:10', 'voyager.testimonials.index', NULL),
	(27, 1, 'Workers', '', '_self', NULL, NULL, 35, 4, '2020-07-14 10:05:36', '2020-07-21 15:11:10', 'voyager.workers.index', NULL),
	(28, 1, 'Products Reviews', '', '_self', NULL, '#000000', 39, 2, '2020-07-14 10:08:17', '2020-07-22 12:36:40', 'voyager.reviews.index', 'null'),
	(29, 1, 'Sliders', '', '_self', NULL, NULL, 35, 1, '2020-07-15 12:33:13', '2020-07-21 15:11:10', 'voyager.sliders.index', NULL),
	(35, 1, 'Home Page', '', '_self', 'voyager-folder', '#000000', NULL, 3, '2020-07-21 15:01:23', '2020-08-06 12:57:50', NULL, NULL),
	(36, 1, 'About Us', '', '_self', 'voyager-documentation', '#000000', NULL, 4, '2020-07-21 15:07:16', '2020-08-06 12:57:50', NULL, NULL),
	(37, 1, 'Products and Blogs', '', '_self', 'voyager-shop', '#000000', NULL, 5, '2020-07-22 12:30:21', '2020-08-06 12:57:50', NULL, NULL),
	(38, 1, 'Categories', '', '_self', 'voyager-tag', '#000000', NULL, 6, '2020-07-22 12:31:51', '2020-08-06 12:57:50', NULL, NULL),
	(39, 1, 'Reviews', '', '_self', 'voyager-logbook', '#000000', NULL, 7, '2020-07-22 12:35:39', '2020-08-06 12:57:50', NULL, NULL),
	(40, 1, 'Any', '', '_self', 'voyager-bookmark', '#000000', NULL, 8, '2020-07-22 12:37:27', '2020-08-06 12:57:50', NULL, NULL),
	(41, 1, 'Form subcribe', '', '_self', 'voyager-bubble', '#000000', NULL, 9, '2020-07-22 12:40:11', '2020-08-06 12:57:51', NULL, NULL),
	(47, 2, 'Testimonials', '', '_self', NULL, '#000000', 46, 1, '2020-07-23 09:12:09', '2020-07-23 09:13:44', NULL, ''),
	(48, 2, 'Faq\'s', '', '_self', NULL, '#000000', 46, 2, '2020-07-23 09:12:32', '2020-07-23 09:14:39', NULL, ''),
	(50, 2, 'Home', '/', '_self', NULL, '#000000', NULL, 1, '2020-07-23 13:13:04', '2020-07-23 15:20:14', NULL, ''),
	(51, 2, 'About Us', '/about-us', '_self', NULL, '#000000', NULL, 2, '2020-07-23 13:15:17', '2020-07-23 13:22:26', NULL, ''),
	(52, 2, 'Products', '/shop', '_self', NULL, '#000000', NULL, 3, '2020-07-23 13:15:45', '2020-07-23 13:36:03', NULL, ''),
	(53, 2, 'News', '/blog', '_self', NULL, '#000000', NULL, 4, '2020-07-23 13:20:28', '2020-07-23 13:36:03', NULL, ''),
	(55, 2, 'Testimonials', '/testimonial', '_self', NULL, '#000000', NULL, 5, '2020-07-23 13:21:28', '2020-07-24 10:30:39', NULL, ''),
	(56, 2, 'FAQ', '/faq', '_self', NULL, '#000000', NULL, 6, '2020-07-23 13:21:52', '2020-07-24 10:30:42', NULL, ''),
	(57, 2, 'Contact', '/contact', '_self', NULL, '#000000', NULL, 8, '2020-07-23 13:22:21', '2020-07-24 10:30:42', NULL, ''),
	(60, 1, 'Pages', '', '_self', NULL, '#000000', 5, 7, '2020-08-06 12:57:04', '2020-08-06 13:03:47', 'voyager.pages.index', NULL);
/*!40000 ALTER TABLE `menu_items` ENABLE KEYS */;

-- Дамп структуры для таблица organic.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы organic.migrations: ~45 rows (приблизительно)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2016_01_01_000000_add_voyager_user_fields', 1),
	(4, '2016_01_01_000000_create_data_types_table', 1),
	(5, '2016_05_19_173453_create_menu_table', 1),
	(6, '2016_10_21_190000_create_roles_table', 1),
	(7, '2016_10_21_190000_create_settings_table', 1),
	(8, '2016_11_30_135954_create_permission_table', 1),
	(9, '2016_11_30_141208_create_permission_role_table', 1),
	(10, '2016_12_26_201236_data_types__add__server_side', 1),
	(11, '2017_01_13_000000_add_route_to_menu_items_table', 1),
	(12, '2017_01_14_005015_create_translations_table', 1),
	(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
	(14, '2017_03_06_000000_add_controller_to_data_types_table', 1),
	(15, '2017_04_21_000000_add_order_to_data_rows_table', 1),
	(16, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
	(17, '2017_08_05_000000_add_group_to_settings_table', 1),
	(18, '2017_11_26_013050_add_user_role_relationship', 1),
	(19, '2017_11_26_015000_create_user_roles_table', 1),
	(20, '2018_03_11_000000_add_user_settings', 1),
	(21, '2018_03_14_000000_add_details_to_data_types_table', 1),
	(22, '2018_03_16_000000_make_settings_value_nullable', 1),
	(23, '2019_08_19_000000_create_failed_jobs_table', 1),
	(24, '2020_07_08_143649_create_sales_table', 1),
	(25, '2020_07_08_143717_create_tags_table', 1),
	(26, '2020_07_08_143732_create_product_categories_table', 1),
	(27, '2020_07_08_143733_create_products_table', 1),
	(28, '2020_07_08_143804_create_authors_table', 1),
	(29, '2020_07_08_143805_create_blog_categories_table', 1),
	(30, '2020_07_08_143806_create_blogs_table', 1),
	(31, '2020_07_08_143807_create_reviews_table', 1),
	(32, '2020_07_08_143821_create_advantages_table', 1),
	(33, '2020_07_08_144008_create_workers_table', 1),
	(34, '2020_07_08_144025_create_testimonials_table', 1),
	(35, '2020_07_08_144041_create_partners_table', 1),
	(36, '2020_07_08_144055_create_subscriptions_table', 1),
	(37, '2020_07_08_144111_create_advertisements_table', 1),
	(38, '2020_07_08_144125_create_deliveries_table', 1),
	(39, '2020_07_08_144140_create_feedback_table', 1),
	(40, '2020_07_08_144154_create_faqs_table', 1),
	(41, '2020_07_09_065135_create_product_tags_table', 1),
	(42, '2020_07_09_133745_create_blogs_reviews_table', 1),
	(43, '2020_07_15_093806_create_recents_table', 2),
	(44, '2020_07_15_113624_create_sliders_table', 2),
	(45, '2020_08_06_080243_create_pages_table', 3);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Дамп структуры для таблица organic.pages
CREATE TABLE IF NOT EXISTS `pages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name_page` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы organic.pages: ~7 rows (приблизительно)
DELETE FROM `pages`;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` (`id`, `name_page`, `title`, `slug`, `meta_title`, `meta_description`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'home', 'Title Home', NULL, 'meta data', 'meta data', '1', '2020-08-06 14:57:42', '2020-08-06 14:57:44'),
	(2, 'about_as', 'Title Home', NULL, 'meta data', 'meta data', '1', '2020-08-06 14:57:42', '2020-08-06 14:57:44'),
	(3, 'contacts', 'Title Home', NULL, 'meta data', 'meta data', '1', '2020-08-06 14:57:42', '2020-08-06 14:57:44'),
	(4, 'faq', 'Title Home', NULL, 'meta data', 'meta data', '1', '2020-08-06 14:57:42', '2020-08-06 14:57:44'),
	(5, 'testimonial', 'Title Home', NULL, 'meta data', 'meta data', '1', '2020-08-06 14:57:42', '2020-08-06 14:57:44'),
	(6, 'blogs', 'Title Home', NULL, 'meta data', 'meta data', '1', '2020-08-06 14:57:42', '2020-08-06 14:57:44'),
	(7, 'products', 'Title Home', NULL, 'meta data', 'meta data', '1', '2020-08-06 14:57:42', '2020-08-06 14:57:44');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;

-- Дамп структуры для таблица organic.partners
CREATE TABLE IF NOT EXISTS `partners` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` tinyint(4) NOT NULL DEFAULT 0,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы organic.partners: ~8 rows (приблизительно)
DELETE FROM `partners`;
/*!40000 ALTER TABLE `partners` DISABLE KEYS */;
INSERT INTO `partners` (`id`, `created_at`, `updated_at`, `image`, `title`, `order`, `status`) VALUES
	(1, '2020-07-14 19:07:37', '2020-07-14 19:07:37', 'partners\\July2020\\VmdOIblfOAnfFC1aDdcR.png', 'leaf', 1, 1),
	(2, '2020-07-14 19:08:19', '2020-07-14 19:08:19', 'partners\\July2020\\Wn4iO6Llq2AASfDEvQkd.png', 'sweet', 2, 1),
	(3, '2020-07-14 19:08:37', '2020-07-14 19:08:37', 'partners\\July2020\\unWfG5X5vhPgideMfQKc.png', 'coganid', 3, 1),
	(4, '2020-07-14 19:08:52', '2020-07-14 19:08:52', 'partners\\July2020\\YvOgHbxyNs4pFoG3BGsb.png', 'true', 4, 1),
	(5, '2020-07-14 19:09:05', '2020-07-14 19:09:05', 'partners\\July2020\\MiRp3U6y6DjvCgbuaoAj.png', 'food', 5, 1),
	(6, '2020-07-14 19:09:27', '2020-07-14 19:09:27', 'partners\\July2020\\PbJslk5GnxItybruQ2D0.png', 'organic', 6, 1),
	(7, '2020-07-14 19:10:03', '2020-07-14 19:10:03', 'partners\\July2020\\gShjVdIhRFmyh8qGMnKe.png', 'taste of food', 7, 1),
	(8, '2020-07-14 19:10:41', '2020-07-14 19:10:41', 'partners\\July2020\\BgqznIIZFkzV3elPvEN2.png', 'austrelian', 8, 1);
/*!40000 ALTER TABLE `partners` ENABLE KEYS */;

-- Дамп структуры для таблица organic.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы organic.password_resets: ~0 rows (приблизительно)
DELETE FROM `password_resets`;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Дамп структуры для таблица organic.permissions
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_key_index` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=126 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы organic.permissions: ~125 rows (приблизительно)
DELETE FROM `permissions`;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
	(1, 'browse_admin', NULL, '2020-07-14 08:54:16', '2020-07-14 08:54:16'),
	(2, 'browse_bread', NULL, '2020-07-14 08:54:16', '2020-07-14 08:54:16'),
	(3, 'browse_database', NULL, '2020-07-14 08:54:16', '2020-07-14 08:54:16'),
	(4, 'browse_media', NULL, '2020-07-14 08:54:16', '2020-07-14 08:54:16'),
	(5, 'browse_compass', NULL, '2020-07-14 08:54:17', '2020-07-14 08:54:17'),
	(6, 'browse_menus', 'menus', '2020-07-14 08:54:17', '2020-07-14 08:54:17'),
	(7, 'read_menus', 'menus', '2020-07-14 08:54:17', '2020-07-14 08:54:17'),
	(8, 'edit_menus', 'menus', '2020-07-14 08:54:17', '2020-07-14 08:54:17'),
	(9, 'add_menus', 'menus', '2020-07-14 08:54:17', '2020-07-14 08:54:17'),
	(10, 'delete_menus', 'menus', '2020-07-14 08:54:17', '2020-07-14 08:54:17'),
	(11, 'browse_roles', 'roles', '2020-07-14 08:54:17', '2020-07-14 08:54:17'),
	(12, 'read_roles', 'roles', '2020-07-14 08:54:17', '2020-07-14 08:54:17'),
	(13, 'edit_roles', 'roles', '2020-07-14 08:54:17', '2020-07-14 08:54:17'),
	(14, 'add_roles', 'roles', '2020-07-14 08:54:17', '2020-07-14 08:54:17'),
	(15, 'delete_roles', 'roles', '2020-07-14 08:54:18', '2020-07-14 08:54:18'),
	(16, 'browse_users', 'users', '2020-07-14 08:54:18', '2020-07-14 08:54:18'),
	(17, 'read_users', 'users', '2020-07-14 08:54:18', '2020-07-14 08:54:18'),
	(18, 'edit_users', 'users', '2020-07-14 08:54:18', '2020-07-14 08:54:18'),
	(19, 'add_users', 'users', '2020-07-14 08:54:18', '2020-07-14 08:54:18'),
	(20, 'delete_users', 'users', '2020-07-14 08:54:18', '2020-07-14 08:54:18'),
	(21, 'browse_settings', 'settings', '2020-07-14 08:54:18', '2020-07-14 08:54:18'),
	(22, 'read_settings', 'settings', '2020-07-14 08:54:18', '2020-07-14 08:54:18'),
	(23, 'edit_settings', 'settings', '2020-07-14 08:54:18', '2020-07-14 08:54:18'),
	(24, 'add_settings', 'settings', '2020-07-14 08:54:18', '2020-07-14 08:54:18'),
	(25, 'delete_settings', 'settings', '2020-07-14 08:54:18', '2020-07-14 08:54:18'),
	(26, 'browse_advantages', 'advantages', '2020-07-14 09:04:07', '2020-07-14 09:04:07'),
	(27, 'read_advantages', 'advantages', '2020-07-14 09:04:07', '2020-07-14 09:04:07'),
	(28, 'edit_advantages', 'advantages', '2020-07-14 09:04:07', '2020-07-14 09:04:07'),
	(29, 'add_advantages', 'advantages', '2020-07-14 09:04:07', '2020-07-14 09:04:07'),
	(30, 'delete_advantages', 'advantages', '2020-07-14 09:04:08', '2020-07-14 09:04:08'),
	(31, 'browse_advertisements', 'advertisements', '2020-07-14 09:08:20', '2020-07-14 09:08:20'),
	(32, 'read_advertisements', 'advertisements', '2020-07-14 09:08:20', '2020-07-14 09:08:20'),
	(33, 'edit_advertisements', 'advertisements', '2020-07-14 09:08:20', '2020-07-14 09:08:20'),
	(34, 'add_advertisements', 'advertisements', '2020-07-14 09:08:20', '2020-07-14 09:08:20'),
	(35, 'delete_advertisements', 'advertisements', '2020-07-14 09:08:20', '2020-07-14 09:08:20'),
	(36, 'browse_authors', 'authors', '2020-07-14 09:11:06', '2020-07-14 09:11:06'),
	(37, 'read_authors', 'authors', '2020-07-14 09:11:06', '2020-07-14 09:11:06'),
	(38, 'edit_authors', 'authors', '2020-07-14 09:11:06', '2020-07-14 09:11:06'),
	(39, 'add_authors', 'authors', '2020-07-14 09:11:06', '2020-07-14 09:11:06'),
	(40, 'delete_authors', 'authors', '2020-07-14 09:11:06', '2020-07-14 09:11:06'),
	(41, 'browse_blog_categories', 'blog_categories', '2020-07-14 09:11:58', '2020-07-14 09:11:58'),
	(42, 'read_blog_categories', 'blog_categories', '2020-07-14 09:11:58', '2020-07-14 09:11:58'),
	(43, 'edit_blog_categories', 'blog_categories', '2020-07-14 09:11:58', '2020-07-14 09:11:58'),
	(44, 'add_blog_categories', 'blog_categories', '2020-07-14 09:11:58', '2020-07-14 09:11:58'),
	(45, 'delete_blog_categories', 'blog_categories', '2020-07-14 09:11:58', '2020-07-14 09:11:58'),
	(46, 'browse_blog_reviews', 'blog_reviews', '2020-07-14 09:21:25', '2020-07-14 09:21:25'),
	(47, 'read_blog_reviews', 'blog_reviews', '2020-07-14 09:21:25', '2020-07-14 09:21:25'),
	(48, 'edit_blog_reviews', 'blog_reviews', '2020-07-14 09:21:26', '2020-07-14 09:21:26'),
	(49, 'add_blog_reviews', 'blog_reviews', '2020-07-14 09:21:26', '2020-07-14 09:21:26'),
	(50, 'delete_blog_reviews', 'blog_reviews', '2020-07-14 09:21:26', '2020-07-14 09:21:26'),
	(51, 'browse_blogs', 'blogs', '2020-07-14 09:23:51', '2020-07-14 09:23:51'),
	(52, 'read_blogs', 'blogs', '2020-07-14 09:23:51', '2020-07-14 09:23:51'),
	(53, 'edit_blogs', 'blogs', '2020-07-14 09:23:51', '2020-07-14 09:23:51'),
	(54, 'add_blogs', 'blogs', '2020-07-14 09:23:51', '2020-07-14 09:23:51'),
	(55, 'delete_blogs', 'blogs', '2020-07-14 09:23:51', '2020-07-14 09:23:51'),
	(56, 'browse_deliveries', 'deliveries', '2020-07-14 09:26:24', '2020-07-14 09:26:24'),
	(57, 'read_deliveries', 'deliveries', '2020-07-14 09:26:24', '2020-07-14 09:26:24'),
	(58, 'edit_deliveries', 'deliveries', '2020-07-14 09:26:24', '2020-07-14 09:26:24'),
	(59, 'add_deliveries', 'deliveries', '2020-07-14 09:26:24', '2020-07-14 09:26:24'),
	(60, 'delete_deliveries', 'deliveries', '2020-07-14 09:26:24', '2020-07-14 09:26:24'),
	(61, 'browse_faqs', 'faqs', '2020-07-14 09:32:11', '2020-07-14 09:32:11'),
	(62, 'read_faqs', 'faqs', '2020-07-14 09:32:11', '2020-07-14 09:32:11'),
	(63, 'edit_faqs', 'faqs', '2020-07-14 09:32:12', '2020-07-14 09:32:12'),
	(64, 'add_faqs', 'faqs', '2020-07-14 09:32:12', '2020-07-14 09:32:12'),
	(65, 'delete_faqs', 'faqs', '2020-07-14 09:32:12', '2020-07-14 09:32:12'),
	(66, 'browse_feedback', 'feedback', '2020-07-14 09:33:43', '2020-07-14 09:33:43'),
	(67, 'read_feedback', 'feedback', '2020-07-14 09:33:43', '2020-07-14 09:33:43'),
	(68, 'edit_feedback', 'feedback', '2020-07-14 09:33:43', '2020-07-14 09:33:43'),
	(69, 'add_feedback', 'feedback', '2020-07-14 09:33:43', '2020-07-14 09:33:43'),
	(70, 'delete_feedback', 'feedback', '2020-07-14 09:33:43', '2020-07-14 09:33:43'),
	(71, 'browse_partners', 'partners', '2020-07-14 09:36:57', '2020-07-14 09:36:57'),
	(72, 'read_partners', 'partners', '2020-07-14 09:36:57', '2020-07-14 09:36:57'),
	(73, 'edit_partners', 'partners', '2020-07-14 09:36:57', '2020-07-14 09:36:57'),
	(74, 'add_partners', 'partners', '2020-07-14 09:36:57', '2020-07-14 09:36:57'),
	(75, 'delete_partners', 'partners', '2020-07-14 09:36:57', '2020-07-14 09:36:57'),
	(76, 'browse_product_categories', 'product_categories', '2020-07-14 09:39:13', '2020-07-14 09:39:13'),
	(77, 'read_product_categories', 'product_categories', '2020-07-14 09:39:13', '2020-07-14 09:39:13'),
	(78, 'edit_product_categories', 'product_categories', '2020-07-14 09:39:13', '2020-07-14 09:39:13'),
	(79, 'add_product_categories', 'product_categories', '2020-07-14 09:39:13', '2020-07-14 09:39:13'),
	(80, 'delete_product_categories', 'product_categories', '2020-07-14 09:39:13', '2020-07-14 09:39:13'),
	(81, 'browse_products', 'products', '2020-07-14 09:42:18', '2020-07-14 09:42:18'),
	(82, 'read_products', 'products', '2020-07-14 09:42:18', '2020-07-14 09:42:18'),
	(83, 'edit_products', 'products', '2020-07-14 09:42:18', '2020-07-14 09:42:18'),
	(84, 'add_products', 'products', '2020-07-14 09:42:18', '2020-07-14 09:42:18'),
	(85, 'delete_products', 'products', '2020-07-14 09:42:18', '2020-07-14 09:42:18'),
	(86, 'browse_sales', 'sales', '2020-07-14 09:55:47', '2020-07-14 09:55:47'),
	(87, 'read_sales', 'sales', '2020-07-14 09:55:47', '2020-07-14 09:55:47'),
	(88, 'edit_sales', 'sales', '2020-07-14 09:55:47', '2020-07-14 09:55:47'),
	(89, 'add_sales', 'sales', '2020-07-14 09:55:47', '2020-07-14 09:55:47'),
	(90, 'delete_sales', 'sales', '2020-07-14 09:55:47', '2020-07-14 09:55:47'),
	(91, 'browse_subscriptions', 'subscriptions', '2020-07-14 10:00:10', '2020-07-14 10:00:10'),
	(92, 'read_subscriptions', 'subscriptions', '2020-07-14 10:00:10', '2020-07-14 10:00:10'),
	(93, 'edit_subscriptions', 'subscriptions', '2020-07-14 10:00:10', '2020-07-14 10:00:10'),
	(94, 'add_subscriptions', 'subscriptions', '2020-07-14 10:00:10', '2020-07-14 10:00:10'),
	(95, 'delete_subscriptions', 'subscriptions', '2020-07-14 10:00:10', '2020-07-14 10:00:10'),
	(96, 'browse_tags', 'tags', '2020-07-14 10:01:23', '2020-07-14 10:01:23'),
	(97, 'read_tags', 'tags', '2020-07-14 10:01:23', '2020-07-14 10:01:23'),
	(98, 'edit_tags', 'tags', '2020-07-14 10:01:23', '2020-07-14 10:01:23'),
	(99, 'add_tags', 'tags', '2020-07-14 10:01:23', '2020-07-14 10:01:23'),
	(100, 'delete_tags', 'tags', '2020-07-14 10:01:23', '2020-07-14 10:01:23'),
	(101, 'browse_testimonials', 'testimonials', '2020-07-14 10:03:04', '2020-07-14 10:03:04'),
	(102, 'read_testimonials', 'testimonials', '2020-07-14 10:03:04', '2020-07-14 10:03:04'),
	(103, 'edit_testimonials', 'testimonials', '2020-07-14 10:03:04', '2020-07-14 10:03:04'),
	(104, 'add_testimonials', 'testimonials', '2020-07-14 10:03:04', '2020-07-14 10:03:04'),
	(105, 'delete_testimonials', 'testimonials', '2020-07-14 10:03:04', '2020-07-14 10:03:04'),
	(106, 'browse_workers', 'workers', '2020-07-14 10:05:36', '2020-07-14 10:05:36'),
	(107, 'read_workers', 'workers', '2020-07-14 10:05:36', '2020-07-14 10:05:36'),
	(108, 'edit_workers', 'workers', '2020-07-14 10:05:36', '2020-07-14 10:05:36'),
	(109, 'add_workers', 'workers', '2020-07-14 10:05:36', '2020-07-14 10:05:36'),
	(110, 'delete_workers', 'workers', '2020-07-14 10:05:36', '2020-07-14 10:05:36'),
	(111, 'browse_reviews', 'reviews', '2020-07-14 10:08:17', '2020-07-14 10:08:17'),
	(112, 'read_reviews', 'reviews', '2020-07-14 10:08:17', '2020-07-14 10:08:17'),
	(113, 'edit_reviews', 'reviews', '2020-07-14 10:08:17', '2020-07-14 10:08:17'),
	(114, 'add_reviews', 'reviews', '2020-07-14 10:08:17', '2020-07-14 10:08:17'),
	(115, 'delete_reviews', 'reviews', '2020-07-14 10:08:17', '2020-07-14 10:08:17'),
	(116, 'browse_sliders', 'sliders', '2020-07-15 12:33:13', '2020-07-15 12:33:13'),
	(117, 'read_sliders', 'sliders', '2020-07-15 12:33:13', '2020-07-15 12:33:13'),
	(118, 'edit_sliders', 'sliders', '2020-07-15 12:33:13', '2020-07-15 12:33:13'),
	(119, 'add_sliders', 'sliders', '2020-07-15 12:33:13', '2020-07-15 12:33:13'),
	(120, 'delete_sliders', 'sliders', '2020-07-15 12:33:13', '2020-07-15 12:33:13'),
	(121, 'browse_pages', 'pages', '2020-08-06 13:01:42', '2020-08-06 13:01:42'),
	(122, 'read_pages', 'pages', '2020-08-06 13:01:42', '2020-08-06 13:01:42'),
	(123, 'edit_pages', 'pages', '2020-08-06 13:01:42', '2020-08-06 13:01:42'),
	(124, 'add_pages', 'pages', '2020-08-06 13:01:42', '2020-08-06 13:01:42'),
	(125, 'delete_pages', 'pages', '2020-08-06 13:01:42', '2020-08-06 13:01:42');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;

-- Дамп структуры для таблица organic.permission_role
CREATE TABLE IF NOT EXISTS `permission_role` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы organic.permission_role: ~125 rows (приблизительно)
DELETE FROM `permission_role`;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
	(1, 1),
	(2, 1),
	(3, 1),
	(4, 1),
	(5, 1),
	(6, 1),
	(7, 1),
	(8, 1),
	(9, 1),
	(10, 1),
	(11, 1),
	(12, 1),
	(13, 1),
	(14, 1),
	(15, 1),
	(16, 1),
	(17, 1),
	(18, 1),
	(19, 1),
	(20, 1),
	(21, 1),
	(22, 1),
	(23, 1),
	(24, 1),
	(25, 1),
	(26, 1),
	(27, 1),
	(28, 1),
	(29, 1),
	(30, 1),
	(31, 1),
	(32, 1),
	(33, 1),
	(34, 1),
	(35, 1),
	(36, 1),
	(37, 1),
	(38, 1),
	(39, 1),
	(40, 1),
	(41, 1),
	(42, 1),
	(43, 1),
	(44, 1),
	(45, 1),
	(46, 1),
	(47, 1),
	(48, 1),
	(49, 1),
	(50, 1),
	(51, 1),
	(52, 1),
	(53, 1),
	(54, 1),
	(55, 1),
	(56, 1),
	(57, 1),
	(58, 1),
	(59, 1),
	(60, 1),
	(61, 1),
	(62, 1),
	(63, 1),
	(64, 1),
	(65, 1),
	(66, 1),
	(67, 1),
	(68, 1),
	(69, 1),
	(70, 1),
	(71, 1),
	(72, 1),
	(73, 1),
	(74, 1),
	(75, 1),
	(76, 1),
	(77, 1),
	(78, 1),
	(79, 1),
	(80, 1),
	(81, 1),
	(82, 1),
	(83, 1),
	(84, 1),
	(85, 1),
	(86, 1),
	(87, 1),
	(88, 1),
	(89, 1),
	(90, 1),
	(91, 1),
	(92, 1),
	(93, 1),
	(94, 1),
	(95, 1),
	(96, 1),
	(97, 1),
	(98, 1),
	(99, 1),
	(100, 1),
	(101, 1),
	(102, 1),
	(103, 1),
	(104, 1),
	(105, 1),
	(106, 1),
	(107, 1),
	(108, 1),
	(109, 1),
	(110, 1),
	(111, 1),
	(112, 1),
	(113, 1),
	(114, 1),
	(115, 1),
	(116, 1),
	(117, 1),
	(118, 1),
	(119, 1),
	(120, 1),
	(121, 1),
	(122, 1),
	(123, 1),
	(124, 1),
	(125, 1);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;

-- Дамп структуры для таблица organic.products
CREATE TABLE IF NOT EXISTS `products` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `old_price` decimal(8,2) NOT NULL,
  `contains` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_hot` tinyint(4) NOT NULL DEFAULT 0,
  `is_new` tinyint(4) NOT NULL DEFAULT 0,
  `feature` tinyint(4) NOT NULL DEFAULT 0,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_description` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` tinyint(4) NOT NULL DEFAULT 0,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `top` tinyint(4) NOT NULL DEFAULT 0,
  `product_category_id` bigint(20) NOT NULL,
  `slug` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы organic.products: ~5 rows (приблизительно)
DELETE FROM `products`;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` (`id`, `created_at`, `updated_at`, `title`, `image`, `price`, `old_price`, `contains`, `is_hot`, `is_new`, `feature`, `description`, `short_description`, `order`, `status`, `top`, `product_category_id`, `slug`, `meta_description`, `meta_title`) VALUES
	(3, '2020-07-14 11:42:00', '2020-07-15 18:41:19', 'We Grow Beauty', 'products\\July2020\\xG8NvWM21CDxWY1mm6PC.png', 122.20, 140.00, '35% of organic raisins 55% of oats and 10% of butter.', 1, 0, 1, '<p style="box-sizing: border-box; margin: 0px; padding: 0px; font-family: \'Open Sans\', sans-serif; color: #9c9c9c; line-height: 26px;">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years on purpose.</p>\r\n<p style="box-sizing: border-box; margin: 10px 0px 0px; padding: 0px; font-family: \'Open Sans\', sans-serif; color: #9c9c9c; line-height: 26px;">Distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors when looking.</p>', '<p><span style="color: #9c9c9c; font-family: \'Open Sans\', sans-serif;">There are many variations of passages,the majority suffered uts seds alterationin some of form uts, by injected humour random words which don\'t look evenslightly believable.</span></p>', 2, 1, 0, 2, 'we_grow_5', 'meta desc', 'meta title'),
	(4, '2020-07-14 11:42:00', '2020-07-15 15:11:58', 'We Grow Beauty', 'products\\July2020\\pLLtxebqx0FaFi97agmN.png', 122.20, 140.00, '35% of organic raisins 55% of oats and 10% of butter.', 1, 1, 1, '<p style="box-sizing: border-box; margin: 0px; padding: 0px; font-family: \'Open Sans\', sans-serif; color: #9c9c9c; line-height: 26px;">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years on purpose.</p>\r\n<p style="box-sizing: border-box; margin: 10px 0px 0px; padding: 0px; font-family: \'Open Sans\', sans-serif; color: #9c9c9c; line-height: 26px;">Distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors when looking.</p>', '<p><span style="color: #9c9c9c; font-family: \'Open Sans\', sans-serif;">There are many variations of passages,the majority suffered uts seds alterationin some of form uts, by injected humour random words which don\'t look evenslightly believable.</span></p>', 3, 1, 1, 1, 'we_grow_4', 'meta desc', 'meta title'),
	(5, '2020-07-14 11:42:00', '2020-07-15 18:58:36', '789', 'products\\July2020\\4lLMLCFIlJlApzGeTZk4.png', 122.20, 140.00, '35% of organic raisins 55% of oats and 10% of butter.', 1, 0, 1, '<p style="box-sizing: border-box; margin: 0px; padding: 0px; font-family: \'Open Sans\', sans-serif; color: #9c9c9c; line-height: 26px;">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years on purpose.</p>\r\n<p style="box-sizing: border-box; margin: 10px 0px 0px; padding: 0px; font-family: \'Open Sans\', sans-serif; color: #9c9c9c; line-height: 26px;">Distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors when looking.</p>', '<p><span style="color: #9c9c9c; font-family: \'Open Sans\', sans-serif;">There are many variations of passages,the majority suffered uts seds alterationin some of form uts, by injected humour random words which don\'t look evenslightly believable.</span></p>', 1, 1, 0, 3, 'we_grow_3', 'meta desc', 'meta title'),
	(6, '2020-07-14 11:42:00', '2020-07-15 15:27:15', '456', 'products\\July2020\\4lLMLCFIlJlApzGeTZk4.png', 122.20, 140.00, '35% of organic raisins 55% of oats and 10% of butter.', 0, 0, 0, '<p style="box-sizing: border-box; margin: 0px; padding: 0px; font-family: \'Open Sans\', sans-serif; color: #9c9c9c; line-height: 26px;">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a 123 search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years on purpose.</p>\r\n<p style="box-sizing: border-box; margin: 10px 0px 0px; padding: 0px; font-family: \'Open Sans\', sans-serif; color: #9c9c9c; line-height: 26px;">Distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors when looking.</p>', '<p><span style="color: #9c9c9c; font-family: \'Open Sans\', sans-serif;">There are many variations of passages,the majority suffered uts seds alterationin some of form uts, by injected humour random words which don\'t look evenslightly believable.</span></p>', 1, 1, 0, 3, 'we_grow_2', 'meta desc', 'meta title'),
	(7, '2020-07-14 11:42:00', '2020-07-15 15:27:15', 'We Grow123', 'products\\July2020\\4lLMLCFIlJlApzGeTZk4.png', 122.20, 140.00, '35% of organic raisins 55% of oats and 10% of butter.', 0, 0, 0, '<p style="box-sizing: border-box; margin: 0px; padding: 0px; font-family: \'Open Sans\', sans-serif; color: #9c9c9c; line-height: 26px;">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years on purpose.</p>\r\n<p style="box-sizing: border-box; margin: 10px 0px 0px; padding: 0px; font-family: \'Open Sans\', sans-serif; color: #9c9c9c; line-height: 26px;">Distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors when looking.</p>', '<p><span style="color: #9c9c9c; font-family: \'Open Sans\', sans-serif;">There are many variations of passages,the majority suffered uts seds alterationin some of form uts, by injected humour random words which don\'t look evenslightly believable.</span></p>', 1, 1, 0, 3, 'we_grow_1', 'meta desc', 'meta title');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;

-- Дамп структуры для таблица organic.product_categories
CREATE TABLE IF NOT EXISTS `product_categories` (
  `id` bigint(20) NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` tinyint(4) NOT NULL DEFAULT 0,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `product_id` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  KEY `product_categories_created_at_index` (`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы organic.product_categories: ~3 rows (приблизительно)
DELETE FROM `product_categories`;
/*!40000 ALTER TABLE `product_categories` DISABLE KEYS */;
INSERT INTO `product_categories` (`id`, `created_at`, `updated_at`, `title`, `order`, `status`, `product_id`, `slug`) VALUES
	(1, '2020-07-14 11:40:00', '2020-07-14 13:58:33', 'Fruit', 1, 1, '0', 'fruit'),
	(2, '2020-07-14 11:40:00', '2020-07-14 13:58:33', 'Vegetables', 1, 1, '0', 'vegetables'),
	(3, '2020-07-14 11:40:00', '2020-07-14 13:58:33', 'Liquor', 1, 1, '0', 'liquor');
/*!40000 ALTER TABLE `product_categories` ENABLE KEYS */;

-- Дамп структуры для таблица organic.product_tags
CREATE TABLE IF NOT EXISTS `product_tags` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `product_id` bigint(20) unsigned NOT NULL,
  `tag_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_tags_product_id_foreign` (`product_id`),
  KEY `product_tags_tag_id_foreign` (`tag_id`),
  CONSTRAINT `product_tags_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  CONSTRAINT `product_tags_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы organic.product_tags: ~0 rows (приблизительно)
DELETE FROM `product_tags`;
/*!40000 ALTER TABLE `product_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_tags` ENABLE KEYS */;

-- Дамп структуры для таблица organic.recents
CREATE TABLE IF NOT EXISTS `recents` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы organic.recents: ~0 rows (приблизительно)
DELETE FROM `recents`;
/*!40000 ALTER TABLE `recents` DISABLE KEYS */;
/*!40000 ALTER TABLE `recents` ENABLE KEYS */;

-- Дамп структуры для таблица organic.reviews
CREATE TABLE IF NOT EXISTS `reviews` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `comment` varchar(1500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rate` tinyint(4) DEFAULT 1,
  `product_id` bigint(20) unsigned NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `site` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `order` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `reviews_product_id_foreign` (`product_id`),
  CONSTRAINT `reviews_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы organic.reviews: ~23 rows (приблизительно)
DELETE FROM `reviews`;
/*!40000 ALTER TABLE `reviews` DISABLE KEYS */;
INSERT INTO `reviews` (`id`, `created_at`, `updated_at`, `comment`, `rate`, `product_id`, `name`, `email`, `site`, `status`, `order`) VALUES
	(2, '2020-07-19 17:21:51', '2020-07-19 17:21:51', 'sdasdafas', 1, 5, 'sweet', 'kahovskaia@gmail.com', NULL, 0, 0),
	(3, '2020-07-19 17:24:16', '2020-07-19 17:24:16', 'sdasdafas', 1, 5, 'sweet', 'kahovskaia@gmail.com', NULL, 1, 0),
	(4, '2020-07-19 17:26:58', '2020-07-19 17:26:58', 'qweqwe', 1, 5, 'sweet', 'caster977@gmail.com', NULL, 0, 0),
	(5, '2020-07-19 17:34:12', '2020-07-19 17:34:12', 'sdad', 1, 5, 'asdasd', 'admin@gmail.com', NULL, 0, 0),
	(6, '2020-07-19 17:34:43', '2020-07-19 17:34:43', 'sdasdads', 1, 5, 'dasdas', 'fasfa@sdaf', NULL, 0, 0),
	(7, '2020-07-19 17:34:43', '2020-07-19 17:34:43', 'sdasdads', 1, 5, 'dasdas', 'fasfa@sdaf', NULL, 0, 0),
	(8, '2020-07-19 17:34:43', '2020-07-19 17:34:43', 'sdasdads', 1, 5, 'dasdas', 'fasfa@sdaf', NULL, 0, 0),
	(9, '2020-07-19 17:34:43', '2020-07-19 17:34:43', 'sdasdads', 1, 5, 'dasdas', 'fasfa@sdaf', NULL, 0, 0),
	(10, '2020-07-19 17:34:43', '2020-07-19 17:34:43', 'sdasdads', 1, 5, 'dasdas', 'fasfa@sdaf', NULL, 0, 0),
	(11, '2020-07-19 17:34:44', '2020-07-19 17:34:44', 'sdasdads', 1, 5, 'dasdas', 'fasfa@sdaf', NULL, 0, 0),
	(12, '2020-07-19 17:34:44', '2020-07-19 17:34:44', 'sdasdads', 1, 5, 'dasdas', 'fasfa@sdaf', NULL, 0, 0),
	(13, '2020-07-19 17:34:44', '2020-07-19 17:34:44', 'sdasdads', 1, 5, 'dasdas', 'fasfa@sdaf', NULL, 0, 0),
	(14, '2020-07-19 17:37:05', '2020-07-19 17:37:05', 'qrqwr', 1, 5, 'qwe', 'weqrqwr@mail.ru', NULL, 0, 0),
	(15, '2020-07-19 17:37:08', '2020-07-19 17:37:08', 'qrqwr', 1, 5, 'qwe', 'weqrqwr@mail.ru', NULL, 0, 0),
	(16, '2020-07-19 17:38:18', '2020-07-19 17:38:18', 'asd2dsad', 1, 5, 'casdad', 'admin@gmail.com', NULL, 0, 0),
	(17, '2020-07-19 17:38:34', '2020-07-19 17:38:34', 'sdf3f23f2', 1, 5, 'asdafsasf', 'asfa@mail.ru', NULL, 0, 0),
	(18, '2020-07-19 17:39:56', '2020-07-19 17:39:56', 'asdgfasf', 1, 5, 'sdafa', 'asdasd@mail.ru', NULL, 0, 0),
	(19, '2020-07-19 17:40:07', '2020-07-19 17:40:07', 'asdgfasf', 1, 5, 'sdafa', 'asdasd@mail.ru', NULL, 0, 0),
	(20, '2020-07-19 17:40:22', '2020-07-19 17:40:22', 'asdgfasf', 1, 5, 'sdafa', 'asdasd@mail.ru', NULL, 0, 0),
	(21, '2020-07-19 17:40:35', '2020-07-19 17:40:35', 'asdgfasf', 1, 5, 'sdafa', 'asdasd@mail.ru', NULL, 0, 0),
	(22, '2020-07-19 17:40:49', '2020-07-19 17:40:49', 'asdgfasf', 1, 5, 'sdafa', 'asdasd@mail.ru', NULL, 0, 0),
	(23, '2020-07-20 12:45:29', '2020-07-20 12:45:29', 'fsdfsdsdf', 1, 4, 'Vlad Vladik', 'kahdfovskaia@gmail.com', NULL, 1, 0),
	(24, '2020-07-21 09:28:01', '2020-07-21 09:28:01', 'фывфыв', 1, 3, 'Vlad Vladik', 'admin@admin.com', NULL, 0, 0);
/*!40000 ALTER TABLE `reviews` ENABLE KEYS */;

-- Дамп структуры для таблица organic.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы organic.roles: ~2 rows (приблизительно)
DELETE FROM `roles`;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
	(1, 'admin', 'Administrator', '2020-07-14 08:54:16', '2020-07-14 08:54:16'),
	(2, 'user', 'Normal User', '2020-07-14 08:54:16', '2020-07-14 08:54:16');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Дамп структуры для таблица organic.sales
CREATE TABLE IF NOT EXISTS `sales` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `label` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `offer` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` tinyint(4) NOT NULL DEFAULT 0,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы organic.sales: ~2 rows (приблизительно)
DELETE FROM `sales`;
/*!40000 ALTER TABLE `sales` DISABLE KEYS */;
INSERT INTO `sales` (`id`, `created_at`, `updated_at`, `label`, `offer`, `title`, `description`, `link`, `image`, `order`, `status`) VALUES
	(1, '2020-07-14 10:19:00', '2020-07-20 15:21:31', 'WE GROW BEAUTY', '27%', 'We Grow Beauty', '<p>Short desctiption</p>', 'https://vk.ru', 'sales\\July2020\\ri7eEDz00I3TH8RvIq3r.jpg', 1, 1),
	(2, '2020-07-14 10:19:00', '2020-07-20 15:21:48', 'WE GROW BEAUTY', '30%', 'Green Gross', '<p>Short desctiption</p>', 'vas', 'sales\\July2020\\YdGuIEPHyki2pGCxjDeX.jpg', 1, 1);
/*!40000 ALTER TABLE `sales` ENABLE KEYS */;

-- Дамп структуры для таблица organic.settings
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы organic.settings: ~48 rows (приблизительно)
DELETE FROM `settings`;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
	(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
	(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
	(3, 'site.logo', 'Site Logo', 'settings\\July2020\\Sy4B476qmYonW2feKngM.jpg', '', 'image', 3, 'Site'),
	(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
	(5, 'admin.bg_image', 'Admin Background Image', 'settings\\July2020\\rBXMwKKhVre2jQZ4fZyA.jpg', '', 'image', 5, 'Admin'),
	(6, 'admin.title', 'Admin Title', 'Organic', '', 'text', 1, 'Admin'),
	(7, 'admin.description', 'Admin Description', 'Organic product', '', 'text', 2, 'Admin'),
	(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
	(9, 'admin.icon_image', 'Admin Icon Image', 'settings\\July2020\\mX9boNbWHCSOuoD6XLRf.png', '', 'image', 4, 'Admin'),
	(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin'),
	(11, 'footer.Logo_Image', 'Logo in footer', 'settings\\July2020\\E1QRIP1elzPCvCrOyGbt.png', NULL, 'image', 6, 'Footer'),
	(12, 'footer.description_footer', 'Desctiption', '<p style="box-sizing: border-box; margin: 13px 0px 22px; padding: 0px; font-family: \'Open Sans\', sans-serif; color: #9c9c9c; line-height: 26px; background-color: #363636;">Denouncing pleasures and praising pain was born and I will give you a complete account of the system.</p>\r\n<p style="box-sizing: border-box; margin: 13px 0px 22px; padding: 0px; font-family: \'Open Sans\', sans-serif; color: #9c9c9c; line-height: 26px; background-color: #363636;">Expound that actual teachings the great explorer of the truth, the master-builder of human happiness no one rejects, likes, or avoids pleasure itself rationally.</p>', NULL, 'rich_text_box', 7, 'Footer'),
	(15, 'contact.title_contact', 'Title contact', 'GET IN TOUCH', NULL, 'text', 10, 'Contact'),
	(16, 'contact.contact_email', 'Contact email', 'Organic store@gmail.com', NULL, 'text', 11, 'Contact'),
	(17, 'contact.contact_phone', 'Phone number', '+91 (321) 758 142 5698', NULL, 'text', 12, 'Contact'),
	(18, 'contact.adress', 'company adress', 'No 271, Red Cross Building, Modern\r\n Street, Newyork City, USA.', NULL, 'text_area', 13, 'Contact'),
	(19, 'contact.work_time', 'Title Time Work', '<p>Monday - Friday: 09.00am to 07.00pm Saturday: 10.00am to 05.00pm</p>\r\n<p>Sunday: <span style="color: #ff9900;">Closed</span></p>', NULL, 'rich_text_box', 14, 'Contact'),
	(22, 'about-us.story_about', 'Story about Us', '<div class="about_quot">Our food should be our medicine,Our medicine organics should be our food your health.</div>\r\n<div class="text">\r\n<p>How all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness</p>\r\n</div>\r\n<div class="icon-box">\r\n<div class="single-item">\r\n<div class="icon">&nbsp;</div>\r\n<div class="count">745+</div>\r\n<div class="name color1">Own Farm Houses</div>\r\n</div>\r\n<div class="single-item">\r\n<div class="icon">&nbsp;</div>\r\n<div class="count">2480+</div>\r\n<div class="name color1">Pastoral Farmers</div>\r\n</div>\r\n</div>\r\n<div class="text">\r\n<p>Denouncing pleasure and praising pain was born and I will give you a complete account of the system, the actual teachings of the great explorer.</p>\r\n</div>\r\n<div class="link">&nbsp;</div>', NULL, 'rich_text_box', 19, 'About Us'),
	(24, 'about-us.page_name_about_us', 'Page about us name', 'ABOUT US', NULL, 'text', 15, 'About Us'),
	(25, 'about-us.about_first_title', 'Title UnderHaeder', 'WELCOME TO CERTIFIED ONLINE ORGANIC PRODUCTS SUPPLIERSR', NULL, 'text', 16, 'About Us'),
	(26, 'about-us.title_first_block', 'FIrst Block Title', 'STORY ABOUT US', NULL, 'text', 17, 'About Us'),
	(27, 'about-us.second_block_about_us', '2nd block', 'DELIVERY PROCESS', NULL, 'text', 20, 'About Us'),
	(28, 'about-us.third_block_about_us', '3th', 'OUR GALLERY', NULL, 'text', 21, 'About Us'),
	(29, 'about-us.fourth_block_about_us', '4th', 'AWARDS & RECOGNITION', NULL, 'text', 22, 'About Us'),
	(30, 'about-us.first_block_image', 'First Block Image', 'settings\\July2020\\RRjp047lWtvapOkeDS6E.jpg', NULL, 'image', 18, 'About Us'),
	(31, 'about-us.award_text', 'award', '<div class="text" style="box-sizing: border-box; margin: 35px 0px 20px; color: #272727; font-family: Roboto, sans-serif; font-size: 16px;">\r\n<p style="box-sizing: border-box; margin: 0px; padding: 0px; font-family: \'Open Sans\', sans-serif; font-size: 14px; color: #9c9c9c; line-height: 26px;">How to pursue pleasure rationally encounter consequences that are extremely painful Nor again is there anyone who loves or pursues or desires to obtain pain of because it is pain, but because occasionally circumstances occur in some great.</p>\r\n</div>\r\n<div class="award-logo" style="box-sizing: border-box; position: relative; color: #272727; font-family: Roboto, sans-serif; font-size: 16px;">\r\n<ul style="box-sizing: border-box; margin: 0px -15px; padding: 0px; list-style-type: none;">\r\n<li style="box-sizing: border-box; position: relative; display: inline-block; margin: 0px 15px;"><img style="box-sizing: border-box; border: 0px; vertical-align: middle; max-width: 100%; display: block;" src="http://organic.org/images/partner-logo/5.png" alt="" /></li>\r\n&nbsp;\r\n<li style="box-sizing: border-box; position: relative; display: inline-block; margin: 0px 15px;"><img style="box-sizing: border-box; border: 0px; vertical-align: middle; max-width: 100%; display: block;" src="http://organic.org/images/partner-logo/6.png" alt="" /></li>\r\n&nbsp;\r\n<li style="box-sizing: border-box; position: relative; display: inline-block; margin: 0px 15px;"><img style="box-sizing: border-box; border: 0px; vertical-align: middle; max-width: 100%; display: block;" src="http://organic.org/images/partner-logo/7.png" alt="" /></li>\r\n&nbsp;\r\n<li style="box-sizing: border-box; position: relative; display: inline-block; margin: 0px 15px;"><img style="box-sizing: border-box; border: 0px; vertical-align: middle; max-width: 100%; display: block;" src="http://organic.org/images/partner-logo/8.png" alt="" /></li>\r\n</ul>\r\n</div>\r\n<div class="customer-text" style="box-sizing: border-box; margin: 20px 0px 40px; color: #272727; font-family: Roboto, sans-serif; font-size: 16px;">\r\n<p style="box-sizing: border-box; margin: 0px 0px 20px; padding: 0px; font-family: \'Open Sans\', sans-serif; font-size: 14px; color: #9c9c9c; line-height: 26px;"><strong style="box-sizing: border-box; color: #333333; font-size: 18px;">Customer Support:</strong>&nbsp;If you have any doubt about ConsultPress WordPress or how we can support your business, Send us an&nbsp;<span style="box-sizing: border-box; color: #7fb401;">email</span>&nbsp;and we&rsquo;ll get in touch shortly, or Contact via&nbsp;<span style="box-sizing: border-box; color: #7fb401;">Support Forum.</span></p>\r\n<p style="box-sizing: border-box; margin: 0px 0px 20px; padding: 0px; font-family: \'Open Sans\', sans-serif; font-size: 14px; color: #9c9c9c; line-height: 26px;"><strong style="box-sizing: border-box; color: #333333; font-size: 18px;">Office Hours :</strong>&nbsp;07:30 and 19:00 Monday to Saturday,<span style="box-sizing: border-box; color: #7fb401;">Sunday - Holiday</span></p>\r\n</div>', NULL, 'rich_text_box', 23, 'About Us'),
	(32, 'blog.blog_top', 'Blog Top Title', 'NEWS WITH SIDEBAR', NULL, 'text', 24, 'blog'),
	(33, 'blog.subtitle_blog', 'Subtitle Blog', 'WELCOME TO CERTIFIED ONLINE ORGANIC PRODUCTS SUPPLIERSR', NULL, 'text', 25, 'blog'),
	(34, 'faq.faq_title', 'FAQ Title', 'FAQ\'S', NULL, 'text_area', 26, 'Faq'),
	(35, 'faq.faq_subtitle', 'FAQ SubTitle', 'WELCOME TO CERTIFIED ONLINE ORGANIC PRODUCTS SUPPLIERSR', NULL, 'text_area', 27, 'Faq'),
	(36, 'home.1_title', '1 title', 'FEATURED PRODUCTS', NULL, 'text_area', 28, 'Home'),
	(37, 'home.2_title', '2 title', 'WHY TO CHOOSE US', NULL, 'text_area', 29, 'Home'),
	(39, 'home.4_title', '4 title', 'TOP SELLING PRODUCTS', NULL, 'text_area', 32, 'Home'),
	(40, 'home.5_title', '5 title', 'OUR LATEST NEWS', NULL, 'text_area', 33, 'Home'),
	(41, 'home.6_title', '6 title', 'OUR FARMERS', NULL, 'text_area', 34, 'Home'),
	(42, 'home.3_title', '3 title', '<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered <br />alteration in some form, by injected humour.</p>', NULL, 'rich_text_box', 31, 'Home'),
	(43, 'home.7_title', '7 title', 'TESTIMONIALS', NULL, 'text_area', 35, 'Home'),
	(44, 'all-product.shop_title', 'Title shop page', 'OUR STORE', NULL, 'text', 36, 'All Product'),
	(45, 'all-product.sub_title', 'SubTitle', 'WELCOME TO CERTIFIED ONLINE ORGANIC PRODUCTS SUPPLIERSR', NULL, 'text', 37, 'All Product'),
	(46, 'blog-detail.blog_background', 'Blog Detail', 'settings\\July2020\\QWl13Zs1hwHiFEaGP2z0.jpg', NULL, 'image', 38, 'Blog Detail'),
	(47, 'blog-detail.blog_detail_title', 'Blog Title', 'NEWS SINGLE', NULL, 'text', 39, 'Blog Detail'),
	(48, 'blog-detail.blog_detail_subtitle', 'Blog Subtitle', 'WELCOME TO CERTIFIED ONLINE ORGANIC PRODUCTS SUPPLIERSR', NULL, 'text', 40, 'Blog Detail'),
	(49, 'header.number_phone', 'Number Phone', '+321 453 68 739', NULL, 'text', 41, 'Header'),
	(50, 'header.email', 'Email', 'organicstore@gmail.com', NULL, 'text', 42, 'Header'),
	(51, 'header.logo', 'Logo Image', 'settings\\July2020\\70LhcaYavlVofRp9XRBe.png', NULL, 'image', 43, 'Header'),
	(52, 'testimonials.title_testimonials', 'Title Testimonials', NULL, NULL, 'text', 44, 'Testimonials'),
	(53, 'testimonials.subtitle_testimonials', 'SubTitle Testimonials', NULL, NULL, 'text_area', 45, 'Testimonials'),
	(57, 'header.We', 'We provide 100% organic products', 'We provide 100% organic products', NULL, 'text', 46, 'Header');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;

-- Дамп структуры для таблица organic.sliders
CREATE TABLE IF NOT EXISTS `sliders` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `image_background` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sup_title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` tinyint(4) NOT NULL DEFAULT 0,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы organic.sliders: ~2 rows (приблизительно)
DELETE FROM `sliders`;
/*!40000 ALTER TABLE `sliders` DISABLE KEYS */;
INSERT INTO `sliders` (`id`, `image_background`, `image`, `title`, `sup_title`, `description`, `order`, `status`, `created_at`, `updated_at`) VALUES
	(3, 'sliders\\July2020\\yIJo0ueFaNr4AfmAyPDN.jpg', 'sliders\\July2020\\ub03xhNOGQHooAnNG5tL.png', 'We Grow Beauty', 'Овощи~Фрукты~Сено~Трава', 'Здесь большое описание банера, проверка цвета и шрифта.', 1, 1, '2020-07-15 13:20:00', '2020-07-15 13:22:24'),
	(4, 'sliders\\July2020\\vFgxEHwWHDTno4T2Eipv.jpg', 'sliders\\July2020\\Coir9bsSBad54SAoQsip.png', 'We Grow Beauty', 'Овощи~Фрукты~Сено~Трава', 'Здесь большое описание банера, проверка цвета и шрифта.', 1, 1, '2020-07-15 13:20:00', '2020-07-15 13:21:55');
/*!40000 ALTER TABLE `sliders` ENABLE KEYS */;

-- Дамп структуры для таблица organic.subscriptions
CREATE TABLE IF NOT EXISTS `subscriptions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` tinyint(4) NOT NULL DEFAULT 0,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы organic.subscriptions: ~54 rows (приблизительно)
DELETE FROM `subscriptions`;
/*!40000 ALTER TABLE `subscriptions` DISABLE KEYS */;
INSERT INTO `subscriptions` (`id`, `created_at`, `updated_at`, `name`, `email`, `order`, `status`) VALUES
	(1, '2020-07-14 21:07:08', '2020-07-14 21:07:08', 'Влад', 'admin@gmail.com', 0, 0),
	(2, '2020-07-14 21:13:42', '2020-07-14 21:13:42', 'Влад', 'admin@gmail.com', 0, 0),
	(3, '2020-07-15 20:32:24', '2020-07-15 20:32:24', 'coganid', '93-3caenko@mail.ru', 0, 0),
	(4, '2020-07-15 21:21:13', '2020-07-15 21:21:13', 'Влад', 'admin@admin.com', 0, 0),
	(5, '2020-07-15 21:21:45', '2020-07-15 21:21:45', 'Влад', 'admin@admin.com', 0, 0),
	(6, '2020-07-15 21:23:56', '2020-07-15 21:23:56', 'sweet', '93-3caenko@mail.ru', 0, 0),
	(7, '2020-07-15 21:24:56', '2020-07-15 21:24:56', 'sweet', '../@mail.ru', 0, 0),
	(8, '2020-07-16 07:59:39', '2020-07-16 07:59:39', 'Влад', 'admin@admin.com', 0, 0),
	(9, '2020-07-16 08:00:51', '2020-07-16 08:00:51', 'Влад', '../@mail.ru', 0, 0),
	(10, '2020-07-16 08:03:35', '2020-07-16 08:03:35', 'Влад', 'admin@admin.com', 0, 0),
	(11, '2020-07-16 08:03:50', '2020-07-16 08:03:50', 'Влад', 'admin@admin.com', 0, 0),
	(12, '2020-07-16 08:06:29', '2020-07-16 08:06:29', 'leaf', 'admin@admin.com', 0, 0),
	(13, '2020-07-16 08:12:52', '2020-07-16 08:12:52', 'leaf', '93-3caenko@mail.ru', 0, 0),
	(14, '2020-07-16 08:14:31', '2020-07-16 08:14:31', 'leaf', 'admin@gmail.com', 0, 0),
	(15, '2020-07-16 08:15:17', '2020-07-16 08:15:17', 'coganid', 'admin@admin.com', 0, 0),
	(16, '2020-07-16 08:20:59', '2020-07-16 08:20:59', 'leaf', 'caster977@gmail.com', 0, 0),
	(17, '2020-07-16 08:21:31', '2020-07-16 08:21:31', 'leaf', '93-3caenko@mail.ru', 0, 0),
	(18, '2020-07-16 08:30:30', '2020-07-16 08:30:30', 'sweet', 'kahovskaia@gmail.com', 0, 0),
	(19, '2020-07-16 08:36:18', '2020-07-16 08:36:18', 'Влад', 'admin@admin.com', 0, 0),
	(20, '2020-07-16 08:36:21', '2020-07-16 08:36:21', 'Влад', 'admin@admin.com', 0, 0),
	(21, '2020-07-16 08:36:23', '2020-07-16 08:36:23', 'Влад', 'admin@admin.com', 0, 0),
	(22, '2020-07-16 08:36:24', '2020-07-16 08:36:24', 'Влад', 'admin@admin.com', 0, 0),
	(23, '2020-07-16 08:36:24', '2020-07-16 08:36:24', 'Влад', 'admin@admin.com', 0, 0),
	(24, '2020-07-16 08:36:26', '2020-07-16 08:36:26', 'fsd', 'admin@admin.com', 0, 0),
	(25, '2020-07-16 08:36:28', '2020-07-16 08:36:28', 'fsd', 'admin@admin.com', 0, 0),
	(26, '2020-07-16 08:36:29', '2020-07-16 08:36:29', 'fsd', 'admin@admin.com', 0, 0),
	(27, '2020-07-16 08:36:31', '2020-07-16 08:36:31', 'bdfdf', 'admin@admin.com', 0, 0),
	(28, '2020-07-16 08:38:49', '2020-07-16 08:38:49', 'Влад', '93-3caenko@mail.ru', 0, 0),
	(29, '2020-07-16 08:38:50', '2020-07-16 08:38:50', 'Влад', '93-3caenko@mail.ru', 0, 0),
	(30, '2020-07-16 08:38:51', '2020-07-16 08:38:51', 'Влад', '93-3caenko@mail.ru', 0, 0),
	(31, '2020-07-16 08:38:51', '2020-07-16 08:38:51', 'Влад', '93-3caenko@mail.ru', 0, 0),
	(32, '2020-07-16 08:38:52', '2020-07-16 08:38:52', 'Влад', '93-3caenko@mail.ru', 0, 0),
	(33, '2020-07-16 08:38:53', '2020-07-16 08:38:53', 'Влад', '93-3caenko@mail.ru', 0, 0),
	(34, '2020-07-16 08:38:53', '2020-07-16 08:38:53', 'Влад', '93-3caenko@mail.ru', 0, 0),
	(35, '2020-07-16 08:38:53', '2020-07-16 08:38:53', 'Влад', '93-3caenko@mail.ru', 0, 0),
	(36, '2020-07-16 08:38:54', '2020-07-16 08:38:54', 'Влад', '93-3caenko@mail.ru', 0, 0),
	(37, '2020-07-16 08:40:43', '2020-07-16 08:40:43', 'Влад', 'admin@gmail.com', 0, 0),
	(38, '2020-07-16 08:40:47', '2020-07-16 08:40:47', 'Влад', 'admin@gmail.com', 0, 0),
	(39, '2020-07-16 08:40:47', '2020-07-16 08:40:47', 'Влад', 'admin@gmail.com', 0, 0),
	(40, '2020-07-16 08:40:47', '2020-07-16 08:40:47', 'Влад', 'admin@gmail.com', 0, 0),
	(41, '2020-07-16 08:40:47', '2020-07-16 08:40:47', 'Влад', 'admin@gmail.com', 0, 0),
	(42, '2020-07-16 08:48:16', '2020-07-16 08:48:16', 'Влад', '93-3caenko@mail.ru', 0, 0),
	(43, '2020-07-16 13:48:07', '2020-07-16 13:48:07', 'фывфыв', 'asdf@mail.ru', 0, 0),
	(44, '2020-07-16 14:09:46', '2020-07-16 14:09:46', 'Vlad Vladik', 'admin@admin.com', 0, 0),
	(45, '2020-07-16 14:30:38', '2020-07-16 14:30:38', 'leaf123123123123123', 'kahovskaia@gmail.com', 0, 0),
	(46, '2020-07-16 14:34:42', '2020-07-16 14:34:42', 'asd', 'a@as', 0, 0),
	(47, '2020-07-16 17:04:07', '2020-07-16 17:04:07', 'sdad', 'asd', 0, 0),
	(48, '2020-07-16 17:06:05', '2020-07-16 17:06:05', 'sad', '323@a', 0, 0),
	(49, '2020-07-20 12:40:37', '2020-07-20 12:40:37', 'Vlad Vladik', 'kahovsdfskaia@gmail.com', 0, 0),
	(50, '2020-07-24 10:24:30', '2020-07-24 10:24:30', '\'or 1 = 1', 'div@gmail.com', 0, 0),
	(51, '2020-07-24 10:25:00', '2020-07-24 15:17:11', '</div>', 'divj@gmail.com', 0, 1),
	(54, '2020-07-28 20:15:29', '2020-07-28 20:15:29', '13123123123', 'adm3din@admin.com', 0, 1),
	(55, '2020-07-28 20:15:29', '2020-07-28 20:15:29', '13123123123', 'adm3din@admin.com', 0, 1),
	(56, '2020-07-28 20:16:16', '2020-07-28 20:16:16', '13123123123', 'ad2m3din@admin.com', 0, 1);
/*!40000 ALTER TABLE `subscriptions` ENABLE KEYS */;

-- Дамп структуры для таблица organic.tags
CREATE TABLE IF NOT EXISTS `tags` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` tinyint(4) NOT NULL DEFAULT 0,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы organic.tags: ~0 rows (приблизительно)
DELETE FROM `tags`;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;

-- Дамп структуры для таблица organic.testimonials
CREATE TABLE IF NOT EXISTS `testimonials` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profession` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы organic.testimonials: ~5 rows (приблизительно)
DELETE FROM `testimonials`;
/*!40000 ALTER TABLE `testimonials` DISABLE KEYS */;
INSERT INTO `testimonials` (`id`, `created_at`, `updated_at`, `name`, `image`, `description`, `profession`, `status`) VALUES
	(33, '2020-07-14 19:03:00', '2020-07-20 15:12:05', 'Vlad Vladik', 'testimonials\\July2020\\8y9uywPa0JC0qAgfzs9s.png', '<p><span style="color: #ffffff; font-size: 16px; margin-bottom: 20px; font-family: \'Open Sans\', sans-serif; font-style: italic;">Who do not know how to pursue an sed pleasure rationally encounter that are extremely win painful nor again is there anyone who loves or pursues or desires obtain pain itself circumstances.</span></p>', 'Дворник', 1),
	(38, '2020-07-14 19:03:00', '2020-07-20 15:12:42', 'Vlad Vladik', 'testimonials\\July2020\\jVhpY7QQ4X3pU0sQPEzd.png', '<p><span style="color: #ffffff; font-size: 16px; margin-bottom: 20px; font-family: \'Open Sans\', sans-serif; font-style: italic;">Who do not know how to pursue an sed pleasure rationally encounter that are extremely win painful nor again is there anyone who loves or pursues or desires obtain pain itself circumstances.</span></p>', 'Кореец', 1),
	(39, '2020-07-14 19:03:04', '2020-07-14 19:03:04', 'Vlad Vladik', 'testimonials\\July2020\\CoMWNQ0liAjveQ4BtMpE.png', '<p><span style="color: #ffffff; font-size: 16px; margin-bottom: 20px; font-family: \'Open Sans\', sans-serif; font-style: italic;">Who do not know how to pursue an sed pleasure rationally encounter that are extremely win painful nor again is there anyone who loves or pursues or desires obtain pain itself circumstances.</span></p>', 'Дворник', 1),
	(40, '2020-07-14 19:03:04', '2020-07-14 19:03:04', 'Vlad Vladik', 'testimonials\\July2020\\CoMWNQ0liAjveQ4BtMpE.png', '<p><span style="color: #ffffff; font-size: 16px; margin-bottom: 20px; font-family: \'Open Sans\', sans-serif; font-style: italic;">Who do not know how to pursue an sed pleasure rationally encounter that are extremely win painful nor again is there anyone who loves or pursues or desires obtain pain itself circumstances.</span></p>', 'Дворник', 1),
	(41, '2020-07-14 19:03:04', '2020-07-14 19:03:04', 'Vlad Vladik', 'testimonials\\July2020\\CoMWNQ0liAjveQ4BtMpE.png', '<p><span style="color: #ffffff; font-size: 16px; margin-bottom: 20px; font-family: \'Open Sans\', sans-serif; font-style: italic;">Who do not know how to pursue an sed pleasure rationally encounter that are extremely win painful nor again is there anyone who loves or pursues or desires obtain pain itself circumstances.</span></p>', 'Дворник', 1);
/*!40000 ALTER TABLE `testimonials` ENABLE KEYS */;

-- Дамп структуры для таблица organic.translations
CREATE TABLE IF NOT EXISTS `translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) unsigned NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы organic.translations: ~0 rows (приблизительно)
DELETE FROM `translations`;
/*!40000 ALTER TABLE `translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `translations` ENABLE KEYS */;

-- Дамп структуры для таблица organic.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы organic.users: ~1 rows (приблизительно)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
	(1, 1, 'admin', 'admin@admin.com', 'users\\July2020\\Z1rbm4kKRZP47FyPfNR6.png', NULL, '$2y$10$jmTPHSnWsLElFZ4672oYHe4CM8Q32uFfz77irC414G2BlSm8/RnvC', NULL, '{"locale":"en"}', '2020-07-14 08:55:03', '2020-07-17 13:53:12');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Дамп структуры для таблица organic.user_roles
CREATE TABLE IF NOT EXISTS `user_roles` (
  `user_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `user_roles_user_id_index` (`user_id`),
  KEY `user_roles_role_id_index` (`role_id`),
  CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы organic.user_roles: ~0 rows (приблизительно)
DELETE FROM `user_roles`;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;

-- Дамп структуры для таблица organic.workers
CREATE TABLE IF NOT EXISTS `workers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profession` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `facebook` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instagram` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `twitter` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` tinyint(4) NOT NULL DEFAULT 0,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы organic.workers: ~5 rows (приблизительно)
DELETE FROM `workers`;
/*!40000 ALTER TABLE `workers` DISABLE KEYS */;
INSERT INTO `workers` (`id`, `created_at`, `updated_at`, `image`, `name`, `profession`, `description`, `facebook`, `instagram`, `twitter`, `order`, `status`) VALUES
	(1, '2020-07-14 14:33:00', '2020-07-14 15:08:17', 'workers\\July2020\\pRBsvEsbswMukb8D2oIQ.jpg', 'Vlad Vladik', 'IT', '<p>This information for people,</p>\r\n<p>i write somethink.</p>', '1', '1', '1', 1, 1),
	(2, '2020-07-14 14:33:24', '2020-07-14 14:33:24', 'workers\\July2020\\M2hiAglPUSWG1Etr4Iu2.jpg', 'Egor Crid', 'PoP', '<p>This information for people,</p>\r\n<p>i write somethink.</p>', '1', '1', '1', 1, 1),
	(3, '2020-07-14 14:33:24', '2020-07-14 14:33:24', 'workers\\July2020\\M2hiAglPUSWG1Etr4Iu2.jpg', 'Sasha Svift', 'Bloger', '<p>This information for people,</p>\r\n<p>i write somethink.</p>', '1', '1', '1', 1, 1),
	(4, '2020-07-14 14:33:24', '2020-07-14 14:33:24', 'workers\\July2020\\M2hiAglPUSWG1Etr4Iu2.jpg', 'Ivan Urgant', 'TV', '<p>This information for people,</p>\r\n<p>i write somethink.</p>', '1', '1', '1', 1, 1),
	(5, '2020-07-14 14:33:24', '2020-07-14 14:33:24', 'workers\\July2020\\M2hiAglPUSWG1Etr4Iu2.jpg', 'Shtefan Chelmore', 'History', '<p>This information for people,</p>\r\n<p>i write somethink.</p>', '1', '1', '1', 1, 1);
/*!40000 ALTER TABLE `workers` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
