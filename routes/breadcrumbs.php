<?php

Breadcrumbs::register('home', function ($breadcrumbs) {
    $breadcrumbs->push('Home', route('home'));
});

Breadcrumbs::register('contact', function ($breadcrumbs){
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Contact', route('contact'));
});

Breadcrumbs::register('about-us', function ($breadcrumbs){
    $breadcrumbs->parent('home');
    $breadcrumbs->push('About-Us', route('about-us'));
});

Breadcrumbs::register('shop', function ($breadcrumbs){
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Our Store', route('shop'));
});

Breadcrumbs::register('blog', function ($breadcrumbs){
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Blog', route('blog'));
});

Breadcrumbs::register('testimonial', function ($breadcrumbs){
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Testimonial', route('testimonial'));
});

Breadcrumbs::register('faq', function ($breadcrumbs){
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Faq', route('faq'));
});

