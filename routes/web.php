<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontendController@index')->name('home');
Route::get('/about-us', 'FrontendController@aboutUs')->name('about-us');
Route::post('/subscribe', 'FrontendController@subscribe')->name('subscribe');
Route::post('/feedback', 'FrontendController@feedback')->name('feedback');
Route::get('/blog', 'FrontendController@blog')->name('blog');
Route::get('/contact', 'FrontendController@contact')->name('contact');
Route::get('/faq', 'FrontendController@faq')->name('faq');
Route::get('/testimonial', 'FrontendController@testimonial')->name('testimonial');
Route::get('/shop', 'FrontendController@shop')->name('shop');
Route::get('/blog/{slug}', 'FrontendController@blog_detail')->name('blog_detail');
Route::get('/shop/{slug}', 'FrontendController@product_detail')->name('product_detail');
Route::post('/blog/comment_blog', 'FrontendController@comment_blog')->name('comment_blog');
Route::post('/product/review_product', 'FrontendController@review_product')->name('review_product');
Route::get('/search', 'FrontendController@search')->name('search');
Route::get('/sitemap', 'FrontendController@sitemap')->name('sitemap');
Route::get('/shop-cart', 'CartController@cart')->name('shop-cart');
Route::get('/cart-sent/{id}', 'CartController@cart_sent')->name('cart_sent');
Route::get('/cart-delete/{id}', 'CartController@cart_delete')->name('cart_delete');
Route::get('/order-add', 'CartController@order_add')->name('order_add');
Route::post('/order-create', 'CartController@order_store')->name('order_store');
// route for processing payment
Route::post('paypal', 'PaymentController@payWithpaypal');
// route for check status of the payment
Route::get('status', 'PaymentController@getPaymentStatus');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});





