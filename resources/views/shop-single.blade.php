@extends('layouts.app')
@section('content')
			<section class="breadcrumb-area" style="background-image:url({{asset('images/background/2.jpg')}});">
			    <div class="container">
			        <div class="row">
			            <div class="col-md-12">
			                <div class="breadcrumbs text-center">
			                    <h1>single product</h1>
			                    <h4>Welcome to certified online organic products suppliersr</h4>
			                </div>
			            </div>
			        </div>
			    </div>
				<div class="breadcrumb-bottom-area">
				    <div class="container">
				        <div class="row">
				            <div class="col-lg-8 col-md-5 col-sm-5">
				                <ul>
				                    <li><a href="{{route('home')}}">Home</a></li>
				                    <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
				                    <li><a href="{{route('shop')}}">Shop</a></li>
				                    <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
				                    <li>{{$product_detail->title}}</li>
				                </ul>
				            </div>
				            <div class="col-lg-4 col-md-7 col-sm-7">
				                <p>{{setting('header.We')}}</p>
				            </div>
				        </div>
				    </div>
				</div>

			</section>

			<!-- Single Shop page content ________________ -->

	        <div class="shop_single_page">
	        	<div class="container">
	        		<div class="row">


						<!-- _______________________ SIDEBAR ____________________ -->
	        			<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 sidebar_styleTwo">
	        				<div class="wrapper">
	        					<div class="sidebar_search">
	        						<form action="#">
	        							<input type="text">
	        							<button class="tran3s color1_bg"><i class="fa fa-search" aria-hidden="true"></i></button>
	        						</form>
	        					</div> <!-- End of .sidebar_styleOne -->

	        					<div class="sidebar_categories">
	        						<div class="theme_inner_title">
										<h4>Categories</h4>
									</div>
									<ul>
                                        @foreach($categories as $category)
                                            <li><a href="{{route('shop')}}?product_category_id={{$category->id}}" class="tran3s">{{$category->title}}</a></li>
                                        @endforeach
									</ul>
	        					</div> <!-- End of .sidebar_categories -->
								<div class="best_sellers clear_fix wow fadeInUp">
									<div class="theme_inner_title">
										<h4>popular products</h4>
									</div>
                                    @foreach($popular_products as $popular_product)
                                        <div class="best_selling_item clear_fix border">
                                            <div class="img_holder float_left">
                                                <img src="{{Voyager::image($popular_product->image)}}" alt="image">
                                            </div> <!-- End of .img_holder -->

                                            <div class="text float_left">
                                                <a href="{{route('product_detail', $popular_product->slug)}}"><h6>{{$popular_product->title}}</h6></a>
                                                <span>{{$popular_product->price}}</span>
                                            </div> <!-- End of .text -->
                                        </div> <!-- End of .best_selling_item -->
                                    @endforeach
								</div> <!-- End of .best_sellers -->

	        				</div> <!-- End of .wrapper -->
	        			</div> <!-- End of .sidebar_styleTwo -->
	        			<div class="col-lg-9 col-md-8 col-sm-12 col-xs-12 product_details">
	        				<div class="wrapper">
	        					<div class="product_top_section clear_fix">
	        						<div class="img_holder float_left">
	        							<img src="{{Voyager::image($product_detail->image)}}" alt="img" class="img-responsive">
	        						</div> <!-- End of .img_holder -->
	        						<div class="item_description float_left">
	        							<h4>The Art Of The Start</h4>
										<span class="item_price">${{$product_detail->price}}</span>
										{!! $product_detail->short_description!!}
										</div>
	        						</div> <!-- End of .item_description -->
	        					</div> <!-- End of .product_top_section -->

	        					<!-- __________________ Product review ___________________ -->
	        					<div class="product-review-tab">
									<ul class="nav nav-pills">
									    <li><a data-toggle="pill" href="#tab1">Description</a></li>
									    <li class="active"><a data-toggle="pill" href="#tab2">Reviews</a></li>
								  	</ul>

									 <div class="tab-content">
									    <div id="tab1" class="tab-pane fade">
                                            {!! $product_detail->description!!}
									    </div> <!-- End of #tab1 -->

									    <div id="tab2" class="tab-pane fade in active">
                                            @foreach($product_detail->reviews as $review)
									            <!-- Single Review -->
                                                <div class="item_review_content clear_fix">
                                                    <div class="text float_left">
                                                        <div class="sec_up clear_fix">
                                                            <h6 class="float_left">{{$review->name}}</h6>
                                                            <div class="float_right">
                                                                <span class="p_color">{{date('d F Y H:i', strtotime($review->created_at))}}</span>
                                                            </div>
                                                        </div> <!-- End of .sec_up -->
                                                        <p>{{$review->comment}}</p>
                                                    </div> <!-- End of .text -->
                                                </div> <!-- End of .item_review_content -->
                                            @endforeach
									      <div class="add_your_review">
										      	<div class="theme_inner_title">
													<h4>Add Your Review</h4>
												</div>
                                              <form method="post" id="review_product" action="{{route('review_product')}}">
                                                  @csrf
													<div class="row">
														<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
															<input type="text" name="name" placeholder="Name*">
														</div>
														<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
															<input type="email" name="email" placeholder="Email*">
														</div>
														<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
															<textarea name="comment" placeholder="Your Review..."></textarea>
														</div>
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <input name="product_id" type="hidden" value="{{$product_detail->id}}">
                                                        </div>
													</div>
													<button class="color1_bg tran3s">Add A Review</button>
												</form>
									      </div> <!-- End of .add_your_review -->
									    </div> <!-- End of #tab2 -->
									 </div> <!-- End of .tab-content -->
								</div> <!-- End of .product-review-tab -->
                                <script>
                                    let in_proccess = false;
                                    let backup_form = document.getElementById('review_product')
                                    backup_form.addEventListener("submit", function (event) {
                                        event.preventDefault()
                                        console.log(in_proccess)
                                        if(in_proccess){
                                            return false
                                        }
                                        in_proccess = true;
                                        const data = new FormData(review_product)
                                        axios.post('{{route('review_product')}}', data).then(function (response) {
                                            backup_form.insertAdjacentHTML('beforeend', '<div class="alert" id="min" style="color: white; text-align: center; background: darkgreen; animation: hideDiv 2s forwards ;"><ul><li>Комментарий ожидает модерации</li></ul></div>');
                                            const d1 = document.getElementById('min')
                                            setTimeout(function() {
                                                in_proccess = false;
                                                d1.remove();
                                            },10000);

                                        })

                                    })
                                </script>

								<div class="related_product">
									<div class="theme_title">
										<h3>Feature Product</h3>
									</div>


									<!-- Shop Page Content************************ -->
							        <div class="shop_page featured-product">

						        		<div class="row">
                                            @foreach($related_products as $related_product)
                                                <!--Default Item-->
                                                <div class="col-md-4 col-sm-6 col-xs-12 default-item" style="display: inline-block;">
                                                    <div class="inner-box">
                                                        <div class="single-item center">
                                                            <figure class="image-box"><img src="{{Voyager::image($related_product->image)}}" alt=""><div class="product-model new">New</div></figure>
                                                            <div class="content">
                                                                <h3><a href="shop-single.html">{{$related_product->title}}</a></h3>
                                                                <div class="rating"><span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span></div>
                                                                <div class="price">${{$related_product->price}} <span class="prev-rate">${{$related_product->old_price}}</span></div>
                                                            </div>
                                                            <div class="overlay-box">
                                                                <div class="inner">
                                                                    <div class="top-content">
                                                                        <ul>
                                                                            <li><a href="#"><span class="fa fa-eye"></span></a></li>
                                                                            <li class="tultip-op"><span class="tultip"><i class="fa fa-sort-desc"></i>ADD TO CART</span><a href="#"><span class="icon-icon-32846"></span></a>

                                                                            </li>
                                                                            <li><a href="#"><span class="fa fa-heart-o"></span></a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <div class="bottom-content">
                                                                        <h4><a href="{{route('product_detail', $related_product->slug)}}">It Contains:</a></h4>
                                                                        <p>{{$related_product->contains}} </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
						        		</div> <!-- End of .row -->

							        </div> <!-- End of .shop_page -->
								</div> <!-- End of .related_product -->
	        				</div> <!-- End of .wrapper -->
	        			</div> <!-- End of .col -->


	        		</div> <!-- End of .row -->
	        	</div> <!-- End of .container -->
	        </div> <!-- End of .shop_single_page -->

@endsection
