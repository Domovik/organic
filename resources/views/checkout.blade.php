@extends('layouts.app')
@section('content')


			<section class="breadcrumb-area" style="background-image:url(images/background/2.jpg);">
			    <div class="container">
			        <div class="row">
			            <div class="col-md-12">
			                <div class="breadcrumbs text-center">
			                    <h1>checkout</h1>
			                    <h4>Welcome to certified online organic products suppliersr</h4>
			                </div>
			            </div>
			        </div>
			    </div>
				<div class="breadcrumb-bottom-area">
				    <div class="container">
				        <div class="row">
				            <div class="col-lg-8 col-md-5 col-sm-5">
				                <ul>
				                    <li><a href="#">Home</a></li>
				                    <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
				                    <li><a href="#">Gallery</a></li>
				                    <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
				                    <li>checkout</li>
				                </ul>
				            </div>
				            <div class="col-lg-4 col-md-7 col-sm-7">
				                <p>{{setting('header.We')}}</p>
				            </div>
				        </div>
				    </div>
				</div>

			</section>


	        <!-- Checkout page content******************* -->
	        <div class="check_out_form container">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 submit_form">
						<div class="theme-title">
							<h2>Billing Address</h2>
						</div>
						<form action="#" class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<span>Country *</span>
								<input type="text">
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<span>First Name *</span>
								<input type="text" placeholder="">
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<span>Last Name *</span>
								<input type="text" placeholder="">
							</div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<span>Address</span>
								<input type="text" placeholder="">
								<input type="text" placeholder="">
							</div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<span>Town / City *</span>
								<input type="text" placeholder="">
							</div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<span>Contact Info *</span>
								<input type="email" placeholder="Email Address">
								<input type="text" placeholder="Phone Number">
							</div>
						</form>
					</div> <!-- /submit_form -->

					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 submit_form shipping_address">
						<div class="theme-title">
							<h2>Shipping Address <input type="checkbox"></h2>
						</div>
						<form action="#" class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<span>Country *</span>
								<input type="text">
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<span>First Name *</span>
								<input type="text" placeholder="">
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<span>Last Name *</span>
								<input type="text" placeholder="">
							</div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<span>Address</span>
								<input type="text" placeholder="">
								<input type="text" placeholder="">
							</div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<span>Town / City *</span>
								<input type="text" placeholder="">
							</div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<span>Other Notes</span>
								<textarea></textarea>
							</div>
						</form>
					</div> <!-- /submit_form -->
				</div> <!-- /row -->
			</div> <!-- /check_out_form -->




			<!-- cart table*********************** -->
			<div class="cart_table container">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<div class="table-responsive">
							<table class="table table-1">
								<thead>
									<tr>
										<th><span>Product</span></th>
										<th style="padding-left:0"><span>Quantity</span></th>
										<th><span style="margin-left: 9px;">Total</span></th>
									</tr>
								</thead> <!-- /thead -->
								<tbody>

									<tr>
										<td class="flex_item clear_fix">
											<img src="images/shop/9.png" alt="images" class="float_left">
											<h6 class="float_left">Start From The Art</h6>
										</td>
										<td><input type="number" name="quantity" min="0" value="1"></td>
										<td><span>$25.00</span></td>
									</tr> <!-- /tr -->

									<tr>
										<td class="flex_item clear_fix">
											<img src="images/shop/10.png" alt="images" class="float_left">
											<h6 class="float_left">Lords Of Strategy</h6>
										</td>
										<td><input type="number" name="quantity" min="0" value="3"></td>
										<td><span>$69.00</span></td>
									</tr> <!-- /tr -->

									<tr>
										<td class="flex_item clear_fix">
											<img src="images/shop/11.png" alt="images" class="float_left">
											<h6 class="float_left">Start From The Art</h6>
										</td>
										<td><input type="number" name="quantity" min="0" value="2"></td>
										<td><span>$29.00</span></td>
									</tr> <!-- /tr -->

								</tbody> <!-- /tbody -->
							</table>
						</div> <!-- /table-responsive -->
					</div>
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<h4>Cart Totals</h4>
						<div class="table-responsive">
							<table class="table table-2">
								<tbody>
									<tr>
										<td><span>Cart Subtotal</span></td>
										<td><span>$146.00</span></td>
									</tr>
									<tr>
										<td><span>Shipping and Handling</span></td>
										<td><span>Free Shipping</span></td>
									</tr>
									<tr>
										<td><span>Order Total</span></td>
										<td><span>$146.00</span></td>
									</tr>
								</tbody>
							</table>
						</div> <!-- /table-responsive -->
						<div class="payment_system">
							<div class="pay1">
								<input type="checkbox">
								<span>Direct Bank Transfer</span>
								<p>Make your payment directly into our bank account. Please use your Order ID as the payment reference.order won’t be shipped until the funds have cleared.</p>
							</div>
							<div class="pay1">
								<input type="checkbox">
								<span>Cheque Payment</span>
							</div>
							<div class="pay1">
								<input type="checkbox">
								<span>Credit Card</span>
								<img src="images/check-out/1.jpg" alt="image" class="float_right">
							</div>
							<div class="pay1">
								<input type="checkbox">
								<span>Paypal</span>
								<img src="images/check-out/2.jpg" alt="image" class="float_right">
							</div>
							<a href="#" class="tran3s color2_bg float_right">Place Order</a>
						</div>
					</div>
				</div>
			</div> <!-- /cart_table -->


@endsection

