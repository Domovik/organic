@extends('layouts.app')
@section('content')

    <section class="breadcrumb-area" style="background-image:url(images/background/2.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumbs text-center">
                        <h1>Shopping Cart</h1>
                        <h4>Welcome to certified online organic products suppliersr</h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="breadcrumb-bottom-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-md-5 col-sm-5">
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                            <li><a href="#">Gallery</a></li>
                            <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                            <li>Shopping Cart</li>
                        </ul>
                    </div>
                    <div class="col-lg-4 col-md-7 col-sm-7">
                        <p>We provide <span>100% organic</span> products</p>
                    </div>
                </div>
            </div>
        </div>

    </section>


    <!-- cart Table*************************** -->

    <div class="shop_cart_table container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="table-responsive">
                    <table class="table table-1">
                        <tr>
                            <th><span>Product</span></th>
                            <th><span>Quantity</span></th>
                            <th><span>Avalability</span></th>
                            <th><span>Price</span></th>
                            <th><span>Total</span></th>
                            <th><span>Remove</span></th>
                        </tr>
                        @foreach($carts as $cart)
                            <tr>
                                <td class="flex_item clear_fix">
                                    <img src="{{Voyager::image($cart->product->image)}}" alt="images" class="float_left">
                                    <h6 class="float_left">{{$cart->product->title}}</h6>
                                </td>
                                <td><input type="number" name="quantity" min="0" value="{{$cart->quantity}}"></td>
                                <td>
                                    <div class="icon_holder border_round">
                                        <i class="fa fa-check"></i>
                                    </div>
                                    <span class="item">Item(s) <br>Avilable Now</span>
                                </td>
                                <td><span>${{$cart->product->price}}</span></td>
                                <td><span class="color2">${{($cart->product->price)*($cart->quantity)}}</span></td>
                                <td><a class="add_product" href="{{route('cart_delete', $cart->id)}}" style="vertical-align:-2px;"> <span style="padding-left:7px;">Remove</span></td>
                            </tr> <!-- /tr -->
                        @endforeach
                    </table>
                </div> <!-- /table-responsive -->
            </div>
            @if(session()->has('error'))
                {{session()->pull('error')}}
            @endif
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 cart_update" style="text-align:right;">
                <button class="cart_btn3 tran3s">Update Cart</button>
                <form action="/order-create" method="POST" id="payment-form">
                    {{ csrf_field() }}
                    <h2>Billing Details</h2>

                    <div class="form-group">
                        <label for="email">Email Address</label>
                        @if (auth()->user())
                            <input type="email" class="form-control" id="email" name="email" value="{{ auth()->user()->email }}" readonly>
                        @else
                            <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}" required>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" required>
                    </div>
                    <div class="form-group">
                        <label for="address">Address</label>
                        <input type="text" class="form-control" id="address" name="address" value="{{ old('address') }}" required>
                    </div>

                    <div class="half-form">
                        <div class="form-group">
                            <label for="city">City</label>
                            <input type="text" class="form-control" id="city" name="city" value="{{ old('city') }}" required>
                        </div>
                        <div class="form-group">
                            <label for="province">Province</label>
                            <input type="text" class="form-control" id="province" name="province" value="{{ old('province') }}" required>
                        </div>
                    </div> <!-- end half-form -->

                    <div class="half-form">
                        <div class="form-group">
                            <label for="postalcode">Postal Code</label>
                            <input type="text" class="form-control" id="postalcode" name="postalcode" value="{{ old('postalcode') }}" required>
                        </div>
                        <div class="form-group">
                            <label for="phone">Phone</label>
                            <input type="text" class="form-control" id="phone" name="phone" value="{{ old('phone') }}" required>
                        </div>
                    </div> <!-- end half-form -->

                    <div class="spacer"></div>

                    <h2>Payment Details</h2>

                    <div class="form-group">
                        <label for="name_on_card">Name on Card</label>
                        <input type="text" class="form-control" id="name_on_card" name="name_on_card" value="">
                    </div>

                    <div class="form-group">
                        <label for="card-element">
                            Credit or debit card
                        </label>
                        <div id="card-element">
                            <!-- a Stripe Element will be inserted here. -->
                        </div>

                        <!-- Used to display form errors -->
                        <div id="card-errors" role="alert"></div>
                    </div>
                    <div class="spacer"></div>

                    <button type="submit" id="complete-order" class="w3-btn w3-blue full-width" style="position:absolute; left: 0;">Complete Order</button>

                </form>
                <div class="w3-container">
                    @if ($message = Session::get('success'))
                        <div class="w3-panel w3-green w3-display-container">
            <span onclick="this.parentElement.style.display='none'"
                  class="w3-button w3-green w3-large w3-display-topright">&times;</span>
                            <p>{!! $message !!}</p>
                        </div>
                        <?php Session::forget('success');?>
                    @endif

                    @if ($message = Session::get('error'))
                        <div class="w3-panel w3-red w3-display-container">
            <span onclick="this.parentElement.style.display='none'" class="w3-button w3-red w3-large w3-display-topright">&times;</span>
                            <p>{!! $message !!}</p>
                        </div>
                        <?php Session::forget('error');?>
                    @endif
                    <form method="POST" id="payment-form"
                          action="{!! URL::to('paypal') !!}">
                        {{ csrf_field() }}
                        <button class="w3-btn w3-blue">Pay with PayPal</button>
                    </form>
                </div>
            </div>
        </div> <!-- /row -->
    </div> <!-- /cart_table -->
@endsection
