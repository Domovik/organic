@extends('layouts.app')
@section('content')
    <!-- Banner ____________________________________ -->
    <div id="banner">
        <div class="rev_slider_wrapper">
            <!-- START REVOLUTION SLIDER 5.0 auto mode -->
            <div id="main_slider" class="rev_slider" data-version="5.0">
                <ul>
                    @foreach($sliders as $slider)
                        <!-- SLIDE1  -->
                        <li data-index='rs-{{$slider->id}}' data-transition='curtain-1' data-slotamount='1' data-easein='default' data-easeout='default' data-masterspeed='default' data-thumb='images/home/slide-1.jpg' data-rotate='0' data-saveperformance='off' data-title='Business Solutions' data-description='' >
                            <!-- MAIN IMAGE -->
                            <img src="{{Voyager::image($slider->image_background)}}" alt="image" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg">
                            <!-- LAYERS -->

                            <!-- LAYER NR. 1 -->
                            <div class="tp-caption tp-resizeme rs-parallaxlevel-3"
                                 data-x="['center','center','center','center']" data-hoffset="['0','0','35','0']"
                                 data-y="['middle','middle','middle','middle']" data-voffset="['-120','-120','-120','-120']"
                                 data-width="none"
                                 data-height="none"
                                 data-transform_idle="o:1;"
                                 data-transform_in="z:0;rX:0deg;rY:0;rZ:0;sX:1.5;sY:1.5;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;"
                                 data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                                 data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                                 data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                                 data-start="1000"
                                 data-splitout="none"
                                 data-responsive_offset="on"
                                 data-elementdelay="0.05"
                                 style="z-index: 5;">
                                <img src="{{Voyager::image($slider->image)}}" alt="">
                            </div>

                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption tp-resizeme rs-parallaxlevel-3"
                                 data-x="['center','center','center','center']" data-hoffset="['0','0','35','0']"
                                 data-y="['middle','middle','middle','middle']" data-voffset="['50','50','50','50']"
                                 data-width="none"
                                 data-height="none"
                                 data-transform_idle="o:1;"
                                 data-transform_in="z:0;rX:0deg;rY:0;rZ:0;sX:1.5;sY:1.5;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;"
                                 data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                                 data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                                 data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                                 data-start="1500"
                                 data-splitout="none"
                                 data-responsive_offset="on"
                                 data-elementdelay="0.05"
                                 style="z-index: 5;">
                                <h1>{{ $slider->title }}</h1>
                            </div>

                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption tp-resizeme rs-parallaxlevel-2"
                                 data-x="['center','center','center','center']" data-hoffset="['0','0','35','0']"
                                 data-y="['middle','middle','middle','middle']" data-voffset="['110','110','110','110']"
                                 data-width="none"
                                 data-height="none"
                                 data-whitespace="nowrap"
                                 data-transform_idle="o:1;"
                                 data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                                 data-mask_in="x:0px;y:[100%];"
                                 data-mask_out="x:inherit;y:inherit;"
                                 data-start="2000"
                                 data-splitin="none"
                                 data-splitout="none"
                                 data-responsive_offset="on"
                                 style="z-index: 6; white-space: nowrap;">
                                <h5 class="cp-title">{{ $slider->sup_title }} </h5>
                            </div>

                            <!-- LAYER NR. 4 -->
                            <div class="tp-caption tp-resizeme rs-parallaxlevel-2"
                                 data-x="['center','center','center','center']" data-hoffset="['0','0','35','0']"
                                 data-y="['middle','middle','middle','middle']" data-voffset="['160','160','160','160']"
                                 data-width="none"
                                 data-height="none"
                                 data-whitespace="nowrap"
                                 data-transform_idle="o:1;"
                                 data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                                 data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                                 data-mask_in="x:0px;y:[100%];"
                                 data-mask_out="x:inherit;y:inherit;"
                                 data-start="2500"
                                 data-splitin="none"
                                 data-splitout="none"
                                 data-responsive_offset="on"
                                 style="z-index: 6; white-space: nowrap;">
                                <h5 class="cp-title-2">{!!  $slider->description !!}</h5>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div><!-- END REVOLUTION SLIDER -->
    </div> <!-- End of #banner -->

    <!-- about Section ************************** -->
    @if($sales->IsNotEmpty())
        <div class="about_section">
            <div class="container">
                <div class="row">
                    @foreach($sales as $sale)
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12t">
                            <div class="item wow fadeInLef" style="background-image: url({{Voyager::image($sale->image)}});">
                                <div class="offer-sec">
                                    <div class="inner-title">{{$sale->title}}<div class="offer"><span>{{$sale->offer}} <br>OFF</span></div></div>
                                </div>
                                <div class="content">
                                    <h3>{{$sale->title}}</h3>
                                    <p>{!! $sale->description !!}</p>
                                    <div class="link-btn"><a href="{{$sale->link}}" class="tran3s">More Products<span class="fa fa-sort-desc"></span></a></div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div> <!-- End of .container -->
        </div> <!-- End of .welcome_section -->
    @endif

    <!--feature Section-->
    <section class="featured-product">
        <div class="container">
            <div class="theme_title center">
                <h3>{{setting('home.1_title')}}</h3>
            </div>
            <!--Filter-->
            <div class="filters text-center">
                <ul class="filter-tabs filter-btns clearfix">
                    <li class="filter active" data-role="button" data-filter="all"><span class="txt">All Products</span></li>
                    @foreach($product_categories as $product_category)
                        <li href="" class="filter" data-role="button" data-filter=".{{$product_category->title}}"><span class="txt">{{$product_category->title}}</span></li>
                    @endforeach
                </ul>
            </div>

            <div class="row filter-list clearfix" id="MixItUp717B05">
                @foreach($products_feature as $product)
                    <!--Default Item-->
                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 mix mix_all default-item {{$product->category->title}} all " style="display: inline-block;">
                        <div class="inner-box">
                            <div class="single-item center">
                                <figure class="image-box"><img src="{{Voyager::image($product->image)}}" alt="">@if($product->is_new)<div class="product-model new">New</div>@endif @if($product->is_hot)<div class="product-model hot">Hot</div>@endif</figure>
                                <div class="content">
                                    <h3><a href="{{route('product_detail', $product->slug)}}">{{$product->title}}</a></h3>
                                    <div class="rating"><span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span></div>
                                    <div class="price">${{$product->price}}<span class="prev-rate">${{$product->old_price}}</span></div>
                                </div>
                                <div class="overlay-box">
                                    <div class="inner">
                                        <div class="top-content">
                                            <ul>
                                                <li><a href="#"><span class="fa fa-eye"></span></a></li>
                                                <li class="tultip-op"><span class="tultip"><i class="fa fa-sort-desc"></i>ADD TO CART</span><a href="#"><span class="icon-icon-32846"></span></a>

                                                </li>
                                                <li><a href="#"><span class="fa fa-heart-o"></span></a></li>
                                            </ul>
                                        </div>
                                        <div class="bottom-content">
                                            <h4><a href="{{route('product_detail', $product->slug)}}">Сontains:</a></h4>
                                            <p>{{$product->contains}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section><!-- End of section -->

    <!-- Request Quote ******************************* -->
    <section class="why_choose_us">
        <div class="theme_title_bg" style="background-image: url(images/background/1.jpg);">
            <div class="theme_title center">
                <div class="container">
                    <h2>{{setting('home.2_title')}}</h2>
                    {!! setting('home.3_title') !!}
                </div>
            </div>
        </div>

        <div class="container">
            <!-- End of .theme_title_center -->
            <div class="row">
                @foreach($advantages as $advantage)
                    <!-- ______________ Item _____________ -->
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="choose_us_item tran3s">
                            <div class="icon p_color_bg border_round float_left"><span class="{{$advantage->icon}}"></span></div> <!-- End of .icon -->
                            <div class="text float_left">
                                <h5 class="tran3s">{{$advantage->title}}</h5>
                                <p class="tran3s">{!! $advantage->description !!}</p>
                            </div> <!-- End of .text -->
                            <div class="clear_fix"></div>
                        </div> <!-- End of .choose_us_item -->
                    </div> <!-- End of .col -->
                @endforeach
            </div>
        </div> <!-- End of .container -->
    </section>

    <!--gallery Section-->
    <section class="gallery">
        <div class="container">
            <div class="theme_title">
                <h3>{{setting('home.4_title')}}</h3>
            </div>
            <div class="row filter-list clearfix">
                @foreach($products_top as $product)
                        <!--Default Item-->
                        <div class="col-md-4 col-sm-6 col-xs-12 mix mix_all default-item all beauty others" style="display: inline-block;">
                            <div class="inner-box">
                                <div class="single-item center">
                                    <figure class="image-box"><img src="{{Voyager::image($product->image)}}" alt=""></figure>
                                    <div class="overlay-box">
                                        <div class="inner">
                                            <div class="image-view">
                                                <div class="icon-holder">
                                                    <a href="{{Voyager::image($product->image)}}" class="fancybox" data-fancybox-group="home-gallery" title="Gardener Gallery"><span class="icon-magnifier"></span></a>
                                                </div>
                                            </div>
                                            <div class="bottom-content">
                                                <h4><a href="{{route('product_detail', $product->slug)}}">{{$product->title}}</a></h4>
                                                <div class="price">{{$product->price}} <span class="prev-rate">{{$product->old_price}}</span></div>
                                                <div class="icon-box"><a href="shop-cart.html"><span class="icon-icon-32846"></span></a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                @endforeach
            </div>
        </div>
    </section><!-- End of section -->
    <section class="news">
        <div class="container">
            <div class="theme_title center">
                <h3>{{setting('home.5_title')}}</h3>
            </div>
        <div class="row">
            @foreach($blogs as $blog)
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="blogList_single_post clear_fix wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
                        <div class="img_holder">
                            <img src="{{Voyager::image($blog->image_short)}}" alt="News" class="img-responsive">
                            <div class="opacity tran3s">
                                <div class="icon">
                                    <span><a href="{{route('blog_detail', $blog->slug)}}" class="border_round">+</a></span>
                                </div> <!-- End of .icon -->
                            </div> <!-- End of .opacity -->
                        </div> <!-- End of .img_holder -->
                        <div class="post">
                            <ul>
                                <li><a href="{{route('blog_detail', $blog->id)}}" class="tran3s"><i class="fa fa-tag" aria-hidden="true"></i>{{$blog->category->title}} </a></li>
                                <li><a href="{{route('blog_detail', $blog->slug)}}" class="tran3s"><i class="fa fa-clock-o" aria-hidden="true"></i>{{date('d F Y H:i', strtotime($blog->created_at))}}</a></li>
                                <li><a href="{{route('blog_detail', $blog->id)}}" class="tran3s"><i class="fa fa-comments" aria-hidden="true"></i> {{$blog->review->count()}}</a></li>
{{--                                {{dd($blog->review)}}--}}
                            </ul>
                            <div class="text">
                                <h4><a href="{{route('blog_detail', $blog->slug)}}">{{$blog->title}}</a></h4>
                                <p>{!! $blog->short_description !!}</p>
                                <div class="link"><a href="{{route('blog_detail', $blog->id)}}" class="tran3s">READ MORE<span class="fa fa-sort-desc"></span></a></div>
                            </div>
                        </div> <!-- End of .post -->
                    </div>
                </div>
            @endforeach
        </div>
    </section>

    <!-- Our Service ****************************** -->
    <div class="our_farmer">
        <div class="container">
            <div class="theme_title center">
                <h2>{{setting('home.6_title')}}</h2>
            </div>

            <div class="row">
                <div class="service_slider owl-carousel owl-theme">
                    @foreach($workers as $worker)
                        <div class="item center">
                            <div class="img_holder">
                                <img src="{{Voyager::image($worker->image)}}" alt="images">
                                <div class="overlay tran3s">
                                    <div class="inner-box">
                                        <ul>
                                            <li><a href="#"><span class="fa fa-facebook"></span></a></li>
                                            <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                                            <li><a href="#"><span class="fa fa-google-plus"></span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="text">
                                <h4>{{$worker->name}}</h4>
                                <a ><h5>{{$worker->profession}}</h5></a>
                                <p> {!! $worker->description !!}</p>
                            </div>
                        </div> <!-- End of .item -->
                    @endforeach
                </div> <!-- End of .service_slider -->
            </div>
        </div>
    </div> <!-- End of .our_service -->

    <!--Testimonials Section-->
    <section class="testimonials-section" style="background-image:url(images/parallax/1.jpg);">
        <div class="container">
            <div class="theme_title">
                <h2>{{setting('home.7_title')}}</h2>
            </div>
            <div class="testimonials-carousel">
                @foreach($testimonials as $testimonial)
                    <!--Slide Item-->
                    <div class="slide-item">
                        <div class="inner-box">
                            <div class="content">
                                <div class="text-bg">
                                    <div class="quote-icon"><span class="fa fa-quote-left"></span></div>
                                    <div class="text">{!! $testimonial->description!!}</div>
                                </div>
                                <div class="info clearfix">
                                    <div class="author-thumb"><img src="{{Voyager::image($testimonial->image)}}" alt=""></div>
                                    <div class="author">{{$testimonial->name}}</div>
                                    <div class="author-title">{{$testimonial->profession}}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
             </div>
        </div>
    </section>

    <!-- Partner Logo********************** -->
    <div class="partners wow fadeInUp">
        <div class="container">
            <div id="partner_logo" class="owl-carousel owl-theme">
                @foreach($partners as $partner)
                    <div class="item"><img src="{{Voyager::image($partner->image)}}" alt="{{$partner->title}}"></div>
                @endforeach
            </div> <!-- End .partner_logo -->
        </div>
    </div>
    @include('components.subscribe')
@stop
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.min.js" integrity="sha512-VZ6m0F78+yo3sbu48gElK4irv2dzPoep8oo9LEjxviigcnnnNvnTOJRSrIhuFk68FMLOpiNz+T77nNY89rnWDg==" crossorigin="anonymous"></script>

@stop
