
@extends('layouts.app')
@section('content')
    <section class="breadcrumb-area" style="background-image:url(images/background/2.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumbs text-center">
                        <h1>Contact us</h1>
                        <h4>Welcome to certified online organic products suppliersr</h4>
                    </div>
                </div>
            </div>
        </div>

        <div class="breadcrumb-bottom-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-md-5 col-sm-5">
                        {{ Breadcrumbs::render('contact') }}
                    </div>
                    <div class="col-lg-4 col-md-7 col-sm-7">
                        <p>{{setting('header.We')}}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

			<section class="single-contact_us">
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="left_contact">
								<h5>{{setting('contact.title_contact')}}</h5>
								<ul class="list catagories">
		                            <li><a href="#"><i class="fa fa-home color1"></i>{{setting('contact.adress')}}</a></li>
		                            <li><a href="#"><i class="fa fa-envelope color1"></i><span>{{setting('contact.contact_email')}}</span><br>customersupport@organic.com</a></li>
		                            <li><a href="#"><i class="fa fa-phone color1"></i>{{setting('contact.contact_phone')}}<br>1800-8692-258-1547</a></li>
		                        </ul>

		                        <div class="border-area">
			                        <h6>Woriking Hours</h6>
									<div class="list Business">
			                            {!! setting('contact.work_time') !!}
			                        </div>
		                        </div>
							</div>
						</div>
						<div class="col-md-8 col-sm-6 col-xs-12">
                            @include('components.feedback')
			            </div>
					</div>
				</div>
			</section>

			<!-- Google map************************ -->
			<section id="google-map-area">
				<div class="google-map-home" id="google-map" data-map-lat="40.717873" data-map-lng="-73.563033" data-icon-path="images/logo/map.png" data-map-title="Awesome Place" data-map-zoom="11"></div>
	   		</section>
@endsection

