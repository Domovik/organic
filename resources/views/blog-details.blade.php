@extends('layouts.app')
@section('content')


			<section class="breadcrumb-area" style=" background-image:url({{asset('images/background/2.jpg')}});">
			    <div class="container">
			        <div class="row">
			            <div class="col-md-12">
			                <div class="breadcrumbs text-center">
			                    <h1>{{setting('blog-detail.blog_detail_title')}}</h1>
			                    <h4>{{setting('blog-detail.blog_detail_subtitle')}}</h4>
			                </div>
			            </div>
			        </div>
			    </div>
				<div class="breadcrumb-bottom-area">
				    <div class="container">
				        <div class="row">
				            <div class="col-lg-8 col-md-5 col-sm-5">
				                <ul>
				                    <li><a href="{{route('home')}}">Home</a></li>
				                    <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
				                    <li>{{$blog_detail->title}}</li>
				                </ul>
				            </div>
				            <div class="col-lg-4 col-md-7 col-sm-7">
				                <p>{{setting('header.We')}}</p>
				            </div>
				        </div>
				    </div>
				</div>

			</section>

			<section class="news single_news_page with_sidebar news_single">
				<div class="container">
					<div class="row">
						<div class="col-md-9 col-sm-8 col-xs-12">
							<div class="single_left_bar">
								<div class="blogList_single_post clear_fix wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
		        					<div class="img_holder">
										<img src="{{Voyager::image($blog_detail->image_full)}}" alt="News" class="img-responsive">
										<div class="opacity tran3s">
											<div class="icon">
												<span><a href="" class="border_round">+</a></span>
											</div> <!-- End of .icon -->
										</div> <!-- End of .opacity -->
									</div> <!-- End of .img_holder -->
									<div class="post">

										<div class="text">
											<h4><a href="">Interesting facts about organic food and organic store.</a></h4>
											<ul>
												<li><a href="" class="tran3s"><i class="fa fa-user" aria-hidden="true"></i>{{$blog_detail->author->name}}</a></li>
												<li><a href="" class="tran3s"><i class="fa fa-tag" aria-hidden="true"></i> {{$blog_detail->category->title}}</a></li>
												<li><a href="" class="tran3s"><i class="fa fa-clock-o" aria-hidden="true"></i>  {{date('d F Y H:i', strtotime($blog_detail->created_at))}}</a></li>
												<li><a href="" class="tran3s"><i class="fa fa-comments" aria-hidden="true"></i>{{$blog_detail->review->count()}}</a></li>
											</ul>
                                            {!! $blog_detail->description !!}
										</div>

									</div> <!-- End of .post -->

									<div class="author-post">
										<div class="comment-box">
							                <div class="theme_title ">
							                    <h2>about author</h2>

							                </div>
							                <div class="single-comment-box">
							                    <div class="img-box">
							                        <img src="{{Voyager::image($blog_detail->author->image)}}" alt="Awesome Image">
							                    </div>
							                    <div class="text-box">
							                        <div class="top-box">
							                            <h2>{{$blog_detail->author->name}}</h2>
							                        </div>
							                        <div class="content">
                                                        {!! $blog_detail->author->description !!}
							                        </div>
							                    </div>
							                </div>
							            </div>
									</div>
									<div class="comment-box">
						                <div class="theme_title ">
						                    <h2>Comments</h2>
						                </div>
                                        @foreach($blog_detail->review as $comment)
                                            <div class="single-comment-box">
                                                <div class="text-box">
                                                    <div class="clearfix">
                                                         <div class="top-box float_left">
                                                            <h2>{{$comment->name}}</h2>
                                                        </div>
                                                        <div class="float_right">
                                                            <span class="p_color">{{date('d F Y H:i', strtotime($comment->created_at))}}</span>
                                                        </div>
                                                    </div>

                                                    <div class="content">
                                                        {{$comment->comment}}
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
						            </div>
						            <div class="reply-box">
						                <div class="theme_title ">
						                    <h2>leave a commet</h2>
						                </div>
                                        <form method="post" id="comment_blog" action="{{route('comment_blog')}}">
                                            @csrf
                                            @if ($errors->any())
                                                <div class="alert alert-danger">
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <textarea type="comment" name="comment" placeholder="Comments"></textarea>
                                                </div>
                                                <!-- /.col-md-12 -->
                                                <div class="col-md-6">
                                                    <input name="name" placeholder="Your Name*">
                                                </div>
                                                <!-- /.col-md-6 -->
                                                <div class="col-md-6">
                                                    <input name="email" placeholder="Your Email*">
                                                </div>
                                                <!-- /.col-md-6 -->
                                                <div class="col-md-12">
                                                    <input name="site" placeholder="Your Website">
                                                </div>
                                                <div class="col-md-12">
                                                    <input name="blog_id" type="hidden" value="{{$blog_detail->id}}">
                                                </div>

                                                <!-- /.col-md-6 -->
                                                <div class="col-md-12">
                                                    <button type="submit" class="color1_bg">Post Comment</button>
                                                </div>
                                                <!-- /.col-md-12 -->
                                            </div>
						                    <!-- /.row -->
                                        </form>
						            </div>
                                    <script>
                                        let form = document.getElementById('comment_blog')
                                        form.addEventListener("submit", function (event) {
                                            event.preventDefault()
                                            const data = new FormData(form)
                                            axios.post('{{route('comment_blog')}}', data).then(function (response) {
                                                form.insertAdjacentHTML('beforeend', '<div class="alert" id="min" style="color: white; text-align: center; background: darkgreen; animation: hideDiv 2s forwards ;"><ul><li>Комментарий ожидает модерации</li></ul></div>');
                                                const d1 = document.getElementById('min')
                                                setTimeout(function() {
                                                    d1.remove();
                                                },2000);

                                            })

                                        })
                                    </script>
		        				</div>

							</div>
						</div>

						<!-- _______________________ SIDEBAR ____________________ -->
	        			<div class="col-md-3 col-sm-4 col-xs-12 sidebar_styleTwo">
	        				<div class="wrapper">
	        					<div class="sidebar_search">
	        						<form action="#">
	        							<input type="text" placeholder="Search heare...">
	        							<button class="tran3s color1_bg"><i class="fa fa-search" aria-hidden="true"></i></button>
	        						</form>
	        					</div> <!-- End of .sidebar_styleOne -->

	        					<div class="sidebar_categories">
	        						<div class="theme_inner_title">
										<h4>Categories</h4>
									</div>
									<ul>
                                        @foreach($categories as $category)
										    <li><a href="{{route('blog')}}?blog_category_id={{$category->id}}" class="tran3s">{{$category->title}}</a></li>
                                        @endforeach
									</ul>
	        					</div> <!-- End of .sidebar_categories -->

	        					<div class="popular_news">
									<div class="theme_inner_title">
										<h4>popular news</h4>
									</div>

									<div class="recent-posts">
                                        @foreach($footer_blogs as $blog)
                                            <div class="post">
                                                <div class="post-thumb"><a href="#"><img src="{{Voyager::image($blog->image_full)}}" alt=""></a></div>
                                                <h4><a href="#">{{$blog->title}}</a></h4>
                                                <div class="post-info"><i class="fa fa-clock-o"></i>{{ $blog->created_at }}</div>
                                            </div>
                                        @endforeach
									</div>
								</div>
	        				</div> <!-- End of .wrapper -->
	        			</div> <!-- End of .sidebar_styleTwo -->

					</div>
				</div>
			</section>
@endsection
