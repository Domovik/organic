@extends('layouts.app')
@section('content')
			<section class="breadcrumb-area" style="background-image:url(images/background/2.jpg);">
			    <div class="container">
			        <div class="row">
			            <div class="col-md-12">
			                <div class="breadcrumbs text-center">
			                    <h1>{{setting('testimonials.title_testimonials')}}</h1>
			                    <h4>{{setting('testimonials.subtitle_testimonials')}}</h4>
			                </div>
			            </div>
			        </div>
			    </div>
				<div class="breadcrumb-bottom-area">
				    <div class="container">
				        <div class="row">
				            <div class="col-lg-8 col-md-5 col-sm-5">
				                <ul>
                                    {{ Breadcrumbs::render('about-us') }}
				                </ul>
				            </div>
				            <div class="col-lg-4 col-md-7 col-sm-7">
				                <p>{{setting('header.We')}}</p>
				            </div>
				        </div>
				    </div>
				</div>
			</section>
		    <!--Testimonials Section-->

		    <section class="testimonials-section single_testimonial">
		        <div class="container">
		            <div class="row">
                        @foreach($testimonials as $testimonial)
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <!--Slide Item-->
                                <div class="slide-item">
                                    <div class="inner-box">
                                        <div class="content">
                                            <div class="text-bg">
                                                <div class="quote-icon"><span class="fa fa-quote-left"></span></div>
                                                <div class="text">{!! $testimonial->description !!}</div>
                                            </div>
                                            <div class="info clearfix">
                                                <div class="author-thumb"><img src="{{Voyager::image($testimonial->image)}}" alt=""></div>
                                                <div class="author">{{$testimonial->name}}</div>
                                                <div class="author-title">{{$testimonial->profession}}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
		            </div>
		        </div>
		    </section>
@endsection
