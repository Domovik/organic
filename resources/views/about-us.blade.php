@extends('layouts.app')
@section('content')


<section class="breadcrumb-area" style="background-image:url(images/background/2.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumbs text-center">
                    <h1>{{setting('about-us.page_name_about_us')}}</h1>
                    <h4>{{setting('about-us.about_first_title')}}</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="breadcrumb-bottom-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-5 col-sm-5">
                    <ul>
                        {{ Breadcrumbs::render('about-us') }}
                    </ul>
                </div>
                <div class="col-lg-4 col-md-7 col-sm-7">
                    <p>{{setting('header.We')}}</p>
                </div>
            </div>
        </div>
    </div>

</section>

<section class="about-story">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <figure class="img-holder">
                    <img src="{{Voyager::image(setting('about-us.first_block_image'))}}" alt="">
                </figure>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="about-text">
                    <div class="theme_title">
                        <h3>{{setting('about-us.title_first_block')}}</h3>
                    </div>
                    {!! setting('about-us.story_about') !!}
                </div>
            </div>
        </div>
    </div>
</section>

<section class="three-column">
    <div class="contaner-fluid">
        <div class="row">
            @foreach($advertisements as $advertisement)
                <div class="col-md-4 col-sm-12 col-xs-12 color1_bg text-center">
                    <div class="single-item ">
                        <h5>{{$advertisement->title}}</h5>
                        <div class="icon"><i class="{{$advertisement->icon}}"></i></div>
                        {!! $advertisement->description !!}
                        <div class="link"><a href="#">Shop Now</a></div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
<section class="four-column">
    <div class="container text-center">
        <div class="theme_title center">
            <h3>{{setting('about-us.second_block_about_us')}}</h3>
        </div>
        <div class="row">
            @foreach($deliveries as $delivery)
                <div class="col-md-3 col-sm-6 col-xs-12 text-center">
                        <div class="single-item ">
                            <div class="inner-box"><div class="icon"><i class="{{$delivery->icon}}"></i><span>{{$delivery->number}}</span></div></div>
                            <h5>{{$delivery->title}}</h5>
                            {!! $delivery->description !!}
                        </div>
                    </div>
            @endforeach
        </div>
        <div class="link"><a href="{{route('shop')}}" class="rot tran3s color1_bg">Shop products</a></div>
    </div>
</section>



<section class="gallery gallery-grid about-gallery" style="background-image:url(images/background/3.jpg);">
    <div class="tab-links">
        <div class="container">
            <div class="iblock">
                <div class="theme_title text-left">
                    <h2>{{setting('about-us.third_block_about_us')}}</h2>
                </div>
            </div>

            <ul class="tab-list">
                @foreach($product_categories as $product_category)
                    <li><a href="#{{$product_category->title}}" class="tab-btn active"><h2>{{$product_category->title}}</h2></a></li>
                @endforeach
            </ul>
            <div class="link-btn"><a href="{{route('shop')}}" class="tran3s">VIEW MORE<span class="fa fa-sort-desc"></span></a></div>
        </div>
    </div>
                    <!--tab Content-->
    <div class="tab-content">
        <div class="container-fluid">
            @foreach($product_categories as $product_category)
                <!--tab Details / Collapsed-->
                <div class="item collapsed" id="{{$product_category->title}}">
                    <div class="row-10">
                        @foreach($product_category->products as $product)
                            <div class="col-md-2 column-2 col-sm-6 col-xs-12 default-item">
                                <div class="inner-box">
                                    <div class="single-item center">
                                        <figure class="image-box"><img src="{{Voyager::image($product->image)}}" alt=""></figure>
                                        <div class="overlay-box">
                                            <div class="inner">
                                                <div class="image-view">
                                                    <div class="icon-holder">
                                                        <a href="{{Voyager::image($product->image)}}" class="fancybox"><span class="icon-magnifier"></span></a>
                                                    </div>
                                                </div>
                                                <div class="bottom-content">
                                                    <h4><a href="{{route('product_detail', $product->slug)}}">{{$product->title}}</a></h4>
                                                    <div class="price">{{$product->price}} <span class="prev-rate">{{$product->old_price}}</span></div>
                                                    <div class="icon-box"><a href="shop-cart.html"><span class="icon-icon-32846"></span></a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>


<section class="award">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="award-info">
                    <div class="theme_title">
                        <h2>{{setting('about-us.fourth_block_about_us')}}</h2>
                    </div>
                </div>
                {!! setting('about-us.award_text') !!}
            </div>
            <div class="col-md-6 col-sm-12">
                @include('components.feedback')
            </div>
        </div>
    </div>
</section>
@include('components.subscribe')


@endsection
