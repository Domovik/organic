@extends('layouts.app')

@section('content')


			<section class="breadcrumb-area" style="background-image:url(images/background/2.jpg);">
			    <div class="container">
			        <div class="row">
			            <div class="col-md-12">
			                <div class="breadcrumbs text-center">
			                    <h1>{{setting('all-product.shop_title')}}</h1>
			                    <h4>{{setting('all-product.sub_title')}}</h4>
			                </div>
			            </div>
			        </div>
			    </div>
				<div class="breadcrumb-bottom-area">
				    <div class="container">
				        <div class="row">
				            <div class="col-lg-8 col-md-5 col-sm-5">
				                <ul>
                                    {{ Breadcrumbs::render('shop') }}
				                </ul>
				            </div>
				            <div class="col-lg-4 col-md-7 col-sm-7">
				                <p>{{setting('header.We')}}</p>
				            </div>
				        </div>
				    </div>
				</div>

			</section>


			<!-- Shop Page Content************************ -->
	        <div class="shop_page featured-product">
	        	<div class="container">
	        		<div class="row">
	        			<div class="col-lg-9 col-md-8 col-sm-12 col-sx-12">

		        			<div class="row">
                                @php
                                    if($products_filter->isNotEmpty()) $products = $products_filter;
                                @endphp

                                @foreach($products as $product)
                                    <!--Default Item-->
                                    <div class="col-md-4 col-sm-6 col-xs-12 default-item" style="display: inline-block;">
                                        <div class="inner-box">
                                            <div class="single-item center">
                                                <figure class="image-box"><img src="{{Voyager::image($product->image)}}"alt="">@if($product->is_new)<div class="product-model new">New</div>@endif @if($product->is_hot)<div class="product-model hot">Hot</div>@endif</figure>
                                                <div class="content">
                                                    <h3><a href="shop-single.html">{{$product->title}}</a></h3>
                                                    <div class="rating"><span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span></div>
                                                    <div class="price">{{$product->price}} <span class="prev-rate">{{$product->old_price}}</span></div>
                                                </div>
                                                <div class="overlay-box">
                                                    <div class="inner">
                                                        <div class="top-content">
                                                            <ul>
                                                                <li><a href="#"><span class="fa fa-eye"></span></a></li>
                                                                <li class="tultip-op"><span class="tultip"><i class="fa fa-sort-desc"></i>ADD TO CART</span><button data-endpoint="{{route('cart_sent', $product->id)}}" class="add_product"><span class="icon-icon-32846"></span></button>
                                                                </li>
                                                                <li><a href="#"><span class="fa fa-heart-o"></span></a></li>
                                                            </ul>
                                                        </div>
                                                        <div class="bottom-content">
                                                            <h4><a href="{{route('product_detail', $product->slug)}}">It Contains:</a></h4>
                                                            <p>{{$product->contains}}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
			        		</div>
			        	</div>

						<!-- _______________________ SIDEBAR ____________________ -->
	        			<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 sidebar_styleTwo">
                            <aside>
                                <form action="/shop">
                                    <div class="form-group">
                                        <lable for="product" class="from-control">Product</lable>
                                        <input type="text" class="from-control" name="filter" value="{{ request()->filter }}">
                                    </div>
                                    <button type="submit" class="btn btn-primary">SeArCh</button>
                                </form>
                            </aside>
	        				<div class="wrapper">
	        					<div class="sidebar_categories">
	        						<div class="theme_inner_title">
										<h4>Categories</h4>
									</div>
									<ul>
                                        @foreach($categories as $category)
										    <li><a href="?slug={{$category->slug.(request()->has('min','max') ? '&min='.request()->get('min').'&max='.request()->get('max'):'')}}" class="tran3s">{{$category->title}}</a></li>
                                        @endforeach
									</ul>
	        					</div> <!-- End of .sidebar_categories -->

	        					<div class="price_filter wow fadeInUp">
									<div class="theme_inner_title">
										<h4>Filter By Price</h4>
									</div>
									<div class="single-sidebar price-ranger">
										<div id="slider-range"></div>
										<div class="ranger-min-max-block">
                                            <form method="get" action="" id="filter" href="#">
                                                <input type="submit" id="send" value="Filter">
                                                <span>Price:</span>
                                                <input type="text" readonly class="min" >
                                                <span>-</span>
                                                <input type="text" readonly class="max">
                                                <input type="hidden" name="min" id="min">
                                                <input type="hidden" name="max" id="max" >

                                                @if(request()->has('product_category_id'))
                                                    <input type="hidden" name="product_category_id"  value="{{request()->get('product_category_id')}}" >
                                                @endif
                                            </form>
										</div>
									</div> <!-- /price-ranger -->
								</div> <!-- /price_filter -->

                            <div class="best_sellers clear_fix wow fadeInUp">
                                <div class="theme_inner_title">
                                    <h4>popular products</h4>
                                </div>
                                @foreach($popular_products as $popular_product)
                                    <div class="best_selling_item clear_fix border">
                                        <div class="img_holder float_left">
                                            <img src="{{Voyager::image($popular_product->image)}}" alt="image">
                                        </div> <!-- End of .img_holder -->

                                        <div class="text float_left">
                                            <a href="{{route('product_detail', $popular_product->slug)}}"><h6>{{$popular_product->title}}</h6></a>
                                            <ul>
                                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                <li><i class="fa fa-star" aria-hidden="true"></i></li>

                                            </ul>
                                            <span>${{$popular_product->price}}</span>
                                        </div> <!-- End of .text -->
                                    </div> <!-- End of .best_selling_item -->
                                @endforeach
                                <form action="/charge" method="post" id="payment-form">
                                    <div class="form-row">
                                        <label for="card-element">
                                            Credit or debit card
                                        </label>
                                        <div id="card-element">
                                            <!-- A Stripe Element will be inserted here. -->
                                        </div>
                                        <!-- Used to display form errors. -->
                                        <div id="card-errors" role="alert"></div>
                                    </div>
                                    <button>Submit Payment</button>
                                </form>
	        				</div> <!-- End of .wrapper -->
	        			</div> <!-- End of .sidebar_styleTwo -->
	        		</div> <!-- End of .row -->
	        	</div> <!-- End of .container -->
	        </div> <!-- End of .shop_page -->

@endsection
@section('scripts')
    <script>
        if ($('.price-ranger').length) {
            $('.price-ranger #slider-range').slider({
                range: true,
                min: 0,
                max: 900,
                values: [{{request()->get('min') ?? 1}}, {{request()->get('max') ?? 900}}],
                slide: function(event, ui) {
                    console.log(ui.values[0]);
                    $('.price-ranger .ranger-min-max-block .min').val('$' + ui.values[0]);
                    $('.price-ranger .ranger-min-max-block .max').val('$' + ui.values[1]);
                    $('#min').val( ui.values[0]);
                    $('#max').val( ui.values[1]);
                }
            });
            $('.price-ranger .ranger-min-max-block .min').val('$' + $('.price-ranger #slider-range').slider('values', 0));
            $('.price-ranger .ranger-min-max-block .max').val('$' + $('.price-ranger #slider-range').slider('values', 1));
            $('#min').val($('.price-ranger #slider-range').slider('values', 0));
            $('#max').val($('.price-ranger #slider-range').slider('values', 1));
        };

    </script>
@stop

