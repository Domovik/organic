<section class="call-out">
    <div class="container">
        <div class="float_left">
            <h2>Subscribe For Newsletter</h2>
            <p>We send you latest news couple a month ( No Spam).</p>
        </div>
        <div class="float_right">
                <div class="contact-box">
                    <form method="post" id="subscribe_form" action="{{route('subscribe')}}" class="contact-form" validate="validate">
                        @csrf
                            <div class="row clearfix">
                                <div class="form-group">
                                    <input type="text"  id="name" name="name" value="" placeholder="Name*"><i class="fa fa-user" aria-hidden="true"></i>
                                </div>
                                <div class="form-group">
                                    <input type="text" id="email" name="email" value="" placeholder="Email Address*"><i class="fa fa-envelope" aria-hidden="true"></i>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn-style-one center">Submit now</button>
                                </div>
                            </div>
                    </form>
                </div>
        </div>
    </div>
</section>
<script>
    let form = document.getElementById('subscribe_form')
    form.addEventListener("submit", function (event) {
        event.preventDefault()
        const data = new FormData(form)
        axios.post('{{route('subscribe')}}', data).then(function (response) {
            form.insertAdjacentHTML('beforeend', '<div id="min" style="color: white; background: darkgreen; animation: hideDiv 2s forwards ;">подписка оформлена</div>');
            const d1 = document.getElementById('min')
            setTimeout(function() {
                d1.remove();
            },2000);
        })
    })
</script>
