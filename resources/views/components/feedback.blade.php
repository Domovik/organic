<div class="contact_in-box">
    <div class="theme-title ">
        <h2>send us massege</h2>
    </div>
    <form method="post" id="backup_form" action="{{route('feedback')}}">
        @csrf
        <div class="row">
            <div class="col-md-6">
                <i class="fa fa-user" aria-hidden="true"></i>
                <input type="text" id="name" name="name" value="" placeholder="Name*">
            </div>
            <!-- /.col-md-6 -->
            <div class="col-md-6">
                <i class="fa fa-envelope"></i>
                <input type="email" id="email" name="email" value="" placeholder="Email Address*">
            </div>
            <!-- /.col-md-6 -->
            <div class="col-md-6">
                <input type="phone" id="phone" name="phone" value="" placeholder="Phone*">
            </div>
            <!-- /.col-md-6 -->
            <div class="col-md-6">
                <input type="subject" id="subject" name="subject" value="" placeholder="Subject*">
            </div>
            <!-- /.col-md-6 -->

            <div class="col-md-12">
                <i class="fa fa-envelope"></i>
                <textarea type="comment" id="comment" name="comment" value="" placeholder="Subject*"></textarea>
            </div>
            <!-- /.col-md-12 -->
            <div class="col-md-12">
                <button type="submit" class="color1_bg">Post Comment</button>
            </div>
            <!-- /.col-md-12 -->
        </div>
        <!-- /.row -->
    </form>

</div>
<script>
    // subscribe


    // feedback
    let in_proccess = false;
    let backup_form = document.getElementById('backup_form');

    if(backup_form){
        backup_form.addEventListener("submit", function (event) {
            event.preventDefault()
            console.log(in_proccess)
            if(in_proccess){
                return false
            }
            in_proccess = true;
            const data = new FormData(backup_form)
            axios.post('{{route('feedback')}}', data).then(function (response) {
                backup_form.insertAdjacentHTML('beforeend', '<div id="min" style="color: white; background: darkgreen; animation: hideDiv 2s forwards ;">подписка оформлена</div>');
                const d1 = document.getElementById('min')
                setTimeout(function() {
                    in_proccess = false;
                    d1.remove();
                },2000);

            })

        })
    }

</script>
