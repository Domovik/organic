<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from st.ourhtmldemo.com/template/organic_store/blog-Standard  by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 06 Jul 2020 10:09:04 GMT -->
<head>

    {{--    {!! $context !!}--}}
    <meta charset="UTF-8">
    <!-- For IE -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- For Resposive Device -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
{{--    {!! $og !!}--}}
    <!-- Favicon -->
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="apple-touch-icon" sizes="57x57" href="{{asset('images/fav-icon/apple-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{asset('images/fav-icon/apple-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('images/fav-icon/apple-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href={{asset('images/fav-icon/apple-icon-76x76.png')}}"">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('images/fav-icon/apple-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('images/fav-icon/apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset('images/fav-icon/apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('images/fav-icon/apple-icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('images/fav-icon/apple-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{asset('images/fav-icon/android-icon-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('images/fav-icon/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('images/fav-icon/favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('images/fav-icon/favicon-16x16.png')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/3.0.1/css/font-awesome.min.css" integrity="sha512-lm1uyXQqevVQpYeML9XwcacdjfbbA7VsTqNSiKQaq3LnWMpx2y2DuGfNBXC5bGcZz+8xzb9T+0w1WbYJXAXGxg==" crossorigin="anonymous" />

    <!-- Custom Css -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}?key={{time()}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/responsive.css')}}">


    <!-- Fixing Internet Explorer ______________________________________-->

    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="{{asset('vendor/html5shiv.js')}}"></script>
    <![endif]-->
</head>



<body>
<div class="main_page">


    <!-- Header *******************************  -->
    <header>
        <div class="top_header">
            <div class="container">
                <div class="pull-left header_left">
                    <ul>
                        <li><a href="#">Order On Phone: <span>{{setting('header.number_phone')}}</span></a></li>
                        <li><i class="fa fa-envelope-o s_color" aria-hidden="true"></i><a href="#">{{setting('header.email')}}</a></li>
                    </ul>
                </div>
            </div> <!-- End of .container -->
        </div> <!-- End of .top_header -->

        <div class="bottom_header">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-12 col-xs-12">
                        <div class="search-box">
                            <form action="#" class="clearfix">
                                <input id="search_form" type="text" name="" placeholder="search" value="" class="search"  autocomplete="off">
                            </form>
                            <div class="search_dropdown" id="search_result">

                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-5 col-xs-6 logo-responsive">
                        <div class="logo-area">
                            <a href="" class="pull-left logo"><img src="{{Voyager::image(setting('header.logo'))}}" alt="LOGO"></a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-7 col-xs-6 pdt-14">
                        <div class="login_option float_left">
                            <div class="login_form">
                                <div class="user">
                                    <i class="icon-photo"></i>
                                </div>
                                <div class="login-info">
                                    <div class="welcome">Welcome!</div>
                                    </form>
                                </div>
                            </div> <!-- End of .cart_list -->
                        </div>
                        <div class="cart_option float_left">
                            <button class="cart tran3s dropdown-toggle" id="cartDropdown"><i class="fa icon-icon-32846" aria-hidden="true"></i><span class="s_color_bg p_color">2</span></button>
                            <div class="cart-info">
                                <div>My Cart</div>
                            </div>

                            <div class="cart_list color2_bg" id="cartresult"  aria-labelledby="cartDropdown">
                                @include('layouts.partials.cart')
                            </div> <!-- End of .cart_list -->
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- End of .bottom_header -->
    </header>

{{menu('Menu', 'layouts.partials.menu')}}




    @yield('content')






    <!-- Footer************************* -->
    <footer>
        <div class="main_footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 footer_logo">
                        <a href="index "><img src="{{ Voyager::image(setting('footer.Logo_Image')) }}" alt="Logo"></a>
                        {!! setting('footer.description_footer') !!}
                    </div> <!-- End of .footer_logo -->

                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 footer_news">
                        <h5>recent post</h5>
                        <div class="recent-posts">
                            @foreach($footer_blogs as $blog)
                                <div class="post">
                                    <div class="post-thumb"><a href="#"><img src="{{Voyager::image($blog->image_full)}}" alt=""></a></div>
                                    <h4><a href="{{route('blog_detail', $blog->slug)}}">{{$blog->title}}</a></h4>
                                    <div class="post-info"><i class="fa fa-clock-o"></i>{{date('d F Y H:i', strtotime($blog->created_at))}}</div>
                                </div>
                            @endforeach
                        </div>
                    </div> <!-- End of .footer_news -->

                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 footer_subscribe">
                        <h5>categoreis</h5>
                        <ul class="list catagories">

                            @foreach($footer_categories as $category)
                                <li><a href="{{route('shop')}}?product_category_id={{$category->id}}"><i class="fa fa-angle-right"></i>{{$category->title}}</a></li>
                            @endforeach

                        </ul>

                    </div> <!-- End of .footer_subscribe -->


                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 footer_contact">
                        <h5>{{setting('contact.title_contact')}}</h5>
                        <ul class="list catagories">
                            <li><a href="#"><i class="fa fa-envelope"></i>{{setting('contact.contact_email')}}</a></li>
                            <li><a href="#"><i class="fa fa-phone"></i>{{setting('contact.contact_phone')}}</a></li>
                            <li><a href="#"><i class="fa fa-home"></i>{{setting('contact.adress')}}</a></li>
                        </ul>

                        <h5>Business Hours</h5>
                        <div class="list Business">
                            {!! setting('contact.work_time')!!}
                        </div>

                    </div> <!-- End of .footer_contact -->
                </div>
            </div>
        </div> <!-- End of .main_footer -->

        <div class="bottom_footer clear_fix">
            <div class="container">
                <h6 class="pull-left">Copyrights © 2015 All Rights Reserved by<a href="http://themeforest.net/user/steelthemes/portfolio" target="_blank">Steelthemes</a></h6>
                <ul class="social_icon pull-right">
                    <li><a href="#" class="tran3s"><i class="fa fa-cc-visa" aria-hidden="true"></i></a></li>
                    <li><a href="#" class="tran3s"><i class="fa fa-cc-mastercard" aria-hidden="true"></i></a></li>
                    <li><a href="#" class="tran3s"><i class="fa fa-paypal" aria-hidden="true"></i></a></li>
                    <li><a href="#" class="tran3s"><i class="fa fa-credit-card-alt" aria-hidden="true"></i></a></li>
                    <li><a href="#" class="tran3s"><i class="fa fa-cc-discover" aria-hidden="true"></i></a></li>
                </ul>
            </div>
        </div>
    </footer>


    <!-- Scroll Top Button -->
    <button class="scroll-top tran3s color2_bg">
        <span class="fa fa-angle-up"></span>
    </button>
    <!-- pre loader  -->
    <div id="loader-wrapper">
        <div id="loader"></div>
    </div>

    <!-- Js File_________________________________ -->

    <!-- j Query -->
    <script type="text/javascript" src="{{asset('js/jquery-2.1.4.js')}}"></script>
    <!-- Bootstrap JS -->
    <script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>

    <!-- Vendor js _________ -->
    <!-- Google map js -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRvBPo3-t31YFk588DpMYS6EqKf-oGBSI"></script> <!-- Gmap Helper -->
    <script src="{{asset('js/gmap.js')}}"></script>
    <!-- owl.carousel -->
    <script type="text/javascript" src="{{asset('js/owl.carousel.min.js')}}"></script>
    <!-- ui js -->
    <script type="text/javascript" src="{{asset('js/jquery-ui.min.js')}}"></script>
    <!-- Responsive menu-->
    <script type="text/javascript" src="{{asset('js/menuzord.js')}}"></script>
    <script src="https://use.fontawesome.com/8f98423aad.js"></script>
    <!-- revolution -->
    <script src="{{asset('vendor/revolution/jquery.themepunch.tools.min.js')}}"></script>
    <script src="{{asset('vendor/revolution/jquery.themepunch.revolution.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('vendor/revolution/revolution.extension.slideanims.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('vendor/revolution/revolution.extension.layeranimation.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('vendor/revolution/revolution.extension.navigation.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('vendor/revolution/revolution.extension.kenburn.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('vendor/revolution/revolution.extension.actions.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('vendor/revolution/revolution.extension.parallax.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('vendor/revolution/revolution.extension.migration.min.js')}}"></script>

    <!-- landguage switcher js -->
    <script type="text/javascript" src="{{asset('js/jquery.polyglot.language.switcher.js')}}"></script>
    <!-- Fancybox js -->
    <script type="text/javascript" src="{{asset('js/jquery.fancybox.pack.js')}}"></script>
    <!-- js count to -->
    <script type="text/javascript" src="{{asset('js/jquery.appear.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/jquery.countTo.js')}}"></script>
    <!-- WOW js -->
    <script type="text/javascript" src="{{asset('js/wow.min.js')}}"></script>

    <script type="text/javascript" src="{{asset('js/SmoothScroll.js')}}"></script>

    <script src="{{asset('js/bootstrap-select.min.js')}}"></script>
    <script src="{{asset('js/jquery.mixitup.min.js')}}"></script>
    <!-- Theme js -->
    <script type="text/javascript" src="{{asset('js/theme.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/google-map.js')}}"></script>
    @yield('scripts')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.min.js" integrity="sha512-VZ6m0F78+yo3sbu48gElK4irv2dzPoep8oo9LEjxviigcnnnNvnTOJRSrIhuFk68FMLOpiNz+T77nNY89rnWDg==" crossorigin="anonymous"></script>
    <script>
        let search_result = document.getElementById('search_result');
        let search = document.getElementById('search_form');
        search.addEventListener("keyup", function () {
            if(this.value.length <=2){
                search_result.innerHTML='';
                return false
            }
            axios.get('{{route('search')}}?search=' + this.value).then(function (response){
                console.log(response)
                search_result.innerHTML=response.data
            })
        });
        const button = (document.getElementsByClassName('add_product'));
        const cart_result = document.getElementById('cartresult');
        console.log(cart_result)
        for (let i = 0; i<=button.length-1; i++){
            button[i].addEventListener("click", function () {
                const endpoint = this.getAttribute('data-endpoint')
                axios.get(endpoint).then(function (response) {
                    cart_result.innerHTML=response.data
                })
            })
        }
    </script>
    <script src="https://js.braintreegateway.com/web/dropin/1.13.0/js/dropin.min.js"></script>
    <script src="https://js.stripe.com/v3/"></script>
    <script>
        (function(){
            // Create a Stripe client
            var stripe = Stripe('pk_test_51HOg5BE2xuP8bpGN4ItyXGTGwIjGSqNSsR4txdoA8ydGVbA8kOdwhhAxU7vUEwbsDZHUrZTo9gerPPjOkGmlMSN900b5WQAs3F');
            // Create an instance of Elements
            var elements = stripe.elements();
            // Custom styling can be passed to options when creating an Element.
            // (Note that this demo uses a wider set of styles than the guide below.)
            var style = {
                base: {
                    color: '#32325d',
                    lineHeight: '18px',
                    fontFamily: '"Roboto", Helvetica Neue", Helvetica, sans-serif',
                    fontSmoothing: 'antialiased',
                    fontSize: '16px',
                    '::placeholder': {
                        color: '#aab7c4'
                    }
                },
                invalid: {
                    color: '#fa755a',
                    iconColor: '#fa755a'
                }
            };
            // Create an instance of the card Element
            var card = elements.create('card', {
                style: style,
                hidePostalCode: true
            });
            // Add an instance of the card Element into the `card-element` <div>
            card.mount('#card-element');
            // Handle real-time validation errors from the card Element.
            card.addEventListener('change', function(event) {
                var displayError = document.getElementById('card-errors');
                if (event.error) {
                    displayError.textContent = event.error.message;
                } else {
                    displayError.textContent = '';
                }
            });
            // Handle form submission
            var form = document.getElementById('payment-form');
            form.addEventListener('submit', function(event) {
                event.preventDefault();
                // Disable the submit button to prevent repeated clicks
                document.getElementById('complete-order').disabled = true;
                var options = {
                    name: document.getElementById('name_on_card').value,
                    address_line1: document.getElementById('address').value,
                    address_city: document.getElementById('city').value,
                    address_state: document.getElementById('province').value,
                    address_zip: document.getElementById('postalcode').value
                }
                stripe.createToken(card, options).then(function(result) {
                    if (result.error) {
                        // Inform the user if there was an error
                        var errorElement = document.getElementById('card-errors');
                        errorElement.textContent = result.error.message;
                        // Enable the submit button
                        document.getElementById('complete-order').disabled = false;
                    } else {
                        // Send the token to your server
                        stripeTokenHandler(result.token);
                    }
                });
            });
            function stripeTokenHandler(token) {
                // Insert the token ID into the form so it gets submitted to the server
                var form = document.getElementById('payment-form');
                var hiddenInput = document.createElement('input');
                hiddenInput.setAttribute('type', 'hidden');
                hiddenInput.setAttribute('name', 'stripeToken');
                hiddenInput.setAttribute('value', token.id);
                form.appendChild(hiddenInput);
                // Submit the form
                form.submit();
            }
        })();
    </script>
    <script src="https://www.paypalobjects.com/api/checkout.js"></script>

</div> <!-- End of .main_page -->
</body>

<!-- Mirrored from st.ourhtmldemo.com/template/organic_store/blog-Standard  by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 06 Jul 2020 10:09:04 GMT -->
</html>
