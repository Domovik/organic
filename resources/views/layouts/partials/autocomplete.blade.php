@if($search_products->IsNotEmpty() || $search_blogs->IsNotEmpty())
    <ul>
        <li class="search_result">
            @if($search_products->IsNotEmpty())
                <h5>Products</h5>
            @endif
            @foreach($search_products as $search_product)
                <div class="search-products">
                    <img src="{{Voyager::image($search_product->image)}}" alt="">
                    <span><a href="{{route('product_detail', $search_product->slug)}}">{{$search_product->title}}</a></span>
                </div>
            @endforeach
            @if($search_blogs->IsNotEmpty())
                <h5>Blogs</h5>
            @endif
            @foreach($search_blogs as $search_blog)
                <div class="search-blogs">
                    <img src="{{Voyager::image($search_blog->image_short)}}" alt="">
                    <span><a href="{{route('blog_detail', $search_blog->slug)}}">{{$search_blog->title}}</a></span>
                </div>
            @endforeach
        </li>
    </ul>
@endif

