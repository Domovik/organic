<ul>
    @foreach($cart_products as $cart_product)
        <li>
            <div class="cart_item_wrapper clear_fix">
                <div class="img_holder float_left"><img src="{{Voyager::image($cart_product->product->image)}}" alt="Cart Image" class="img-responsive"></div> <!-- End of .img_holder -->
                <div class="item_deatils float_left">
                    <h6>{{$cart_product->product->title}}</h6>
                    <span class="font_fix">${{$cart_product->product->price}} x {{$cart_product->quantity}}</span>
                </div> <!-- End of .item_deatils -->
            </div> <!-- End of .cart_item_wrapper -->
        </li>
    @endforeach
</ul>
<div class="cart_total clear_fix">
    <span class="total font_fix float_left">Total - {{$total_cart_price}}$</span>
    <a href="{{route('shop-cart')}}" class="s_color_bg float_right tran3s">View Cart</a>
</div>
