

<div class="theme_menu color1_bg">
    <div class="container">
        <nav class="menuzord pull-left" id="main_menu">
<ul  class="menuzord-menu">

    @php

        if (Voyager::translatable($items)) {
            $items = $items->load('translations');
        }

    @endphp

    @foreach ($items as $item)

        @php

            $originalItem = $item;
            if (Voyager::translatable($item)) {
                $item = $item->translate($options->locale);
            }

            $isActive = null;
            $styles = null;
            $icon = null;

            // Background Color or Color
            if (isset($options->color) && $options->color == true) {
                $styles = 'color:'.$item->color;
            }
            if (isset($options->background) && $options->background == true) {
                $styles = 'background-color:'.$item->color;
            }

            // Check if link is current
            if(url($item->link()) == url()->current()){
                $isActive = 'active';
            }

            // Set Icon
            if(isset($options->icon) && $options->icon == true){
                $icon = '<i class="' . $item->icon_class . '"></i>';
            }

        @endphp

        <li class="{{ $isActive }}">
            <a href="{{ url($item->link()) }}" target="{{ $item->target }}" style="{{ $styles }}">
                {!! $icon !!}
                <span>{{ $item->title }}</span>
            </a>
            @if(!$originalItem->children->isEmpty())
                <ul class="dropdown">
                    @include('layouts.partials.dropdawn', ['items' => $originalItem->children, 'options' => $options])
                </ul>
            @endif
        </li>
    @endforeach

</ul>
        </nav> <!-- End of #main_menu -->
    </div> <!-- End of .conatiner -->
</div> <!-- End of .theme_menu -->
<!-- Menu ******************************* -->

{{--<ul class="menuzord-menu">--}}

{{--    <li><a href="{{route('home')}}">Home</a></li>--}}
{{--    <li><a href="{{route('about-us')}}">About us</a></li>--}}
{{--    <li><a href="{{route('shop')}}">store</a>--}}
{{--        <ul class="dropdown">--}}
{{--            <li><a href="{{route('shop')}}">Shop With Sidebar</a></li>--}}
{{--        </ul>--}}
{{--    </li>--}}
{{--    <li class="current_page"><a href="{{route('blog')}}">News</a>--}}
{{--        <ul class="dropdown">--}}
{{--            <li><a href="{{route('blog')}}">News Standard</a></li>--}}
{{--        </ul>--}}
{{--    </li>--}}
{{--    <li><a href="#">Pages</a>--}}
{{--        <ul class="dropdown">--}}
{{--            <li><a href="{{route('testimonial')}}">Testimonials</a></li>--}}
{{--            <li><a href="{{route('faq')}} ">FAQ</a></li>--}}
{{--        </ul>--}}
{{--    </li>--}}

{{--    <li><a href="contact ">Contact us</a></li>--}}
{{--</ul> <!-- End of .menuzord-menu -->--}}

