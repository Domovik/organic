@extends('layouts.app')
@section('content')

			<section class="breadcrumb-area" style="background-image:url(images/background/2.jpg);">
			    <div class="container">
			        <div class="row">
			            <div class="col-md-12">
			                <div class="breadcrumbs text-center">
			                    <h1>{{setting('blog.blog_top')}}</h1>
			                    <h4>{{setting('blog.subtitle_blog')}}</h4>
			                </div>
			            </div>
			        </div>
			    </div>
				<div class="breadcrumb-bottom-area">
				    <div class="container">
				        <div class="row">
				            <div class="col-lg-8 col-md-5 col-sm-5">
				                <ul>
                                    {{ Breadcrumbs::render('blog') }}
				                </ul>
				            </div>
				            <div class="col-lg-4 col-md-7 col-sm-7">
				                <p>{{setting('header.We')}}</p>
				            </div>
				        </div>
				    </div>
				</div>
			</section>

			<section class="news single_news_page with_sidebar">
				<div class="container">
					<div class="row">
						<div class="col-md-9 col-sm-8 col-xs-12">
							<div class="row">
                                @foreach($blogs as $blog)
                                    <div class="col-md-12 col-sm-6 col-xs-12">
                                        <div class="blogList_single_post clear_fix wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
                                            <div class="img_holder">
                                                <img src="{{Voyager::image($blog->image_full)}}" alt="News" class="img-responsive">
                                                <div class="opacity tran3s">
                                                    <div class="icon">
                                                        <span><a href="{{route('blog_detail', $blog->slug)}}" class="border_round">+</a></span>
                                                    </div> <!-- End of .icon -->
                                                </div> <!-- End of .opacity -->
                                            </div> <!-- End of .img_holder -->
                                            <div class="post">

                                                <div class="text">
                                                    <h4><a href="{{route('blog_detail', $blog->slug)}}">{{$blog->title}}</a></h4>
                                                    <ul>
                                                        <li><a href="" class="tran3s"><i class="fa fa-user" aria-hidden="true"></i> {{$blog->author->name}}</a></li>
                                                        <li><a href="" class="tran3s"><i class="fa fa-tag" aria-hidden="true"></i> {{$blog->category->title}}</a></li>
                                                        <li><a href="" class="tran3s"><i class="fa fa-clock-o" aria-hidden="true"></i> {{date('d F Y H:i', strtotime($blog->created_at))}}</a></li>
                                                        <li><a href="" class="tran3s"><i class="fa fa-comments" aria-hidden="true"></i> {{$blog->review->count()}}</a></li>
                                                    </ul>
                                                    {!! $blog->pag_discription !!}
                                                    <div class="link float_left"><a href="{{route('blog_detail', $blog->slug)}}" class="tran3s color1_bg">READ MORE</a></div>
                                                    <div class="share_box float_right">
                                                        <ul class="share-content">
                                                            <li><a href="http://www.facebook.com/sharer.php?u=https://www.facebook.com/onekalo" class="tran3s popup-share" title="Facebook" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                            <li><a href="http://twitter.com/share?url=https://twitter.com/Jubayer_alhasan&amp;text=@Jubayer_alhasan" class="tran3s popup-share" title="Twitter" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                                            <li><a href="https://plus.google.com/share?url=https://plus.google.com/u/0/105261123196798323015" class="tran3s popup-share" title="Goolge-Plus" target="_blank"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                                        </ul>
                                                        <button class="tran3s share" title="Share This Post"><i class="fa fa-share-alt" aria-hidden="true"></i></button>
                                                    </div>
                                                </div>
                                            </div> <!-- End of .post -->
                                        </div>
                                    </div>
                                @endforeach
							</div>
                            @if($paginator->total() > $paginator->count())
                                <br>
                                <div class="row justify-content-center">
                                    <div class="col-md-12">
                                        <div class="card">
                                            <div class="card-body">
                                                {{$paginator->links('layouts.partials.paginator')}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
						</div>

						<!-- _______________________ SIDEBAR ____________________ -->
	        			<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 sidebar_styleTwo">
	        				<div class="wrapper">
	        					<div class="sidebar_search">
	        						<form action="/blog">
	        							<input for="product" type="text" name="filter" value="{{ request()->filter }}" placeholder="Filter hear...">
	        							<button class="tran3s color1_bg"><i class="fa fa-search" aria-hidden="true"></i></button>
	        						</form>
	        					</div> <!-- End of .sidebar_styleOne -->

	        					<div class="sidebar_categories">
	        						<div class="theme_inner_title">
										<h4>Categories</h4>
									</div>
									<ul>
                                        @foreach($blog_categories as $blog_category)
                                            <li><a href="?slug={{$blog_category->slug}}" class="tran3s">{{$blog_category->title}}</a></li>
                                        @endforeach
									</ul>
	        					</div> <!-- End of .sidebar_categories -->

	        					<div class="popular_news">
									<div class="theme_inner_title">
										<h4>popular news</h4>
									</div>
									<div class="recent-posts">
                                        @foreach($footer_blogs as $blog)
                                            <div class="post">
                                                <div class="post-thumb"><a href="#"><img src="{{Voyager::image($blog->image_full)}}" alt=""></a></div>
                                                <h4><a href="{{route('blog_detail', $blog->slug)}}">{{$blog->title}}</a></h4>
                                                <div class="post-info"><i class="fa fa-clock-o"></i>{{date('d F Y H:i', strtotime($blog->created_at))}}</div>
                                            </div>
										@endforeach
									</div>
								</div>
	        				</div> <!-- End of .wrapper -->
	        			</div> <!-- End of .sidebar_styleTwo -->
					</div>
				</div>
			</section>

@endsection
